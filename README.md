* VERSION 2.0 Alpha, January 2016
* Web services application AS Coteaux. Tous les messages de réponse sont encapsulés dans le payload d'un GenericResponseMessage et transférées en JSON

### Au lancement de l'application, initialisation des données pour les notifications push: ###
* HTTP method: PUT
* URL: BASE_URL + "/rest/notifications"
* Message to post: NotificationId
* Response message: TeamListMessage

### Liste des joueurs et leurs stats: ###
* HTTP method: GET
* URL: BASE_URL + "/rest/players?category=XXX"
* Message to post: aucun
* Response message: PlayerListMessage

### Classement d'une équipe (FFF): ###
* HTTP method: GET
* URL: BASE_URL + "/rest/ranking?category=XXX&team=XXX"
* Message to post: aucun
* Response message: HtmlMessage

### Résultats (FFF) ###
* HTTP method: GET
* URL: BASE_URL + "/rest/results"
* Message to post: aucun
* Response message: HtmlMessage

### Calendrier d'une équipe (FFF) ###
* HTTP method: GET
* URL: BASE_URL + "/rest/calendar?category=XXX&team=XXX"
* Message to post: aucun
* Response message: HtmlMessage

### Agenda (FFF) ###
* HTTP method: GET
* URL: BASE_URL + "/rest/agenda";
* Message to post: aucun
* Response message: HtmlMessage

### News (requête de la page principale de l'application) ###
* HTTP method: GET
* URL = BASE_URL + "/rest/news";
* Message to post: aucun
* Response message: NewsListMessage

### Liste des matchs live ###
* HTTP method: GET
* URL: BASE_URL + "/rest/liveMatch";
* Message to post: aucun
* Response message: NewsListMessage

### Liste des commentaires ###
* HTTP method: POST
* URL: BASE_URL + "/rest/liveMatchComments";
* Message to post: aucun
* Response message: CommentListMessage

### Poster commentaire. /!\ Requête authentifiée ### 
* HTTP method: POST
* URL: BASE_URL + "/rest/liveMatchComments";
* Message to post: GenericAuthRequestMessage<MatchMessage>
* Response message: OperationResult

### Modfifier un commentaire. /!\ Requête authentifiée ###
* HTTP method: PUT
* URL: BASE_URL + "/rest/liveMatchComments";
* Message to post: GenericAuthRequestMessage<MatchMessage>
* Response message: OperationResult

### Login ###
* HTTP method: POST
* URL: BASE_URL + "/rest/login";
* Message to post: GenericAuthRequestMessage<LoginMessage>
* Response message: LoginMessage