package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.googlecode.objectify.Key;

import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.MatchType;
import eu.boss.mobilesport.externalmodel.TeamMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.model.Match;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.utils.Util;

@ManagedBean
@ViewScoped
public class LiveManagerBean implements Serializable {

	private static final long serialVersionUID = 1713940591496347254L;
	private boolean editing = false;

	//Simplest to implements UI with a checkox while we have only 2 matchs types. If we need to add new match type in the future,
	// we just need to change checkbox by radio buttons and manage directly the matchType. 
	private boolean friendlyMatch;
	private Match match;

	private List<TeamNameMessage> teamNames = new ArrayList<>();

	@PostConstruct
	private void initIfEditing() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String edit = params.get("edit");
		Long matchId = null;
		if (params.get("id") != null) matchId = Long.valueOf(params.get("id"));

		if ((edit != null) && (edit.equals("true")) && (matchId != null)) {
			editing = true;
			this.match = LiveManager.getMatchById(matchId);
			updateTeamNames();
		}
		if (match == null) {
			match = new Match();
			match.setTeamName(TeamNameMessage.A.name());
		}
	}

	public void initMatch() {
		if (friendlyMatch)
			match.setMatchType(MatchType.FRIENDLY);
		else
			match.setMatchType(MatchType.OFFICIAL);
	}
	
	public String saveMatch() {
		initMatch();
		if (isEditing()) {
			LiveManager.updateMatch(match);
			return "matchedited";
		}
		
		if (LiveManager.addNewMatch(new Match(Key.create(Season.class, Util.currentSeasonDate()),
				match.getDate(), match.getTeam1(), match.getTeam2(), match.getIntitule(),
				Category.valueOf(match.getCategoryName()).name(), match.getTeamName(), match.getMatchType())) != null)
			return "matchadded";
		else return "error";
	}

	public String goCompo() {
		// On passe le match en cours au TempMatchCreationBean sans le
		// sauvegarder.
		initMatch();
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		TempMatchCreationBean tempMatchBean = (TempMatchCreationBean) elContext.getELResolver()
				.getValue(elContext, null, "tempMatchCreationBean");
		tempMatchBean.setMatch(match);
		String edit = "";
		if (isEditing()) edit = "&edit=true";
		return "formation?faces-redirect=true" + edit;
	}

	public void updateTeamNames() {
		// On recupere la valeur de TeamListBean et on recupere lee nombre de
		// team dans cette
		// categorie. Cela evite un acces en base.
		teamNames.clear();
		if (match.getCategoryName().trim().equals("")) return;
		else {
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			TeamListBean teamListBean = (TeamListBean) elContext.getELResolver().getValue(elContext,
					null, "teamListBean");

			for (TeamMessage t : teamListBean.getTeamsByCat(match.getCategoryName())) {
				teamNames.add(TeamNameMessage.valueOf(t.getTeamName()));
			}
		}
	}

	public int getTeamNamesSize() {
		return teamNames.size();
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public Match getMatch() {
		return match;
	}

	public void setMatchToEdit(Match match) {
		this.match = match;
	}

	public List<TeamNameMessage> getTeamNames() {
		return teamNames;
	}

	public void setTeamNames(List<TeamNameMessage> teamNames) {
		this.teamNames = teamNames;
	}

	public boolean getFriendlyMatch() {
		return friendlyMatch;
	}

	public void setFriendlyMatch(boolean friendlyMatch) {
		this.friendlyMatch = friendlyMatch;
	}

}
