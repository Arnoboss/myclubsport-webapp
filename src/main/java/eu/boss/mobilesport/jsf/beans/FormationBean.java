package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.DragDropEvent;

import eu.boss.mobilesport.datastore.FormationManager;
import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.datastore.StatsManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.FormationItemMessage;
import eu.boss.mobilesport.externalmodel.FormationMessage;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.model.Match;
import eu.boss.mobilesport.model.Player;
import eu.boss.mobilesport.utils.Util;

@ManagedBean
@ViewScoped
public class FormationBean implements Serializable {

	private static final long serialVersionUID = 6297534467025552574L;
	// Notre Grille : une liste de CanvasFormationItem
	private List<CanvasFormationItem> lstTarget = new ArrayList<CanvasFormationItem>();
	// une Formation.
	private FormationMessage currentFormation;

	private List<Player> playerNameList = new ArrayList<Player>();
	private final List<String> playerListString = new ArrayList<>();
	private Match currentMatch;
	private boolean isEditing = false;
	private TempMatchCreationBean tempMatchBean;
	private String unexistingPlayers;
	// valeur de la checkbox du dialogue pour les joueurs inexistants
	private boolean addUnexistingPlayers = false;

	@PostConstruct
	public void init() {
		// on recupere les parametre si on est en edition
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String edit = params.get("edit");
		if ((edit != null) && (edit.equals("true"))) isEditing = true;

		// Si on n'est pas en mode edition, on charge la derniere formation utilisee pour l'equipe
		// donnee.
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		tempMatchBean = (TempMatchCreationBean) elContext.getELResolver().getValue(elContext, null,
				"tempMatchCreationBean");

		currentMatch = tempMatchBean.getMatch();
		if (!isEditing) {
			currentFormation = FormationManager.getFormationOfLastMatch(
					Category.valueOf(currentMatch.getCategoryName()),
					TeamNameMessage.valueOf(currentMatch.getTeamName()));
			// On retire l'ID de la formation et des items pour que la nouvelle soit unique
			if (currentFormation != null) currentFormation.resetFormationIds();
		} else {
			currentFormation = FormationManager.loadFormationMessage(currentMatch.getId());
		}

		// On initialise le nombre des FormationItem a positionner
		// On est parti pour du foot e 11, mais on pourrait tres bien faire du
		// foot a 7 voire du rugby e XV. La liste currentFormation est utilisee pour l'affichage des
		// noms sur la gauche de l'ecran; la lstTarget concerne les joueurs sur le terrain. C'est
		// obligatoire d'utiliser 2 listes. En effet, en
		// utilisant une seule liste, l'ordre des joueurs dans la liste de gauche se modifierait lors
		// de chaque drag & drop, ce
		// qui fait qu'ils ne seraient plus tries par numero. Les 2 listes pointent sur les memes
		// items, c'est pour cela que la mise a jour des noms se fait dans les 2 listes.

		if (currentFormation == null) {
			currentFormation = new FormationMessage();
			currentFormation.setFormationItemList(new ArrayList<FormationItemMessage>());
			// par defaut on met numItem == coord
			for (int i = 1; i <= currentFormation.getNumberOfPlayers(); i++) {
				FormationItemMessage item = new FormationItemMessage();
				item.setNumItem(i);
				// On n'ajoute des coordonnees que pour les 11 premiers joueurs. Les autres sont les
				// remplacants
				if (i <= currentFormation.getNbTitulaires()) item.setCoord(i + 1);

				currentFormation.getFormationItemList().add(item);
			}
		}

		// On ajoute des joueurs vide jusqu'a atteindre le nombre max autorise.
		while (currentFormation.getFormationItemList().size() < currentFormation
				.getNumberOfPlayers()) {
			FormationItemMessage item = new FormationItemMessage();
			item.setNumItem(currentFormation.getFormationItemList().size() + 1);
			currentFormation.getFormationItemList().add(item);
		}

		Collections.sort(currentFormation.getFormationItemList());
		drawCanvas();
		playerNameList = StatsManager.loadPlayersForCategoryAndSeason(
				Category.valueOf(currentMatch.getCategoryName()), Util.currentSeasonDate());
		for (Player p : playerNameList) {
			playerListString.add(p.getName());
		}
	}

	private void drawCanvas() {
		// On cree la grille
		lstTarget = new ArrayList<CanvasFormationItem>();
		for (int i = 1; i <= 30; i++) {
			CanvasFormationItem item = new CanvasFormationItem();
			item.setStrIdx(String.format("%d", i));

			// On associe le FormationItem qui a le coord correspondant
			if (currentFormation != null && currentFormation.getFormationItemList() != null) {
				for (FormationItemMessage fi : currentFormation.getFormationItemList()) {
					if (fi.getCoord() != null && fi.getCoord().equals(Integer.valueOf(i))) {
						item.setFormationItem(fi);
						break;
					}
				}
			}
			lstTarget.add(item);
		}
	}

	public void onDrop(DragDropEvent event) {
		// Puisque l'on a associe une datasource a nos elements droppable
		// alors event.getData() contient un objet Java et en l'occurrence
		// puisque la datasource en question etait la DataGrid, elle-meme
		// associee a lstTarget etant une ArrayList<CanvasFormationItem> on a
		// donc acces a un objet de type CanvasFormationItem
		// Attention, c'est l'objet dragge, donc l'objet source.
		CanvasFormationItem item = (CanvasFormationItem) event.getData();
		// Pour avoir acces e l'objet ou on a droppe, ce n'est pas encore
		// possible du coup on feinte :D on recupere l'ID de l'element DOM sur
		// lequel on a droppe
		String idCoord = event.getDropId();
		// et on recupere l'index de la grille (j'avoue c'est pas super classe)
		idCoord = idCoord.substring(idCoord.indexOf("trgField")); // cf trgField
		// dans la
		// JSF
		idCoord = idCoord.substring(idCoord.indexOf(':') + 1, idCoord.lastIndexOf(':'));
		Integer coord = Integer.parseInt(idCoord) + 1; // les id autos
		// commencencent a 0 alors
		// que les index de
		// notre grille commencent
		// a 1

		// Maintenant on met a jour les coords des FormationItem et
		// CanvasFormationItem incrimines
		FormationItemMessage fi = item.getFormationItem();

		for (CanvasFormationItem cfi : lstTarget) {
			if (cfi.getStrIdx().equals(String.format("%d", coord))) {
				// on swappe les formationItem
				FormationItemMessage fiNew = cfi.getFormationItem();

				Integer oldCoord = null;
				if (fi != null) {
					oldCoord = fi.getCoord();
				}

				cfi.setFormationItem(fi);
				item.setFormationItem(fiNew);

				if (fi != null) cfi.getFormationItem().setCoord(coord);
				if (oldCoord != null && item.getFormationItem() != null) {
					item.getFormationItem().setCoord(oldCoord);
				}
				break;
			}
		}

	}

	public void updateNbTitulaires() {
		updateNbPlayers();
		drawCanvas();
	}

	public void updateNbPlayers() {
		// On retire des joueurs
		if (currentFormation.getFormationItemList().size() > currentFormation.getNumberOfPlayers()) {
			List<FormationItemMessage> sublist = new ArrayList<FormationItemMessage>(currentFormation
					.getFormationItemList().subList(0, currentFormation.getNumberOfPlayers()));
			currentFormation.setFormationItemList(sublist);
			// On met des coordonnees vides aux remplacants
			for (int i = currentFormation.getNbTitulaires(); i < currentFormation
					.getNumberOfPlayers(); i++) {
				currentFormation.getFormationItemList().get(i).setCoord(null);
			}
		} else {
			// On en ajoute
			for (int i = 0; i < currentFormation.getNumberOfPlayers(); i++) {
				if (i < currentFormation.getFormationItemList().size()
						&& currentFormation.getFormationItemList().get(i) != null) {
					if (currentFormation.getFormationItemList().get(i).getCoord() == null
							&& i < currentFormation.getNbTitulaires())
						currentFormation.getFormationItemList().get(i).setCoord(getFreePlace());
				} else {
					FormationItemMessage item = new FormationItemMessage();
					item.setNumItem(i + 1);
					// On n'ajoute des coordonnees que pour les 11 premiers joueurs. Les autres sont les
					// remplacants
					if (i < currentFormation.getNbTitulaires()) item.setCoord(getFreePlace());
					currentFormation.getFormationItemList().add(item);
				}
			}
		}
	}

	/**
	 * On cherche un emplacement vide dans la grille du terrain pour ajouter un nouveau joueur sans
	 * tout chambouler
	 * 
	 * @return
	 */
	private int getFreePlace() {
		List<FormationItemMessage> items = currentFormation.getFormationItemList();
		for (int i = 1; i <= 30; i++) {

			boolean occupied = false;
			for (int j = 0; j < items.size(); j++) {
				if (items.get(j) != null && items.get(j).getCoord() != null
						&& items.get(j).getCoord() == i) {
					occupied = true;
					break;
				}
			}
			if (!occupied) return i;
		}
		// should never happen
		return 0;
	}

	public List<String> completePlayerName(String query) {
		List<String> results = new ArrayList<String>();
		for (Player p : playerNameList) {
			if (p.getName().toLowerCase().startsWith(query.toLowerCase())) results.add(p.getName());
		}

		return results;
	}

	private void saveFormation() {
		FormationManager.persistFormationAndItems(currentFormation);
	}

	private void saveMatch() {
		Long matchId;
		if (isEditing) {
			matchId = currentMatch.getId();
			LiveManager.updateMatch(currentMatch);
		} else {
			matchId = LiveManager.addNewMatch(currentMatch).getId();
		}
		currentFormation.setMatchId(matchId);
	}

	public String saveFormationMatch(boolean saveNewPlayers) {
		if (hasUnexistingPlayer() && !saveNewPlayers) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dialogConfirmNewPlayers').show();");
			return null;
		} else {
			createUnexistingPlayers();
			saveMatch();
			saveFormation();
			
			return (isEditing? "matchedited": "matchadded");
		}
	}

	/**
	 * NOTE: Plus utilise pour le moment
	 * 
	 * @param saveNewPlayers
	 * @return
	 */
	public String saveFormationMatchAndGoConvoc(boolean saveNewPlayers) {

		if (hasUnexistingPlayer() && !saveNewPlayers) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dialogConfirmNewPlayers').show();");
			return null;
		} else {
			saveMatch();
			saveFormation();
			tempMatchBean.setFormation(currentFormation);
			return "newsManager?faces-redirect=true&generateConvoc=y";
		}
	}

	/**
	 * Teste s'il y a dans la compo un joueur qui n'existe pas dans les stats.
	 * 
	 * @return
	 */
	public boolean hasUnexistingPlayer() {
		unexistingPlayers = "";
		for (FormationItemMessage currentPlayer : currentFormation.getFormationItemList()) {
			if ((currentPlayer.getPlayer().getName() != null
					&& !currentPlayer.getPlayer().getName().equals(""))
					&& (!playerListString.contains(currentPlayer.getPlayer().getName())))
				unexistingPlayers += currentPlayer.getPlayer().getName() + ", ";
		}
		if (!unexistingPlayers.isEmpty()) {
			unexistingPlayers = unexistingPlayers.substring(0, unexistingPlayers.length() - 2);
			return true;
		}
		return false;
	}

	public void createUnexistingPlayers() {
		if (!unexistingPlayers.isEmpty() && addUnexistingPlayers) {
			List<PlayerMessage> playersToAdd = new ArrayList<PlayerMessage>();
			// On recupere la valeur de TeamListBean et on recupere lee nombre de team dans cette
			// categorie. Cela evite un acces en base.
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			TeamListBean teamListBean = (TeamListBean) elContext.getELResolver().getValue(elContext,
					null, "teamListBean");
			Category currentCategory = Category.valueOf(currentMatch.getCategoryName());
			int numberOfTeamInCategory = teamListBean.getNumberOfTeamInCategory(currentCategory);
			String[] separatedNewPlayers = unexistingPlayers.split(", ");
			for (int i = 0; i < separatedNewPlayers.length; i++) {
				playersToAdd.add(new PlayerMessage(separatedNewPlayers[i].trim(), currentCategory,
						numberOfTeamInCategory));
			}
			StatsManager.persistPlayerList(playersToAdd, currentCategory, Util.currentSeasonDate());
		}
	}

	public List<CanvasFormationItem> getLstTarget() {
		return lstTarget;
	}

	public void setLstTarget(List<CanvasFormationItem> lstTarget) {
		this.lstTarget = lstTarget;
	}

	public FormationMessage getCurrentFormation() {
		return currentFormation;
	}

	public void setCurrentFormation(FormationMessage currentFormation) {
		this.currentFormation = currentFormation;
	}

	public Match getCurrentMatch() {
		return currentMatch;
	}

	public void setCurrentMatch(Match currentMatch) {
		this.currentMatch = currentMatch;
	}

	public boolean isEditing() {
		return isEditing;
	}

	public void setEditing(boolean isEditing) {
		this.isEditing = isEditing;
	}

	public String getUnexistingPlayers() {
		return unexistingPlayers;
	}

	public void setUnexistingPlayers(String unexistingPlayers) {
		this.unexistingPlayers = unexistingPlayers;
	}

	public boolean isAddUnexistingPlayers() {
		return addUnexistingPlayers;
	}

	public void setAddUnexistingPlayers(boolean addUnexistingPlayers) {
		this.addUnexistingPlayers = addUnexistingPlayers;
	}

}
