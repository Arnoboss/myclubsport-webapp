package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import eu.boss.mobilesport.utils.Constant;
import eu.boss.mobilesport.utils.EnumProperties;
import eu.boss.mobilesport.utils.Prop;

@ManagedBean
public class ContactBean implements Serializable {

	private static final long serialVersionUID = -6487985440925721694L;
	private static final Logger LOGGER = Logger.getLogger(ContactBean.class.getName());

	private String contactMail;
	private String contactName;
	private String message;

	public String submit() {
		try {
			LOGGER.info("Submit contact form");
			sendEmail();

			return "contactFormSent?faces-redirect=true";
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
			return "failed";
		}
	}

	public void sendEmail() {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Constant.CONTACT_EMAIL, Constant.GENERIC_PWD);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Constant.CONTACT_EMAIL, contactName));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(Constant.CONTACT_RECIPIENT));
			message.setSubject("Contact "
					+ Prop.getInstance(EnumProperties.LABELS).getString("header.teamName"));
			message.setText("Adresse de contact: " + contactMail + "\n\n" + this.message);

			Transport.send(message);

			LOGGER.info("Email sent");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String getContactMail() {
		return contactMail;
	}

	public void setContactMail(String contactMail) {
		this.contactMail = contactMail;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
