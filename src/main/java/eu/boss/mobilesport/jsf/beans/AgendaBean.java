package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.fff.FFFWeb;

@ManagedBean
@RequestScoped
public class AgendaBean implements Serializable {

	private static final long serialVersionUID = -6487985440925721694L;
	private static final Logger LOGGER = Logger.getLogger(AgendaBean.class.getName());

	private String htmlAgenda;

	public String getHtmlAgenda() {
		try {
			LOGGER.info("Loading agenda");
			if (htmlAgenda == null) htmlAgenda = FFFWeb.loadAgenda(false);
		} catch (BossException e) {
			htmlAgenda = "<p>Erreur de chargement de la page. Veuillez réessayer</p>";
			LOGGER.severe(e.getMessage());
		}
		return htmlAgenda;
	}

	public void setHtmlAgenda(String htmlAgenda) {
		this.htmlAgenda = htmlAgenda;
	}

}
