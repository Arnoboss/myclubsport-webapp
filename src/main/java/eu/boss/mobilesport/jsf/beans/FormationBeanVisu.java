package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import eu.boss.mobilesport.datastore.FormationManager;
import eu.boss.mobilesport.externalmodel.FormationItemMessage;
import eu.boss.mobilesport.externalmodel.FormationMessage;

/*
 * Bean utilise uniquement pour la visualisation de la formation d'un match donne. Simplification de
 * FormationBean. TODO: redondance de code. A ameliorer
 */
@ManagedBean
@ViewScoped
public class FormationBeanVisu implements Serializable {

	private static final long serialVersionUID = 990370004025862019L;
	private static final Logger LOGGER = Logger.getLogger(FormationBeanVisu.class.getName());

	// Notre Grille : une liste de CanvasFormationItem
	private List<CanvasFormationItem> lstTarget = new ArrayList<CanvasFormationItem>();
	// une Formation.
	private FormationMessage currentFormation;
	// On utilise un booleen fixe a cause de probleme avec "empty" des
	private boolean displayEmptyImage;

	@PostConstruct
	public void init() {
		try {
			// on recupere l'id du match
			Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
					.getRequestParameterMap();
			String matchId = params.get("id");
			if ((matchId != null)) currentFormation = FormationManager.loadFormationMessage(Long
					.valueOf(matchId));
			if (currentFormation != null) {
				// On ajoute des joueurs vide jusqu'a atteindre le nombre max autorise.
				while (currentFormation.getFormationItemList().size() < currentFormation
						.getNbTitulaires()) {
					FormationItemMessage item = new FormationItemMessage();
					item.setNumItem(currentFormation.getFormationItemList().size() + 1);
					currentFormation.getFormationItemList().add(item);
				}

				Collections.sort(currentFormation.getFormationItemList());
				// On cree la grille
				lstTarget = new ArrayList<CanvasFormationItem>();
				for (int i = 1; i <= 30; i++) {
					CanvasFormationItem item = new CanvasFormationItem();
					item.setStrIdx(String.format("%d", i));

					// On associe le FormationItem qui a le coord correspondant
					if (currentFormation != null && currentFormation.getFormationItemList() != null) {
						for (FormationItemMessage fi : currentFormation.getFormationItemList()) {
							if (fi.getCoord() != null && fi.getCoord().equals(Integer.valueOf(i))) {
								item.setFormationItem(fi);
								break;
							}
						}
					}
					lstTarget.add(item);
				}
			} else displayEmptyImage = true;
		} catch (Exception e) {
			LOGGER.warning("No formation for ID");
			displayEmptyImage = true;
		}
	}

	public List<CanvasFormationItem> getLstTarget() {
		return lstTarget;
	}

	public void setLstTarget(List<CanvasFormationItem> lstTarget) {
		this.lstTarget = lstTarget;
	}

	public FormationMessage getCurrentFormation() {
		return currentFormation;
	}

	public void setCurrentFormation(FormationMessage currentFormation) {
		this.currentFormation = currentFormation;
	}

	public boolean isDisplayEmptyImage() {
		return displayEmptyImage;
	}

	public void setDisplayEmptyImage(boolean displayEmptyImage) {
		this.displayEmptyImage = displayEmptyImage;
	}

}
