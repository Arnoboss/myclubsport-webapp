package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import eu.boss.mobilesport.datastore.LoginManager;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

	private static final Logger LOGGER = Logger.getLogger(LoginBean.class.getName());

	private static final long serialVersionUID = -642466821305868308L;
	private String name;
	private String password;
	private boolean isLogged;

	public void login() {
		LOGGER.info("LOGIN");
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		String userAgent = request.getHeader("user-agent");
		LOGGER.info(userAgent);
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		if (LoginManager.login(name, password)) {
			password = "";
			isLogged = true;
			context.addCallbackParam("loginValid", true);
		} else {
			message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur",
					"Identifiant ou mot de passe incorrect");
			FacesContext.getCurrentInstance().addMessage(null, message);
			context.addCallbackParam("loginValid", false);
			
		}

	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		this.name = "";
		this.password = "";
		this.isLogged = false;
		return "news?faces-redirect=true";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsLogged() {
		return isLogged;
	}

}
