package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.TeamMessage;
import eu.boss.mobilesport.utils.Util;

/**
 * Classe qui charge la liste des equipes. Utilisee dans les pages de stats, classements et
 * calendriers pour gerer dynamiquement le nombre d'equipes
 * 
 * @author Arnaud
 */
@ManagedBean
@SessionScoped
public class TeamListBean implements Serializable {

	private static final long serialVersionUID = 2299201079019300253L;

	List<TeamMessage> teamList = new ArrayList<>();
	List<Category> categoryList = new ArrayList<>();

	@PostConstruct
	public void loadTeamList() {
		teamList = TeamsManager.getAllTeamsForSeason(true, Util.currentSeasonDate());
		for (TeamMessage t : teamList) {
			if (!categoryList.contains(Category.valueOf(t.getCategory()))) {
				categoryList.add(Category.valueOf(t.getCategory()));
			}
		}
	}

	public List<TeamMessage> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<TeamMessage> teamList) {
		this.teamList = teamList;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public List<TeamMessage> getTeamsByCat(String cat) {
		List<TeamMessage> tList = new ArrayList<>();
		for (TeamMessage team : teamList) {
			if (team.getCategory().equals(cat)) {
				tList.add(team);
			}
		}
		return tList;
	}

	public int getNumberOfTeamInCategory(Category cat) {
		return getTeamsByCat(cat.name()).size();
	}

}
