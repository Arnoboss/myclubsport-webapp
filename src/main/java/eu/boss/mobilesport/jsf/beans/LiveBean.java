package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.model.Match;

@ManagedBean
@ViewScoped
public class LiveBean implements Serializable {

	private static final long serialVersionUID = 1713940591496347254L;
	private Collection<Match> matchs = new ArrayList<Match>();
	private Match selectedMatch;

	@PostConstruct
	public void loadMatchs() {
		matchs = LiveManager.getNextMatchs();
	}

	public String editMatch() {
		return "liveMatchManager?faces-redirect=true&edit=true&id=" + selectedMatch.getId();
	}

	public String displayDetails() {
		return "liveMatchDetails?faces-redirect=true&id=" + selectedMatch.getId();
	}

	public void deleteSelected() {
		LiveManager.deleteMatch(selectedMatch.getId());
		matchs.remove(selectedMatch);
	}

	public boolean isListEmpty() {
		return matchs.size() == 0;
	}

	public Collection<Match> getMatchs() {
		return matchs;
	}

	public void setMatchs(Collection<Match> matchs) {
		this.matchs = matchs;
	}

	public Match getSelectedMatch() {
		return selectedMatch;
	}

	public void setSelectedMatch(Match selectedMatch) {
		this.selectedMatch = selectedMatch;
	}

}
