package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import eu.boss.mobilesport.datastore.CloudServiceManager;
import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.externalmodel.NewsType;
import eu.boss.mobilesport.model.News;
import eu.boss.mobilesport.pushnotifications.NotificationType;
import eu.boss.mobilesport.pushnotifications.PushNotificationManager;
import eu.boss.mobilesport.utils.Constant;

@ManagedBean
@ViewScoped
/**
 * Initialization of blobstore url. Used to upload file on the cloud
 * 
 * @author Arnaud
 */
public class NewsManagerBean implements Serializable {

	private static final long serialVersionUID = 3271496816208332663L;
	private static final Logger LOGGER = Logger.getLogger(NewsManagerBean.class.getName());

	private String title;
	private String text;
	private String newsType = NewsType.AUTRE.getLabel();
	private boolean sendNotification;
	private boolean displayImageSelection = true;
	private boolean editing = false;
	private boolean keepOldImage;
	private UploadedFile uploadedImage;
	private News newsToEdit;

	/**
	 * On check s'il y a des parametres passes. S'il y en a, on recupere la news associee pour
	 * l'editer ou le match dont on veut automatiser la convocation
	 */
	@PostConstruct
	private void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		String edit = params.get("edit");
		Long newsId = null;

		if (params.get("id") != null) newsId = Long.valueOf(params.get("id"));

		if (params.get("generateConvoc") != null && params.get("generateConvoc").equals("y")) {
			// On va recuperer le match dans le bean temporaire
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			TempMatchCreationBean tempMatchBean = (TempMatchCreationBean) elContext.getELResolver()
					.getValue(elContext, null, "tempMatchCreationBean");

			// On recupere le match et la formation stockes dans le bean
			// temporaire

			setNewsType(NewsType.CONVOCATION.getLabel());
			this.title = tempMatchBean.getMatch().getConvocTitle();
			this.text = tempMatchBean.getMatch().getConvocContent()
					+ "******* VEUILLEZ PRECISER L'HEURE DE RENDEZ-VOUS *******" + "<div><br></div>"
					+ tempMatchBean.getFormation().getConvocs();
		}

		if ((edit != null) && (edit.equals("true")) && (newsId != null)) {
			editing = true;
			this.newsToEdit = NewsManager.getNewsById(newsId);
			this.title = newsToEdit.getTitle();
			this.text = newsToEdit.getContent();
			this.keepOldImage = true;
		}
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getNewsType() {
		return newsType;
	}

	public void setNewsType(String newsType) {

		if (newsType.equals(NewsType.AUTRE.name())) displayImageSelection = true;
		else displayImageSelection = false;

		this.newsType = newsType;
	}

	public boolean getDisplayImageSelection() {
		return displayImageSelection;
	}

	public void setDisplayImageSelection(boolean displayImageSelection) {
		this.displayImageSelection = displayImageSelection;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String addNews() throws UnsupportedEncodingException {
		try {
			// FIX: Primefaces bug, see
			// http://stackoverflow.com/questions/11190081/primefaces-fileupload-filter-with-utf8-characters-filter
			this.text = new String(text.getBytes("ISO-8859-1"), "UTF-8");
			this.title = new String(title.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.severe("error encoding utf8: " + e.toString());
		}

		LOGGER.info("addNews: " + title);
		LOGGER.info("displayImageSelection: " + displayImageSelection);
		LOGGER.info("keepOldImage: " + keepOldImage);
		String imageUrl = "";
		String newsTypeBis = newsType;

		if (newsType.equals(NewsType.AGENDA.name())) imageUrl = Constant.AGENDA_IMG_URL;
		else if (newsType.equals(NewsType.RESULTAT.name())) imageUrl = Constant.RESULTS_IMG_URL;
		else if (newsType.equals(NewsType.CONVOCATION.name())) imageUrl = Constant.CONVOCS_IMG_URL;

		else if (!keepOldImage && newsType.equals(NewsType.AUTRE.name())) {
			try {
				imageUrl = uploadSelectedImage();
			} catch (Exception e) {
				LOGGER.severe("Exception saving news: " + e.toString());
				return "error";
			}
		} else {
			// On est en mode edition et on garde l'ancienne image
			imageUrl = newsToEdit.getImageUrl();
			newsTypeBis = newsToEdit.getNewsType().getLabel();
		}

		if (editing) {
			newsToEdit.setTitle(title);
			newsToEdit.setContent(text);
			newsToEdit.setImageUrl(imageUrl);
			newsToEdit.setNewsType(NewsType.getByLabel(newsTypeBis));
			NewsManager.editNews(newsToEdit);
			return "newsedited";
		} else {
			News news = new News(title, new Date(), imageUrl, "", text, NewsType.valueOf(newsType));
			if (NewsManager.addNews(news) == null) {
				LOGGER.severe("Error saving news");
				return "error";
			}
			if (sendNotification) {
				if (!PushNotificationManager.sendNotificationMsgToAll(title, text,
						NotificationType.NEWS, imageUrl, "" + news.getId()))
					LOGGER.severe("Problem sending push notification");
			}
			return "newsadded";
		}

	}

	public void handleFileUpload(FileUploadEvent event) {
		LOGGER.info("handleFileUpload: " + event.getFile().getFileName());
		this.uploadedImage = event.getFile();
	}

	private String uploadSelectedImage() throws Exception {
		try {
			String filename = CloudServiceManager.uploadImageFile(uploadedImage.getInputstream(),
					uploadedImage.getContentType(), uploadedImage.getFileName(), Constant.FOLDER_NEWS);

			LOGGER.info(uploadedImage.getFileName() + " a été importé avec succès.");
			return filename;
		} catch (Exception e) {
			LOGGER.severe("Problem uploading image: " + e.toString());
			throw e;
		}
	}

	public String getFileName() {
		if (this.uploadedImage != null && this.uploadedImage.getFileName() != null)
			return this.uploadedImage.getFileName();
		return "";
	}

	public boolean getSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean isEditing) {
		this.editing = isEditing;
	}

	public boolean isKeepOldImage() {
		return keepOldImage;
	}

	public void setKeepOldImage(boolean keepOldImage) {
		this.keepOldImage = keepOldImage;
	}

	public UploadedFile getUploadedImage() {
		return uploadedImage;
	}

	public void setUploadedImage(UploadedFile uploadedImage) {
		this.uploadedImage = uploadedImage;
	}

}
