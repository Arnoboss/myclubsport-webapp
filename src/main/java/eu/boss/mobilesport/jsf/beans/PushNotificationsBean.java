package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import eu.boss.mobilesport.pushnotifications.NotificationType;
import eu.boss.mobilesport.pushnotifications.PushNotificationManager;

@ManagedBean
@ViewScoped
public class PushNotificationsBean implements Serializable {

	private static final long serialVersionUID = 3688498527592603875L;
	private static final Logger LOGGER = Logger.getLogger(PushNotificationsBean.class.getName());

	private String titleMsg;
	private String textMsg;
	private boolean sendIOs = true;
	private boolean sendAndroid = true;

	public String sendPushMessage() {
		LOGGER.info("sendMessage");
		// On recupere la valeur de loginBean et on verifie que l'utilisateur est bien logge
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		LoginBean loginBean = (LoginBean) elContext.getELResolver().getValue(elContext, null,
				"loginBean");

		if (loginBean.getIsLogged()) {
			// On passe a null la valeur detailsID car on affichera la derniere news.
			if (PushNotificationManager.sendNotificationMsgTo(titleMsg, textMsg,
					NotificationType.OTHER, "", null, sendIOs, sendAndroid)) return "notificationSent";
		}
		return "error";
	}

	public String getTitleMsg() {
		return titleMsg;
	}

	public void setTitleMsg(String titleMsg) {
		this.titleMsg = titleMsg;
	}

	public String getTextMsg() {
		return textMsg;
	}

	public void setTextMsg(String textMsg) {
		this.textMsg = textMsg;
	}

	public boolean isSendIOs() {
		return sendIOs;
	}

	public void setSendIOs(boolean sendIOs) {
		this.sendIOs = sendIOs;
	}

	public boolean isSendAndroid() {
		return sendAndroid;
	}

	public void setSendAndroid(boolean sendAndroid) {
		this.sendAndroid = sendAndroid;
	}

}
