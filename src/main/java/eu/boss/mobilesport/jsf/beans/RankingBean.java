package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.fff.FFFWeb;
import eu.boss.mobilesport.model.Team;
import eu.boss.mobilesport.utils.Util;

@ManagedBean
@ViewScoped
public class RankingBean implements Serializable {

	private static final long serialVersionUID = -6487985440925721694L;
	private static final Logger LOGGER = Logger.getLogger(RankingBean.class.getName());

	private String htmlRanking;

	private String teamCat;
	private String teamName;

	private List<String> seasons = Util.getSeasonsList();
	private String selectedSeason = Util.currentSeasonDate();

	@PostConstruct
	public void init() {
		LOGGER.info("Loading ranking");
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		if (params != null) {
			teamCat = params.get("category");
			teamName = params.get("team");
		}
		loadRanking();

	}

	public void loadRanking() {
		try {
			Team t = TeamsManager.loadTeamForSeason(teamCat, teamName, selectedSeason);
			String url = t.getUrlRank();

			htmlRanking = FFFWeb.loadRankingFromUrl(url, false);
		} catch (BossException e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
		}
	}

	public String getHtmlRanking() {
		return htmlRanking;
	}

	public void setHtmlRanking(String htmlRanking) {
		this.htmlRanking = htmlRanking;
	}

	public String getTeamCat() {
		return teamCat;
	}

	public void setTeamCat(String teamCat) {
		this.teamCat = teamCat;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<String> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<String> seasons) {
		this.seasons = seasons;
	}

	public String getSelectedSeason() {
		return selectedSeason;
	}

	public void setSelectedSeason(String selectedSeason) {
		this.selectedSeason = selectedSeason;
	}

}
