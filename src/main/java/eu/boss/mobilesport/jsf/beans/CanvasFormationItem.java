package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;

import eu.boss.mobilesport.externalmodel.FormationItemMessage;

public class CanvasFormationItem implements Serializable, Comparable<CanvasFormationItem> {

	private static final long serialVersionUID = -3034501531215196425L;
	private String strIdx;
	private FormationItemMessage formationItem;

	public String getStrIdx() {
		return strIdx;
	}

	public void setStrIdx(String strIdx) {
		this.strIdx = strIdx;
	}

	public FormationItemMessage getFormationItem() {
		return formationItem;
	}

	public void setFormationItem(FormationItemMessage formationItem) {
		this.formationItem = formationItem;
	}

	@Override
	public int compareTo(CanvasFormationItem formationItemToCompare) {

		if (formationItem == null || formationItem.getNumItem() == null) return 1;
		else if (formationItemToCompare == null || formationItemToCompare.getFormationItem() == null) return 0;
		else if (formationItem.getNumItem() < formationItemToCompare.getFormationItem().getNumItem()) return 1;
		else return 0;
	}

}
