package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.model.Comment;
import eu.boss.mobilesport.model.Match;

@ManagedBean
@ViewScoped
public class LiveDetailsBean implements Serializable {

	private static final long serialVersionUID = -7302019881196721706L;
	private Match match;
	private List<Comment> comments;

	@PostConstruct
	private void initIfEditing() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		Long matchId = null;
		if (params.get("id") != null) matchId = Long.valueOf(params.get("id"));

		if (matchId != null) {
			this.match = LiveManager.getMatchById(matchId);
			this.comments = LiveManager.getCommentsForMatch(matchId);
		}

	}

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
