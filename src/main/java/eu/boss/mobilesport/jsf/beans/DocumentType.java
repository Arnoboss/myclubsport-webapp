package eu.boss.mobilesport.jsf.beans;

public enum DocumentType {
	FOLDER, PICTURE;
}
