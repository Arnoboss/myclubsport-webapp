package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

import eu.boss.mobilesport.datastore.StatsManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.externalmodel.PlayerPositionMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.externalmodel.TeamStatsMessage;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.utils.Util;

@ManagedBean
@ViewScoped
public class StatsBean implements Serializable {

	private static final long serialVersionUID = 7701175790963236820L;
	private static final Logger LOGGER = Logger.getLogger(StatsBean.class.getName());

	private List<PlayerMessage> players = new ArrayList<PlayerMessage>();
	private List<PlayerMessage> playersToDelete = new ArrayList<PlayerMessage>();
	private Season season;
	// Used in dialog box when adding new player
	private String newPLayerName;
	private List<PlayerMessage> checkedPlayers = new ArrayList<PlayerMessage>();

	private int numberOfRowSelected = 0;
	// Team selected in confirm dialog for mass action
	private String teamSelected;

	private Category category;
	private List<TeamNameMessage> teamNames = new ArrayList<>();
	private int numberOfTeamInCategory = 1;

	private List<String> seasons = Util.getSeasonsList();
	private String selectedSeason = Util.currentSeasonDate();

	@PostConstruct
	public void init() {
		loadPlayerList();
	}

	public void loadPlayerList() {
		newPLayerName = "";
		teamSelected = "";
		playersToDelete = new ArrayList<PlayerMessage>();
		checkedPlayers = new ArrayList<PlayerMessage>();
		numberOfRowSelected = 0;
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		if (params != null && params.get("cat") != null)
			this.category = Category.getByName(params.get("cat"));

		// On recupere la valeur de TeamListBean et on recupere lee nombre de team dans cette
		// categorie. Cela evite un acces en base.
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		TeamListBean teamListBean = (TeamListBean) elContext.getELResolver().getValue(elContext, null,
				"teamListBean");

		numberOfTeamInCategory = teamListBean.getNumberOfTeamInCategory(this.category);
		for (int i = 0; i < numberOfTeamInCategory; i++) {
			teamNames.add(TeamNameMessage.values()[i]);
		}

		players = StatsManager.loadAllPlayerStatsForSeasonAndCategory(selectedSeason, this.category,
				numberOfTeamInCategory);

	}

	public void addNewPlayer(ActionEvent event) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage message = null;
		boolean isValid = false;
		if (newPLayerName != null && (!newPLayerName.trim().equals(""))) {
			newPLayerName = newPLayerName.trim();
			for (PlayerMessage p : players) {
				if (p.getName().equals(newPLayerName)) {
					isValid = false;
					message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur",
							"Un joueur porte déjà ce nom. Merci d'en choisir un autre.");
					FacesContext.getCurrentInstance().addMessage(null, message);
					context.addCallbackParam("playerNameValid", isValid);
					return;
				}
			}
			isValid = true;
			players
					.add(new PlayerMessage(newPLayerName.trim(), this.category, numberOfTeamInCategory));
			newPLayerName = "";
		} else {
			isValid = false;
			// not working
			message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur", "Le nom est vide");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		context.addCallbackParam("playerNameValid", isValid);

	}

	/**
	 * Action performed clicking on "Delete": delete selected players
	 */
	public void deleteSelected() {
		LOGGER.info("*deleteSelected*");
		for (PlayerMessage p : checkedPlayers) {
			players.remove(p);
			playersToDelete.add(p);
		}
		checkedPlayers.clear();
	}

	/**
	 * Action performed clicking on "Add Match". Increments match counter for selected players
	 */
	public void addMatchSelected() {
		LOGGER.info("addMatchSelected " + teamSelected);
		for (PlayerMessage p : checkedPlayers) {
			for (TeamStatsMessage ts : p.getTeamStats()) {
				if (ts.getTeamName().name().equals(teamSelected)) {
					ts.setGamesPlayed(ts.getGamesPlayed() + 1);
				}
			}
		}
		teamSelected = "";
		checkedPlayers.clear();
		numberOfRowSelected = 0;
	}

	/**
	 * Action performed clicking on "Add Goal". Increments goal counter for selected players
	 */
	public void addGoalSelected() {
		LOGGER.info("addGoalSelected: " + teamSelected);
		for (PlayerMessage p : checkedPlayers) {
			for (TeamStatsMessage ts : p.getTeamStats()) {
				if (ts.getTeamName().name().equals(teamSelected)) {
					ts.setGoals((ts.getGoals() + 1));
				}
			}
		}
		teamSelected = "";
		checkedPlayers.clear();
		numberOfRowSelected = 0;
	}

	/**
	 * Action performed clicking on "Save"
	 */
	public void savePlayers() {
		try {
			StatsManager.removePlayerList(playersToDelete, category.name(), selectedSeason);
			// On ne peut sauvegarder que 24 elements dans une transaction. On va donc creer plusieurs
			// sous-listes si necessaire
			boolean finished = false;
			int index = 0;
			while (!finished) {
				if (players.size() >= index + 24) {
					StatsManager.persistPlayerList(players.subList(index, index + 24), category,
							selectedSeason);
				} else {
					StatsManager.persistPlayerList(players.subList(index, players.size()), category,
							selectedSeason);
					finished = true;
				}
				index += 24;
			}

			playersToDelete.clear();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "",
					"Modifications enregistrées");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Error saving stats: " + e.toString(), e);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur",
					"Impossible d'enregistrer les modifications");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

	}

	public void onToggleSelect(ToggleSelectEvent event) {
		if (event.isSelected()) numberOfRowSelected = players.size();
		else numberOfRowSelected = 0;
	}

	public void onRowSelect(SelectEvent event) {
		numberOfRowSelected++;
	}

	public void onRowUnselect(UnselectEvent event) {
		numberOfRowSelected--;
	}

	public List<PlayerMessage> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerMessage> players) {
		this.players = players;
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	public String getNewPLayerName() {
		return newPLayerName;
	}

	public void setNewPLayerName(String newPLayerName) {
		this.newPLayerName = newPLayerName;
	}

	public List<PlayerPositionMessage> getPositions() {
		return new ArrayList<PlayerPositionMessage>(Arrays.asList(PlayerPositionMessage.values()));
	}

	public List<PlayerMessage> getCheckedPlayers() {
		return checkedPlayers;
	}

	public void setCheckedPlayers(List<PlayerMessage> checkedPlayers) {
		this.checkedPlayers = checkedPlayers;
	}

	public List<PlayerMessage> getPlayersToDelete() {
		return playersToDelete;
	}

	public void setPlayersToDelete(List<PlayerMessage> playersToDelete) {
		this.playersToDelete = playersToDelete;
	}

	public boolean getCheckListEmpty() {
		return checkedPlayers.isEmpty();
	}

	public int getNumberOfRowSelected() {
		return numberOfRowSelected;
	}

	public void setNumberOfRowSelected(int numberOfRowSelected) {
		this.numberOfRowSelected = numberOfRowSelected;
	}

	public String getTeamSelected() {
		return teamSelected;
	}

	public void setTeamSelected(String teamSelected) {
		this.teamSelected = teamSelected;
	}

	public List<TeamNameMessage> getTeamNames() {
		return teamNames;
	}

	public void setTeamNames(List<TeamNameMessage> teamNames) {
		this.teamNames = teamNames;
	}

	public List<String> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<String> seasons) {
		this.seasons = seasons;
	}

	public String getSelectedSeason() {
		return selectedSeason;
	}

	public void setSelectedSeason(String selectedSeason) {
		this.selectedSeason = selectedSeason;
	}

}
