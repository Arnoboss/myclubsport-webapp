package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;

import com.google.api.client.util.Base64;

import eu.boss.mobilesport.datastore.CloudServiceManager;
import eu.boss.mobilesport.exceptions.ImageManagementException;
import eu.boss.mobilesport.externalmodel.ImageDetails;
import eu.boss.mobilesport.externalmodel.TreeNode;
import eu.boss.mobilesport.utils.Constant;

/**
 * Bean for images galleria
 * 
 * @author Arnaud
 */
@ManagedBean
@ViewScoped
public class GalleriaBean implements Serializable {

	private static final long serialVersionUID = 7931725583186784485L;
	private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif))$)";
	private final Pattern pattern = Pattern.compile(IMAGE_PATTERN);

	// Url des images du dossier selectionne
	private List<String> imagesUrl;
	private org.primefaces.model.TreeNode selectedNode;
	private org.primefaces.model.TreeNode root;
	private Document documentToRemove;
	// Arborescence complete du fichier actuellement selectionné
	private String selectedCompleteTree;

	private static final Logger LOGGER = Logger.getLogger(GalleriaBean.class.getName());

	@PostConstruct
	public void init() {
		try {
			initTreeTableMenu(CloudServiceManager.retrieveTreeFolders(Constant.FOLDER_GALLERY));
			// imagesUrl = new ArrayList<String>();
			// initTreeTableMenu(initTestTreeFolders("TEST"));
		} catch (ImageManagementException e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Erreur",
							"Impossible d'afficher les dossiers de photos"));
			LOGGER.severe("Error retrieving files: " + e.getMessage());
			e.printStackTrace();
		}
	}

	// TODO TEST
	private TreeNode<String> initTestTreeFolders(String folder) throws ImageManagementException {
		TreeNode<String> node = new TreeNode<String>(folder);
		TreeNode<String> child1 = new TreeNode<String>("child1");
		TreeNode<String> child2 = new TreeNode<String>("child2");
		TreeNode<String> child3 = new TreeNode<String>("child3");
		child1.addChild(new TreeNode<String>("child1.jpg"));
		child2.addChild(new TreeNode<String>("child2.jpg"));
		child3.addChild(new TreeNode<String>("child3.jpg"));
		child2.addChild(child3);
		node.addChild(child1);
		node.addChild(child2);

		return node;
	}

	/**
	 * Initialise le menu deroulant en fonction des dossiers de google cloud storage
	 * 
	 * @param treeNode
	 */
	private void initTreeTableMenu(TreeNode<String> treeNode) {

		root = new DefaultTreeNode(new Document("gallery", DocumentType.FOLDER), null);

		// First submenu
		for (TreeNode<String> child : treeNode.children) {
			addSubmenuOrItemIn(root, child);
		}
	}

	/**
	 * Teste si un node est une image.
	 * 
	 * @param treeNode
	 *           Objet TreeNode custom, pas celui de Primefaces !!!
	 * @return
	 */
	private boolean isNodeImage(TreeNode<String> treeNode) {
		Matcher matcher = pattern.matcher(treeNode.data);
		return matcher.matches();
	}

	/**
	 * Charge les images du dossier selectionne
	 */
	public void showImagesFromFolder(String folder) {
		LOGGER.info("showImagesFromFolder: " + folder);
		try {
			imagesUrl = CloudServiceManager.retrieveAllFilesFromFolder(folder, false);
			LOGGER.info("imagesUrl size: " + imagesUrl.size());
		} catch (ImageManagementException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "Erreur", "Impossible d'afficher les images"));
			LOGGER.severe("Error retrieving files: " + e.getMessage());
			e.printStackTrace();
		}
	}

	// /////////////////////// Methodes en lien direct avec l'interface

	/**
	 * Ajoute les sous menus ou les items dans le sous menu donne en parametre.
	 * 
	 * @param parentNode
	 */
	private void addSubmenuOrItemIn(org.primefaces.model.TreeNode parentNode,
			TreeNode<String> treeNode) {
		String[] splitted = treeNode.data.split("/");
		org.primefaces.model.TreeNode item;

		if (isNodeImage(treeNode)) {
			item = new DefaultTreeNode(DocumentType.PICTURE.name(),
					new Document(splitted[splitted.length - 1], DocumentType.PICTURE), parentNode);
		} else {
			item = new DefaultTreeNode(DocumentType.FOLDER.name(),
					new Document(splitted[splitted.length - 1], DocumentType.FOLDER), parentNode);
		}

		for (TreeNode<String> child : treeNode.children) {
			addSubmenuOrItemIn(item, child);
		}
	}

	/**
	 * Lorsqu'un node est selectionne, on met a jour la lightbox pour afficher les images qu'il
	 * contient. On récupére l'arborescence totale du treenode sélectionné. Cela permet de supprimer
	 * le fichier dans cloud storage
	 * 
	 * @param event
	 */
	public void onNodeSelect(NodeSelectEvent event) {
		if (event.getTreeNode().getType().equals(DocumentType.FOLDER.name())) {
			LOGGER.info("node selected: " + event.getTreeNode().getData());
			selectedCompleteTree = findCompleteParentTree(event.getTreeNode());
			showImagesFromFolder(selectedCompleteTree);
			// imagesUrl
			// .add("https://storage.googleapis.com/ascoteaux.appspot.com/news/convocs.png");
		} else {
			showImagesFromFolder(findCompleteParentTree(event.getTreeNode().getParent()));
		}
	}

	/**
	 * Retourne toute l'arborescence des parents
	 */
	private String findCompleteParentTree(org.primefaces.model.TreeNode node) {
		if (node.getParent() != null)
			return (findCompleteParentTree(node.getParent()) + "/" + node.getParent().toString());
		else return "";

	}

	/**
	 * upload les fichiers
	 * 
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		Matcher matcher = pattern.matcher(event.getFile().getFileName());

		if (matcher.matches()) {
			FacesMessage msg = new FacesMessage("Erreur",
					"Veuillez choisir une image au format .jpg ou .png");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			ImageDetails image = new ImageDetails();
			image.setImageAsBytes(Base64.encodeBase64(event.getFile().getContents()));
			image.setImageName(event.getFile().getFileName());
			image.setImageMimeType(event.getFile().getContentType());
			try {
				CloudServiceManager.uploadImage(image, Constant.FOLDER_GALLERY2);

				FacesMessage message = new FacesMessage("Import terminé",
						event.getFile().getFileName() + " a été importé avec succès.");
				FacesContext.getCurrentInstance().addMessage(null, message);
			} catch (ImageManagementException e) {
				e.printStackTrace();
			}
		}
	}

	public void removeSelectedFile() {
		LOGGER.info("Delete " + documentToRemove.getName());
		try {
			CloudServiceManager.removefile(selectedCompleteTree, Constant.FOLDER_GALLERY2);
		} catch (ImageManagementException e) {
			LOGGER.severe("Erreur lors de la suppression du fichier " + e.toString());
			FacesMessage message = new FacesMessage("Erreur ",
					"Erreur lors de la suppression du fichier" + documentToRemove.getName());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public List<String> getImagesUrl() {
		return imagesUrl;
	}

	public org.primefaces.model.TreeNode getRoot() {
		return root;
	}

	public void setRoot(org.primefaces.model.TreeNode root) {
		this.root = root;
	}

	public org.primefaces.model.TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(org.primefaces.model.TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Document getDocumentToRemove() {
		return documentToRemove;
	}

	public void setDocumentToRemove(Document documentToRemove) {
		this.documentToRemove = documentToRemove;
	}

}
