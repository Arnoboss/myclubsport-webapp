package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.exceptions.ImageManagementException;
import eu.boss.mobilesport.model.News;

@ManagedBean
@ViewScoped
public class NewsBean implements Serializable {

	private static final Logger LOGGER = Logger.getLogger(NewsBean.class
			.getName());
	private static final long serialVersionUID = 3279808711526791986L;
	private List<News> newsList;
	private News selectedNews;

	/**
	 * Action performed clicking on "Delete"
	 */
	@PostConstruct
	private void init() {
		newsList = NewsManager.getNews();
	}

	public void deleteSelected() {
		try {
			NewsManager.deleteNews(selectedNews.getId());
		} catch (ImageManagementException e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		newsList.remove(selectedNews);
	}

	public List<News> getNewsList() {
		return newsList;
	}

	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}

	public News getSelectedNews() {
		return selectedNews;
	}

	public void setSelectedNews(News selectedNews) {
		this.selectedNews = selectedNews;
	}

}
