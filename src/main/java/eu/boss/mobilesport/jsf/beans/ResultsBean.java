package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.fff.FFFWeb;

@ManagedBean
@RequestScoped
public class ResultsBean implements Serializable {

	private static final long serialVersionUID = -7962016450499026409L;
	private static final Logger LOGGER = Logger.getLogger(ResultsBean.class.getName());

	private String htmlResults;

	public String getHtmlResults() {
		try {
			LOGGER.info("Loading Results");
			if (htmlResults == null) htmlResults = FFFWeb.loadResults(false);
		} catch (BossException e) {
			htmlResults = "<p>Erreur de chargement de la page. Veuillez réessayer</p>";
			LOGGER.severe(e.getMessage());
		}
		return htmlResults;
	}

	public void setHtmlResults(String htmlResults) {
		this.htmlResults = htmlResults;
	}

}
