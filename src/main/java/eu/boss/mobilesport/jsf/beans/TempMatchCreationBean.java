package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import eu.boss.mobilesport.externalmodel.FormationMessage;
import eu.boss.mobilesport.model.Match;

/**
 * Bean utilise lors de la creation d'un match live. IL permet de stocker temporairement les donnees
 * d'un match lorsqu'on passe a l'etape de "composition d'equipe" sans tout sauvegarder en base
 */
@ManagedBean
@SessionScoped
public class TempMatchCreationBean implements Serializable {

	private static final long serialVersionUID = -5431196862498608614L;
	private Match match;
	private FormationMessage formation;

	public Match getMatch() {
		return match;
	}

	public void setMatch(Match match) {
		this.match = match;
	}

	public FormationMessage getFormation() {
		return formation;
	}

	public void setFormation(FormationMessage formation) {
		this.formation = formation;
	}

}
