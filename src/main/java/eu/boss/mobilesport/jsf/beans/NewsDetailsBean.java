package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.exceptions.ImageManagementException;
import eu.boss.mobilesport.model.News;

@ManagedBean
@ViewScoped
public class NewsDetailsBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8669244812730098783L;

	private static final Logger LOGGER = Logger.getLogger(NewsDetailsBean.class.getName());
	private News news;

	public String editNews() {
		return "newsManager?faces-redirect=true&edit=true&id=" + news.getId();
	}

	public String delete() {
		try {
			NewsManager.deleteNews(news.getId());
		} catch (ImageManagementException e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		return "news";
	}

	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		Long newsId = null;

		if (params.get("newsId") != null) {
			newsId = Long.valueOf(params.get("newsId"));
			this.news = NewsManager.getNewsById(newsId);
		} else {
			LOGGER.severe("Id incorrect");
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Id news incorrect");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

}
