package eu.boss.mobilesport.jsf.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.TeamMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.utils.Util;

/**
 * Bean permettant de gerer la liste des equipes et des categories.
 * 
 * @author Arnaud
 */
@ManagedBean
@ViewScoped
public class TeamManagementBean implements Serializable {

	private static final long serialVersionUID = 9063932878687916239L;
	private static final Logger LOGGER = Logger.getLogger(TeamManagementBean.class.getName());

	List<TeamMessage> teamList = new ArrayList<>();
	List<TeamMessage> checkedTeamList = new ArrayList<>();
	List<TeamMessage> teamsToDelete = new ArrayList<>();
	private int numberOfRowSelected = 0;

	private String teamCat;
	private String teamName;

	private List<String> seasons = Util.getSeasonsList();
	private String selectedSeason = Util.currentSeasonDate();

	@PostConstruct
	public void loadTeams() {
		teamList = TeamsManager.getAllTeamsForSeason(true, selectedSeason);
	}

	public void saveTeams() {
		try {
			TeamsManager.removeTeamListForSeason(teamsToDelete, selectedSeason);
			// On ne peut sauvegarder que 24 elements dans une transaction. On va donc creer plusieurs
			// sous-listes si necessaire
			boolean finished = false;
			int index = 0;
			while (!finished) {
				if (teamList.size() >= index + 24) {
					TeamsManager.persistTeamListForSeason(teamList.subList(index, index + 24),
							selectedSeason);
				} else {
					TeamsManager.persistTeamListForSeason(teamList.subList(index, teamList.size()),
							selectedSeason);
					finished = true;
				}
				index += 24;
			}

			teamsToDelete.clear();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "",
					"Modifications enregistrées");
			FacesContext.getCurrentInstance().addMessage(null, msg);
			// On invalide le bean contenant la liste des equipes car c'est un bean de session
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("teamListBean",
					null);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Error saving stats: " + e.toString(), e);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur",
					"Impossible d'enregistrer les modifications");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void addTeam() {
		TeamMessage team = new TeamMessage();
		team.setId(new Long(team.hashCode()));
		team.setCategory(Category.U7.name());
		team.setTeamName(TeamNameMessage.A.name());
		teamList.add(team);
	}

	/**
	 * Action performed clicking on "Delete": delete selected players
	 */
	public void deleteSelected() {
		LOGGER.info("*deleteSelected*");
		LOGGER.info("*checkedTeamList size: " + checkedTeamList.size());
		for (TeamMessage team : checkedTeamList) {
			teamList.remove(team);
			teamsToDelete.add(team);
		}
		checkedTeamList.clear();
	}

	public void onToggleSelect(ToggleSelectEvent event) {
		if (event.isSelected()) numberOfRowSelected = teamList.size();
		else numberOfRowSelected = 0;
	}

	public void onRowSelect(SelectEvent event) {
		numberOfRowSelected++;
	}

	public void onRowUnselect(UnselectEvent event) {
		numberOfRowSelected--;
	}

	public List<TeamMessage> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<TeamMessage> teamList) {
		this.teamList = teamList;
	}

	public List<TeamMessage> getCheckedTeamList() {
		return checkedTeamList;
	}

	public void setCheckedTeamList(List<TeamMessage> checkedTeamList) {
		this.checkedTeamList = checkedTeamList;
	}

	public int getNumberOfRowSelected() {
		return numberOfRowSelected;
	}

	public void setNumberOfRowSelected(int numberOfRowSelected) {
		this.numberOfRowSelected = numberOfRowSelected;
	}

	public List<Category> getCategories() {
		return new ArrayList<Category>(Arrays.asList(Category.values()));
	}

	public List<TeamNameMessage> getTeamNames() {
		return new ArrayList<TeamNameMessage>(Arrays.asList(TeamNameMessage.values()));
	}

	public List<TeamMessage> getTeamsToDelete() {
		return teamsToDelete;
	}

	public void setTeamsToDelete(List<TeamMessage> teamsToDelete) {
		this.teamsToDelete = teamsToDelete;
	}

	public String getTeamCat() {
		return teamCat;
	}

	public void setTeamCat(String teamCat) {
		this.teamCat = teamCat;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<String> getSeasons() {
		return seasons;
	}

	public void setSeasons(List<String> seasons) {
		this.seasons = seasons;
	}

	public String getSelectedSeason() {
		return selectedSeason;
	}

	public void setSelectedSeason(String selectedSeason) {
		this.selectedSeason = selectedSeason;
	}

}
