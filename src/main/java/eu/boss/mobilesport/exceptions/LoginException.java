package eu.boss.mobilesport.exceptions;

/**
 * Login Exception class
 * 
 * @author Arnaud
 */
public class LoginException extends BossException {

	private static final long serialVersionUID = -644212663974708954L;

	public LoginException() {
		super();
	}

	public LoginException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

}
