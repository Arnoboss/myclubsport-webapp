package eu.boss.mobilesport.exceptions;

/**
 * Database Exception class
 * 
 * @author Arnaud
 */
public class DatabaseException extends BossException {

	private static final long serialVersionUID = 6160261095503572646L;

	public DatabaseException() {
		super();
	}

	public DatabaseException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

}
