package eu.boss.mobilesport.exceptions;

/**
 * Generic Exception class
 * 
 * @author Arnaud
 */
public class BossException extends Exception {

	private static final long serialVersionUID = 6405603486867134706L;

	protected String errorCode;

	public BossException() {
		super();
	}

	public BossException(String message) {
		super(message);
	}

	public BossException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
