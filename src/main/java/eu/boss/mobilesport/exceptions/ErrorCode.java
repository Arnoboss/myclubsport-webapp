package eu.boss.mobilesport.exceptions;

public class ErrorCode {

	public final static String UNEXPECTED_ERROR = "0";
	public final static String TOKEN_EXPIRED = "10";
	public final static String LOGIN_FAILED = "11";
	public final static String DATABASE_ERROR = "20";
	public final static String UPLOAD_IMAGE_ERROR = "801";
	public final static String UPLOAD_IMAGE_ERROR_MSG = "Error during image upload";
	public final static String MISSING_IMAGE = "Image is not defined";
	public final static String INVALID_PARAMS = "901";
	public final static String JSON_PARSING_ERRORCODE = "900";
	public final static String JSON_PARSING_MESSAGE = "Error parsing JSON result";

	public final static String OPERATION_SUCCESS = "SUCCESS";
	public final static String OPERATION_FAILED = "FAILED";
}
