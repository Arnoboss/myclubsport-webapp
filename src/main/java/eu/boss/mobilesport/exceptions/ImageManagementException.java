package eu.boss.mobilesport.exceptions;

/**
 * Image saving Exception class
 * 
 * @author Arnaud
 */
public class ImageManagementException extends BossException {

	private static final long serialVersionUID = -644212663974708954L;

	public ImageManagementException() {
		super();
	}

	public ImageManagementException(String message) {
		super(message);
	}

	public ImageManagementException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

}
