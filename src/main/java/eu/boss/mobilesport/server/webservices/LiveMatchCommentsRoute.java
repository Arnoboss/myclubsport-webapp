package eu.boss.mobilesport.server.webservices;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.converters.CommentConverter;
import eu.boss.mobilesport.converters.MatchConverter;
import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.datastore.LoginManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.exceptions.ErrorCode;
import eu.boss.mobilesport.externalmodel.CommentMessage;
import eu.boss.mobilesport.externalmodel.GenericAuthRequestMessage;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.MatchMessage;
import eu.boss.mobilesport.externalmodel.OperationResultMessage;
import eu.boss.mobilesport.model.Comment;
import eu.boss.mobilesport.model.Match;
import eu.boss.mobilesport.pushnotifications.NotificationType;
import eu.boss.mobilesport.pushnotifications.PushNotificationManager;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on comments
 * 
 * @author Arnaud
 */
public class LiveMatchCommentsRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(LiveMatchCommentsRoute.class.getName());

	/**
	 * On recupere les commentaires + les details du matchs car ils contiennent les details (scores,
	 * buteurs, ...) a afficher dans l'appli
	 * 
	 * @throws Exception
	 */
	@Get
	public String getDetailsForMatch() throws Exception {
		String matchId = getQuery().getValues("matchId");
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<MatchMessage> msg;
		boolean includeCatAndTeam = false;
		if (getQuery().getValues("includeCatAndTeam") != null
				&& getQuery().getValues("includeCatAndTeam").equals("true")) includeCatAndTeam = true;
		try {
			Long id = Long.valueOf(matchId);
			if (id != null && id != 0) {
				MatchMessage match = MatchConverter.toExternalModel(LiveManager.getMatchById(id),
						includeCatAndTeam);
				match.setComments(CommentConverter.toExternalModel(LiveManager.getCommentsForMatch(id))
						.getComments());
				msg = new GenericResponseMessage<MatchMessage>(match);
				return mapper.writeValueAsString(msg);
			} else {
				msg = new GenericResponseMessage<MatchMessage>(null);
				msg.setErrorCode(ErrorCode.INVALID_PARAMS);
				msg.setExceptionMessage("No match Id defined");
			}
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Exception getting match details:", e);
			msg = new GenericResponseMessage<MatchMessage>(null);
			msg.setErrorCode(ErrorCode.JSON_PARSING_ERRORCODE);
			msg.setExceptionMessage("Les données envoyées sont incorrectes");
		}
		return mapper.writeValueAsString(msg);
	}

	@Put
	public String updateComment(String matchStr) throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<OperationResultMessage> msg;
		try {
			GenericAuthRequestMessage<MatchMessage> genericMsg = mapper.readValue(
					URLDecoder.decode(matchStr, "UTF-8"),
					new TypeReference<GenericAuthRequestMessage<MatchMessage>>() {
					});
			LoginManager.loginWithToken(genericMsg.getTokenId());
			MatchMessage matchMsg = genericMsg.getPayLoad();
			Match match = MatchConverter.toInternalModel(matchMsg);
			LiveManager.updateMatch(match);
			LiveManager.updateComment(matchMsg.getId(),
					new ArrayList<CommentMessage>(matchMsg.getComments()).get(0));
			msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
					ErrorCode.OPERATION_SUCCESS));

		} catch (BossException e) {
			LOGGER.log(Level.WARNING, "Exception updating match:", e);
			msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
					ErrorCode.OPERATION_FAILED));
			msg.setErrorCode(e.getErrorCode());
			msg.setExceptionMessage(e.getMessage());

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Exception updating match:", e);
			msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
					ErrorCode.OPERATION_FAILED));
			msg.setErrorCode(ErrorCode.JSON_PARSING_ERRORCODE);
			msg.setExceptionMessage("Les données envoyées sont incorrectes");
		}

		return mapper.writeValueAsString(msg);
	}

	@Post
	public String addComment(String commentStr) throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<OperationResultMessage> msg;
		LOGGER.info("Comment received: " + commentStr);
		LOGGER.info("Comment received decoded: " + URLDecoder.decode(commentStr, "UTF-8"));
		try {
			GenericAuthRequestMessage<MatchMessage> genericMsg = mapper.readValue(
					URLDecoder.decode(commentStr, "UTF-8"),
					new TypeReference<GenericAuthRequestMessage<MatchMessage>>() {
					});
			LoginManager.loginWithToken(genericMsg.getTokenId());
			// TODO Mettre un lock pour qu'un seul utilisateur puisse ajouter des commentaires par
			// match
			MatchMessage matchMsg = genericMsg.getPayLoad();
			ArrayList<CommentMessage> comments = new ArrayList<CommentMessage>(matchMsg.getComments());
			CommentMessage commentMsg = comments.get(0);
			Comment comment = CommentConverter.toInternalModel(commentMsg);
			if (commentMsg != null && matchMsg.getId() != 0) {
				// Update match et commentaire en base
				LiveManager.addComment(matchMsg.getId(), comment);
				LiveManager.updateMatch(MatchConverter.toInternalModel(matchMsg));

				// Envoi d'une notification si le flag est e true
				if (commentMsg.isNotify()) PushNotificationManager.sendNotificationMsgToAll(
						matchMsg.writeScoreSynthese(), comment.getContent(), NotificationType.LIVE, "",
						"" + matchMsg.getId());
				msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
						ErrorCode.OPERATION_SUCCESS));
			} else {
				msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
						ErrorCode.OPERATION_FAILED));
				msg.setErrorCode(ErrorCode.INVALID_PARAMS);
				msg.setExceptionMessage("No match Id defined");
			}

		} catch (BossException e) {
			LOGGER.log(Level.WARNING, "Exception addComment match:", e);
			msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
					ErrorCode.OPERATION_FAILED));
			msg.setErrorCode(e.getErrorCode());
			msg.setExceptionMessage(e.getMessage());

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Exception addComment match:", e);
			msg = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
					ErrorCode.OPERATION_FAILED));
			msg.setErrorCode(ErrorCode.JSON_PARSING_ERRORCODE);
			msg.setExceptionMessage("Les données envoyées sont incorrectes");
		}

		return mapper.writeValueAsString(msg);
	}
}
