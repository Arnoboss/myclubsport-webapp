package eu.boss.mobilesport.server.webservices;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.FormationManager;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on a list of players
 * 
 * @author Arnaud
 */
public class PlayersForMatchRoute extends ServerResource {

	@Get
	public String getPlayersList() throws Exception {
		String matchId = getQuery().getValues("matchId");
		ObjectMapper mapper = JSON.defaultView();
		List<String> playerList = FormationManager.loadPlayerListForMatch(Long.valueOf(matchId));
		return mapper.writeValueAsString(new GenericResponseMessage<List<String>>(playerList));

	}

	@Put
	public String updatePlayerList() {
		// TODO
		return "Todo";
	}

	@Post
	public String postPlayerList() {
		// TODO
		return "Todo";
	}

}
