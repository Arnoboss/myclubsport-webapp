package eu.boss.mobilesport.server.webservices;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.HtmlMessage;
import eu.boss.mobilesport.fff.FFFWeb;
import eu.boss.mobilesport.server.JSON;

/**
 * Route returning ranking
 * 
 * @author Arnaud
 */
public class ResultsRoute extends ServerResource {

	@Get
	public String getResults() throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<HtmlMessage> response = new GenericResponseMessage<HtmlMessage>(null);
		HtmlMessage htmlMsg = new HtmlMessage();
		try {
			htmlMsg.setHtmlContent(FFFWeb.loadResults(true));
			response.setPayLoad(htmlMsg);
		} catch (BossException e) {
			response.setErrorCode(e.getErrorCode());
			response.setExceptionMessage(e.getMessage());
		}

		return mapper.writeValueAsString(response);
	}

}
