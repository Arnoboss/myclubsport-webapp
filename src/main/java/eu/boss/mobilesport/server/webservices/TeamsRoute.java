package eu.boss.mobilesport.server.webservices;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.TeamListMessage;
import eu.boss.mobilesport.server.JSON;
import eu.boss.mobilesport.utils.Util;

public class TeamsRoute extends ServerResource {

	/**
	 * Web service getting all teams. TESTED OK
	 * 
	 * @return
	 * @throws Exception
	 */
	@Get
	public String getTeamsAndCategories() throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<TeamListMessage> response = new GenericResponseMessage<TeamListMessage>(
				null);
		TeamListMessage teamList = new TeamListMessage();
		teamList.setTeams(TeamsManager.getAllTeamsForSeason(true, Util.currentSeasonDate()));
		response.setPayLoad(teamList);

		return mapper.writeValueAsString(response);
	}
}
