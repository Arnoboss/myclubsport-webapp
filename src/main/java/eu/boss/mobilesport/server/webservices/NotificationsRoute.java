package eu.boss.mobilesport.server.webservices;

import java.net.URLDecoder;
import java.util.logging.Logger;

import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.converters.NotificationIdConverter;
import eu.boss.mobilesport.datastore.NotificationIdManager;
import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.exceptions.ErrorCode;
import eu.boss.mobilesport.externalmodel.GenericAuthRequestMessage;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.NotificationIdMessage;
import eu.boss.mobilesport.externalmodel.OperationResultMessage;
import eu.boss.mobilesport.externalmodel.TeamListMessage;
import eu.boss.mobilesport.server.JSON;
import eu.boss.mobilesport.utils.Util;

/**
 * Web service appele lors du demarrage de l'application. Il sert e s'enregistrer pour les
 * notifications, e mettre e jour les parametres (acceptation de certaines notifs). En retour, le
 * serveur renvoie la liste des equipes
 * 
 * @author Arnaud
 */
public class NotificationsRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(NotificationsRoute.class.getName());

	@Put()
	public String addNotificationId(String notificationIdJson) throws Exception {
		LOGGER.info("Notification JSON ID: " + notificationIdJson);
		ObjectMapper mapper = JSON.defaultView();
		NotificationIdMessage notificationId;
		GenericAuthRequestMessage<NotificationIdMessage> genericMsg = mapper.readValue(
				URLDecoder.decode(notificationIdJson, "UTF-8"),
				new TypeReference<GenericAuthRequestMessage<NotificationIdMessage>>() {
				});

		notificationId = genericMsg.getPayLoad();
		NotificationIdManager
				.persistNotificationId(NotificationIdConverter.toInternalModel(notificationId));

		GenericResponseMessage<OperationResultMessage> response = new GenericResponseMessage<OperationResultMessage>(
				new OperationResultMessage(ErrorCode.OPERATION_SUCCESS));
		return mapper.writeValueAsString(response);

	}

	/**
	 * Route pour la V2: liste dynamique des equipes
	 * 
	 * @param notificationIdJson
	 * @return
	 * @throws Exception
	 */
	@Post()
	public String addNotificationIdAndGetTeamList(String notificationIdJson) throws Exception {
		LOGGER.info("Notification JSON ID: " + notificationIdJson);
		ObjectMapper mapper = JSON.defaultView();
		NotificationIdMessage notificationId;
		GenericAuthRequestMessage<NotificationIdMessage> genericMsg = mapper.readValue(
				URLDecoder.decode(notificationIdJson, "UTF-8"),
				new TypeReference<GenericAuthRequestMessage<NotificationIdMessage>>() {
				});

		notificationId = genericMsg.getPayLoad();
		try {
			NotificationIdManager
					.persistNotificationId(NotificationIdConverter.toInternalModel(notificationId));
		} catch (Exception e) {
			LOGGER.warning("Unable to save notification ID: ID might be null (see previous log info)");
		}

		TeamListMessage teamList = new TeamListMessage();
		teamList.setTeams(TeamsManager.getAllTeamsForSeason(true, Util.currentSeasonDate()));
		GenericResponseMessage<TeamListMessage> response = new GenericResponseMessage<TeamListMessage>(
				teamList);
		return mapper.writeValueAsString(response);

	}
}
