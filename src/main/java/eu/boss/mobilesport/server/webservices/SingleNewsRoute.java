package eu.boss.mobilesport.server.webservices;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.converters.NewsConverter;
import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.NewsMessage;
import eu.boss.mobilesport.model.News;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on a list of players
 * 
 * @author Arnaud
 */
public class SingleNewsRoute extends ServerResource {

	@Get
	public String getNews() throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		String newsId = getQuery().getValues("id");
		News news = NewsManager.getNewsById(Long.valueOf(newsId));
		NewsMessage newsMsg = NewsConverter.toExternalModel(news);
		GenericResponseMessage<NewsMessage> response = new GenericResponseMessage<NewsMessage>(
				newsMsg);
		return mapper.writeValueAsString(response);

	}

	@Put
	public String updateNews() {
		// TODO
		return "Todo";
	}

	@Post
	public String postNews() {
		// TODO
		return "Todo";
	}

}
