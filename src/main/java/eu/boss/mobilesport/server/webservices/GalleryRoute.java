package eu.boss.mobilesport.server.webservices;

import java.net.URLDecoder;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.CloudServiceManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.externalmodel.GalleryResponseMessage;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.server.JSON;
import eu.boss.mobilesport.utils.Constant;

/**
 * Route used to get or put image from gallery
 * 
 * @author Arnaud
 */
public class GalleryRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(GalleryRoute.class.getName());

	@Get
	public String getNodeElements() throws Exception {
		// Si folderPath est vide, ca signifie qu'on demande l'arborescence du dossier gallery
		String folderPath = null;
		if (getQuery().getValues("folderPath") != null
				&& !getQuery().getValues("folderPath").equals("")) {
			folderPath = URLDecoder.decode(getQuery().getValues("folderPath"), "UTF-8");
		}
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<GalleryResponseMessage> response = new GenericResponseMessage<GalleryResponseMessage>(
				null);
		try {
			GalleryResponseMessage responseMsg = new GalleryResponseMessage();
			if (folderPath != null && !folderPath.equals("")) {
				responseMsg.setImagesUrl(CloudServiceManager.retrieveAllFilesFromFolder(folderPath,
						true));
			} else responseMsg.setPathTreeNode(CloudServiceManager
					.retrieveTreeFolders(Constant.FOLDER_GALLERY));

			response.setPayLoad(responseMsg);

		} catch (BossException e) {
			response.setErrorCode(e.getErrorCode());
			response.setExceptionMessage(e.getMessage());
			LOGGER.severe("Erreur login route: " + e.toString());
		}

		return (mapper.writeValueAsString(response));
	}

}
