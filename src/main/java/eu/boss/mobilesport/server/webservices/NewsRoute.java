package eu.boss.mobilesport.server.webservices;

import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.CloudServiceManager;
import eu.boss.mobilesport.datastore.LoginManager;
import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.exceptions.ErrorCode;
import eu.boss.mobilesport.externalmodel.GenericAuthRequestMessage;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.NewsListMessage;
import eu.boss.mobilesport.externalmodel.NewsRequestMessage;
import eu.boss.mobilesport.externalmodel.NewsType;
import eu.boss.mobilesport.externalmodel.OperationResultMessage;
import eu.boss.mobilesport.model.News;
import eu.boss.mobilesport.pushnotifications.NotificationType;
import eu.boss.mobilesport.pushnotifications.PushNotificationManager;
import eu.boss.mobilesport.server.JSON;
import eu.boss.mobilesport.utils.Constant;

/**
 * Route for operations on a list of players
 * 
 * @author Arnaud
 */
public class NewsRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(NewsRoute.class.getName());

	@Get
	public String getNewsList() throws Exception {
		ObjectMapper mapper = JSON.defaultView();

		List<News> news = NewsManager.getNews();
		NewsListMessage newsList = new NewsListMessage();
		newsList.setNews2(news);
		GenericResponseMessage<NewsListMessage> response = new GenericResponseMessage<NewsListMessage>(
				newsList);
		return mapper.writeValueAsString(response);

	}

	@Put
	public String updateNewsList() {
		// TODO
		return "Todo";
	}

	/**
	 * Post a News from web service
	 * 
	 * @return
	 */
	@Post
	public String postNews(String newsStr) throws Exception {

		LOGGER.info("News received from app: " + newsStr);
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<OperationResultMessage> msg;
		GenericAuthRequestMessage<NewsRequestMessage> genericMsg = mapper.readValue(
				URLDecoder.decode(newsStr, "UTF-8"),
				new TypeReference<GenericAuthRequestMessage<NewsRequestMessage>>() {
				});

		try {
			LoginManager.loginWithToken(genericMsg.getTokenId());
			NewsRequestMessage newsMsg = genericMsg.getPayLoad();
			if (newsMsg.getImage() != null && newsMsg.getImage().getImageAsBytes() != null) {
				if (newsMsg.getNewsType() == NewsType.AUTRE) {
					newsMsg.setImageUrl(
							CloudServiceManager.uploadImage(newsMsg.getImage(), Constant.FOLDER_NEWS));
				} else throw new BossException(ErrorCode.UPLOAD_IMAGE_ERROR, ErrorCode.MISSING_IMAGE);
			} else {
				if (newsMsg.getNewsType() == NewsType.AGENDA)
					newsMsg.setImageUrl(Constant.AGENDA_IMG_URL);
				else if (newsMsg.getNewsType() == NewsType.CONVOCATION)
					newsMsg.setImageUrl(Constant.CONVOCS_IMG_URL);
				else if (newsMsg.getNewsType() == NewsType.RESULTAT)
					newsMsg.setImageUrl(Constant.RESULTS_IMG_URL);
			}

			News news = new News(newsMsg.getTitle(), new Date(), newsMsg.getImageUrl(), "",
					newsMsg.getContent().replaceAll("\\<[^>]*>", "").replace("\n", "<br>"),
					newsMsg.getNewsType());
			if (NewsManager.addNews(news) == null) {
				LOGGER.severe("Error saving news");
				msg = new GenericResponseMessage<OperationResultMessage>(
						new OperationResultMessage(ErrorCode.OPERATION_FAILED));
				msg.setErrorCode(ErrorCode.UNEXPECTED_ERROR);
				msg.setExceptionMessage("Erreur lors de l'enregistrement de la news");
			} else {
				if (newsMsg.isNotify())
					PushNotificationManager.sendNotificationMsgToAll(newsMsg.getTitle(),
							newsMsg.getContent(), NotificationType.NEWS, newsMsg.getImageUrl(),
							"" + news.getId());
				msg = new GenericResponseMessage<OperationResultMessage>(
						new OperationResultMessage(ErrorCode.OPERATION_SUCCESS));
			}
		} catch (BossException e) {
			msg = new GenericResponseMessage<OperationResultMessage>(
					new OperationResultMessage(ErrorCode.OPERATION_FAILED));
			msg.setErrorCode(e.getErrorCode());
			msg.setExceptionMessage(e.getMessage());
		}

		LOGGER.info("Posted response: " + msg.getPayLoad().getOperationResult());
		return mapper.writeValueAsString(msg);
	}
}
