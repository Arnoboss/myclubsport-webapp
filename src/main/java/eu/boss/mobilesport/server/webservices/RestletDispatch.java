package eu.boss.mobilesport.server.webservices;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class RestletDispatch extends Application {

	/**
	 * Creates a root Restlet that will receive all incoming calls.
	 */
	@Override
	public Restlet createInboundRoot() {
		// Create a router Restlet that routes each call to a
		// new instance of HelloWorldResource.
		Router router = new Router(getContext());

		// Defines routes
		router.attach("/players", PlayersRoute.class);
		router.attach("/player", PlayerRoute.class);
		router.attach("/ranking", RankingRoute.class);
		router.attach("/notifications", NotificationsRoute.class);
		router.attach("/agenda", AgendaRoute.class);
		router.attach("/calendar", CalendarRoute.class);
		router.attach("/results", ResultsRoute.class);
		router.attach("/news", NewsRoute.class);
		router.attach("/liveMatch", LiveListRoute.class);
		router.attach("/liveMatchComments", LiveMatchCommentsRoute.class);
		router.attach("/login", LoginRoute.class);
		router.attach("/teams", TeamsRoute.class);
		router.attach("/playersForMatch", PlayersForMatchRoute.class);
		router.attach("/singleNews", SingleNewsRoute.class);
		router.attach("/gallery", GalleryRoute.class);
		return router;
	}

}
