package eu.boss.mobilesport.server.webservices;

import java.util.List;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.MatchListMessage;
import eu.boss.mobilesport.model.Match;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on match
 * 
 * @author Arnaud
 */
public class LiveListRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(LiveListRoute.class.getName());

	@Get
	public String getMatchList() throws Exception {
		// On regarde si un parametre "date" est passe dans la requete. Si c'est le cas, on recupere
		// les
		// matchs joues e la date donnee.
		String date = getQuery().getValues("date");
		// Le parametre maxMatch est utilise pour limiter le nombre maximum de matchs e charger. 10
		// par defaut
		String nbMaxMatchToLoad = getQuery().getValues("maxMatch");

		boolean includeCatAndTeam = false;
		if (getQuery().getValues("includeCatAndTeam") != null
				&& getQuery().getValues("includeCatAndTeam").equals("true")) includeCatAndTeam = true;

		int maxMatch = 10;
		try {
			if (nbMaxMatchToLoad != null) maxMatch = Integer.valueOf(nbMaxMatchToLoad);
		} catch (Exception e) {
			// on s'en fout, on aura 10 par defaut
		}
		ObjectMapper mapper = JSON.defaultView();

		List<Match> matchs;
		if (date == null) matchs = LiveManager.getNextMatchs();
		else matchs = LiveManager.getLastMatchsBeforeDate(date, maxMatch);
		MatchListMessage matchList = new MatchListMessage();
		matchList.setMatch2(matchs, includeCatAndTeam);
		GenericResponseMessage<MatchListMessage> response = new GenericResponseMessage<MatchListMessage>(
				matchList);

		return mapper.writeValueAsString(response);
	}

	// TODO Test et gerer exceptions
	// @Put
	// public String updateMatch(String matchStr) throws Exception {
	// ObjectMapper mapper = JSON.defaultView();
	// GenericResponseMessage<OperationResultMessage> response = new
	// GenericResponseMessage<OperationResultMessage>(
	// null);
	// try {
	// GenericAuthRequestMessage<MatchMessage> genericMsg = mapper.readValue(
	// URLDecoder.decode(matchStr, "UTF-8"),
	// new TypeReference<GenericAuthRequestMessage<MatchMessage>>() {
	// });
	//
	// MatchMessage matchMsg = genericMsg.getPayLoad();
	// Match match = MatchConverter.toInternalModel(matchMsg);
	// LiveManager.updateMatch(match);
	// } catch (Exception e) {
	// LOGGER.log(Level.WARNING, "Exception updating match :", e);
	// response = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
	// ErrorCode.OPERATION_FAILED));
	// response.setErrorCode(ErrorCode.JSON_PARSING_ERRORCODE);
	// response.setExceptionMessage("Les donnees envoyees sont incorrectes");
	// }
	//
	// return mapper.writeValueAsString(response);
	// }
	//
	// // TODO Test
	// @Post
	// public String addMatch(String matchStr) throws Exception {
	// ObjectMapper mapper = JSON.defaultView();
	// GenericResponseMessage<OperationResultMessage> response = new
	// GenericResponseMessage<OperationResultMessage>(
	// null);
	// try {
	// GenericAuthRequestMessage<MatchMessage> genericMsg = mapper.readValue(
	// URLDecoder.decode(matchStr, "UTF-8"),
	// new TypeReference<GenericAuthRequestMessage<MatchMessage>>() {
	// });
	// MatchMessage matchMsg = genericMsg.getPayLoad();
	// LiveManager.addNewMatch(MatchConverter.toInternalModel(matchMsg));
	// response = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
	// ErrorCode.OPERATION_SUCCESS));
	// } catch (Exception e) {
	// LOGGER.log(Level.WARNING, "Exception adding match :", e);
	// response = new GenericResponseMessage<OperationResultMessage>(new OperationResultMessage(
	// ErrorCode.OPERATION_FAILED));
	// response.setErrorCode(ErrorCode.JSON_PARSING_ERRORCODE);
	// response.setExceptionMessage("Les donnees envoyees sont incorrectes");
	// }
	//
	// return mapper.writeValueAsString(response);
	// }

}
