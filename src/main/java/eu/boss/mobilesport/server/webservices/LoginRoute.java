package eu.boss.mobilesport.server.webservices;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.logging.Logger;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.LoginManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.externalmodel.GenericAuthRequestMessage;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.LoginMessage;
import eu.boss.mobilesport.server.JSON;

/**
 * Route used for login. Not in HTTPS because it is not possible for free in app engine
 * 
 * @author Arnaud
 */
public class LoginRoute extends ServerResource {

	private static final Logger LOGGER = Logger.getLogger(LoginRoute.class.getName());

	/**
	 * Dans cette requete de login, on doit specifier des identifiants. Si on ajoute un tokenId dans
	 * la requete, celui-ci sera invalide s'il existe.
	 * 
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonGenerationException
	 */
	@Post
	public String postLoginCredentials(String value) throws Exception {
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<LoginMessage> response = new GenericResponseMessage<LoginMessage>(null);

		LoginMessage login;
		try {
			GenericAuthRequestMessage<LoginMessage> genericMsg = mapper.readValue(
					URLDecoder.decode(value, "UTF-8"),
					new TypeReference<GenericAuthRequestMessage<LoginMessage>>() {
					});
			login = genericMsg.getPayLoad();
			if ((login.getLogin() != null && (!login.getLogin().equals("")))
					&& (login.getPassword() != null & (!login.getPassword().equals("")))) {
				response.setPayLoad(LoginManager.loginWithToken(login));
			}
		} catch (BossException e) {
			response.setErrorCode(e.getErrorCode());
			response.setExceptionMessage(e.getMessage());
			LOGGER.severe("Erreur login route: " + e.toString());
		}

		return (mapper.writeValueAsString(response));
	}

	@Get
	public String getTest() {
		return "success";
	}

}
