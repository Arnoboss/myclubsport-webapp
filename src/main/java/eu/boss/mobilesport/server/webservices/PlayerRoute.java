package eu.boss.mobilesport.server.webservices;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.StatsManager;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on unique player
 * 
 * @author Arnaud
 */
public class PlayerRoute extends ServerResource {

	@Get
	public String getPlayer() throws Exception {
		String playerName = getQuery().getValues("playerName");
		String category = getQuery().getValues("category");

		ObjectMapper mapper = JSON.defaultView();
		return mapper.writeValueAsString(new GenericResponseMessage<List<PlayerMessage>>(StatsManager
				.loadAllPlayerStatsForSeason(playerName, category)));

	}

	@Put
	public String updatePlayer() {
		// TODO
		return "Todo";
	}

	@Post
	public String postPlayer() {
		// TODO
		return "Todo";
	}

}
