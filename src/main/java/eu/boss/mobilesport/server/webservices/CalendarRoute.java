package eu.boss.mobilesport.server.webservices;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.TeamsManager;
import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.HtmlMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.fff.FFFWeb;
import eu.boss.mobilesport.model.Team;
import eu.boss.mobilesport.server.JSON;
import eu.boss.mobilesport.utils.Util;

/**
 * Route returning ranking. From version 2.0, applications specify the wanted category.
 * 
 * @author Arnaud
 */
public class CalendarRoute extends ServerResource {

	@Get
	public String getCalendar() throws Exception {
		String teamName = getQuery().getValues("team");
		String category = getQuery().getValues("category");
		ObjectMapper mapper = JSON.defaultView();
		GenericResponseMessage<HtmlMessage> response = new GenericResponseMessage<HtmlMessage>(null);
		HtmlMessage htmlMsg = new HtmlMessage();
		try {
			if (category == null) {
				// for version < 2.0
				htmlMsg.setHtmlContent(FFFWeb.loadCalendar(TeamNameMessage.valueOf(teamName), true));
			} else {
				Team team = TeamsManager
						.loadTeamForSeason(category, teamName, Util.currentSeasonDate());
				htmlMsg.setHtmlContent(FFFWeb.loadCalendarFromUrl(team.getUrlCalendar(), true));
			}
			response.setPayLoad(htmlMsg);
		} catch (BossException e) {
			response.setErrorCode(e.getErrorCode());
			response.setExceptionMessage(e.getMessage());
		}

		return mapper.writeValueAsString(response);
	}
}
