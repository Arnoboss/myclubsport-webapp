package eu.boss.mobilesport.server.webservices;

import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.boss.mobilesport.datastore.StatsManager;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.GenericResponseMessage;
import eu.boss.mobilesport.externalmodel.PlayerListMessage;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.server.JSON;

/**
 * Route for operations on a list of players
 * 
 * @author Arnaud
 */
public class PlayersRoute extends ServerResource {

	@Get
	public String getPlayersList() throws Exception {
		String season = getQuery().getValues("season");
		String category = getQuery().getValues("category");
		ObjectMapper mapper = JSON.defaultView();
		List<PlayerMessage> players;
		// requests with category are for app from version 2.0
		if (category == null) players = StatsManager.loadAllPlayerStatsForSeason(season, null);
		else players = StatsManager.loadAllPlayerStatsForSeasonAndCategory(season,
				Category.valueOf(category), 0);

		PlayerListMessage playerList = new PlayerListMessage();
		playerList.setPlayers(players);
		return mapper.writeValueAsString(new GenericResponseMessage<PlayerListMessage>(playerList));

	}

	@Put
	public String updatePlayerList() {
		// TODO
		return "Todo";
	}

	@Post
	public String postPlayerList() {
		// TODO
		return "Todo";
	}

}
