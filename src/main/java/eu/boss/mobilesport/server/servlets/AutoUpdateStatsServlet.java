package eu.boss.mobilesport.server.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.boss.mobilesport.datastore.FormationManager;
import eu.boss.mobilesport.datastore.LiveManager;
import eu.boss.mobilesport.datastore.StatsManager;
import eu.boss.mobilesport.externalmodel.MatchStatus;
import eu.boss.mobilesport.externalmodel.MatchType;
import eu.boss.mobilesport.model.Formation;
import eu.boss.mobilesport.model.FormationItem;
import eu.boss.mobilesport.model.Match;

/**
 * Servlet used with CRON to run automatically to update players stats with the matchs of the day
 * 
 * @author Arnaud
 */
public class AutoUpdateStatsServlet extends HttpServlet {

	private static final long serialVersionUID = -3502530883573557227L;
	private static final Logger LOGGER = Logger.getLogger(AutoUpdateStatsServlet.class.getName());

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.info("AutoUpdateStatsServlet call ");
		// Get match of the day
		List<Match> matchs = LiveManager.getMatchByDateAndStatus(new Date(), MatchStatus.FINISHED);

		for (int i = 0; i < matchs.size(); i++) {
			Match match = matchs.get(i);
			//ignore friendly matchs
			if (match.getMatchType() == MatchType.FRIENDLY)
				return;

			// On ne differencie pas les 2 equipes. Pour l'instant on part du principe que seule 1
			// equipe e des buteurs vu que c'est une appli dediee a un club
			LinkedHashMap<String, Integer> scorersTeam = initScorersHashmap(match.getButeursTeam1(),
					match.getButeursTeam2());
			// Get formation for each match (if exists)
			Formation formation = FormationManager.getFormationForMatch(match.getId());
			if (formation != null) {
				// get players for formation (if exists)
				List<FormationItem> items = FormationManager
						.getItemsForFormation(formation.getIdFormation());
				if (items != null && items.size() > 0) {
					// update stats for every player
					for (int j = 0; j < items.size(); j++) {
						if (items.get(j) != null && items.get(j).getPlayerName() != null) {
							String playerName = items.get(j).getPlayerName().trim();
							// Add game played and goals
							int goals = 0;
							if (scorersTeam.get(playerName) != null) goals = scorersTeam.get(playerName);
							StatsManager.addGamePlayedAndGoals(match.getCategoryName(),
									match.getTeamName(), playerName, goals);
						}
					}
				}

			}
			// Apres avoir mis automatiquement les stats e jour, on met un flag sur le match pour dire
			// qu'il a ete mis e jour automatiquement
			match.setAutoSavedStats(true);
			LiveManager.updateMatch(match);
		}
	}

	/**
	 * Convertit les listes des buteurs en hashmaps
	 */
	private LinkedHashMap<String, Integer> initScorersHashmap(String scorersList1,
			String scorersList2) {
		LinkedHashMap<String, Integer> scorersTeam = new LinkedHashMap<String, Integer>();
		Pattern p = Pattern.compile(
				"(([a-zA-Z. -]+)(\\s[x])([0-9]+))");
		if (scorersList1 != null && !scorersList1.equals("")) {
			String[] separatedScorers = scorersList1.split(", ");

			for (int i = 0; i < separatedScorers.length; i++) {
				LOGGER.info("scorer:" + separatedScorers[i]);
				Matcher m = p.matcher(separatedScorers[i]);
				if (m.matches()) {
					// Le joueur a marque plusieurs buts
					scorersTeam.put(m.group(2), Integer.valueOf(m.group(4)));
				} else scorersTeam.put(separatedScorers[i], 1);
			}
		}
		if (scorersList2 != null && !scorersList2.equals("")) {
			String[] separatedScorers = scorersList2.split(", ");

			for (int i = 0; i < separatedScorers.length; i++) {
				LOGGER.info("scorer:" + separatedScorers[i]);
				Matcher m = p.matcher(separatedScorers[i]);
				if (m.matches()) {
					// Le joueur a marque plusieurs buts
					scorersTeam.put(m.group(2), Integer.valueOf(m.group(4)));
				} else scorersTeam.put(separatedScorers[i], 1);
			}
		}

		return scorersTeam;
	}
}
