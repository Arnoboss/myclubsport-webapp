package eu.boss.mobilesport.server.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;

import eu.boss.mobilesport.datastore.ImageManager;
import eu.boss.mobilesport.datastore.NewsManager;
import eu.boss.mobilesport.externalmodel.NewsType;
import eu.boss.mobilesport.model.News;
import eu.boss.mobilesport.pushnotifications.NotificationType;
import eu.boss.mobilesport.pushnotifications.PushNotificationManager;

/**
 * Servlet implementation class UploadImageServlet
 */

public class UploadImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(UploadImageServlet.class.getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadImageServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ImageManager.deleteImage(request.getParameter("blobkey"));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Repond sur /upload

		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

		// Recupere une Map de tous les champs d'upload de fichiers
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(request);

		// Recupere la liste des fichiers uploades dans le champ "uploadedFile"
		// (il peut y en avoir plusieurs si c'est un champ d'upload multiple, d'oe la List)
		List<BlobKey> blobKeys = blobs.get("uploadedFile");

		// Recupere la cle identifiant du fichier uploade dans le Blobstore (e sauvegarder)
		String imageBlobKey = blobKeys.get(0).getKeyString();

		// Recuperation de l'URL de l'image
		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		String urlImage = imagesService
				.getServingUrl(ServingUrlOptions.Builder.withBlobKey(blobKeys.get(0)));

		LOGGER.info("blob key " + imageBlobKey);
		LOGGER.info("Url image: " + urlImage);
		// On peut recuperer les autres champs du formulaire comme d'habitude
		String title = request.getParameter("j_idt47:j_idt55");
		String content = request.getParameter("j_idt47:editor_input");
		LOGGER.info("title " + content);
		LOGGER.info("content: " + content);
		News news = new News(title, new Date(), urlImage, imageBlobKey, content, NewsType.AUTRE);

		boolean isEditing = false;
		Long newsId = null;
		if ((request.getParameter("edit") != null) && (request.getParameter("edit").equals("true"))) {
			isEditing = true;
			if (request.getParameter("newsId") != null) {
				newsId = Long.valueOf(request.getParameter("newsId"));
				news.setId(newsId);
			}
			NewsManager.editNews(news);
		} else newsId = NewsManager.addNews(news).getId();

		// check if notification flag if set
		if ((request.getParameter("j_idt47:j_idt68_input") != null)
				&& (request.getParameter("j_idt47:j_idt68_input").equals("on"))) {
			PushNotificationManager.sendNotificationMsgToAll(title, content, NotificationType.NEWS,
					urlImage, "" + newsId);
		}

		if (isEditing) response.sendRedirect("/faces/newsedited.xhtml");
		else response.sendRedirect("/faces/newsadded.xhtml");
	}
}
