package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.exceptions.DatabaseException;
import eu.boss.mobilesport.exceptions.ErrorCode;
import eu.boss.mobilesport.exceptions.LoginException;
import eu.boss.mobilesport.externalmodel.LoginMessage;
import eu.boss.mobilesport.externalmodel.SecurityTokenMessage;
import eu.boss.mobilesport.model.Login;
import eu.boss.mobilesport.model.SecurityToken;
import eu.boss.mobilesport.utils.Constant;

public class LoginManager {

	private static final Logger LOGGER = Logger.getLogger(LoginManager.class.getName());
	static {
		ObjectifyService.register(Login.class);
		ObjectifyService.register(SecurityToken.class);
	}

	/**
	 * Methode initialisation de l'admin. Merdique niveau securite mais vu l'application, rien de
	 * grave
	 * 
	 * @return
	 */
	public static Key<Login> initAdmin() {
		Login admin = new Login(Constant.ADMIN_INIT_USER, "" + Constant.ADMIN_INIT_PWD.hashCode());
		return ofy().save().entity(admin).now(); // Synchrone (enregistre immediatement)
	}

	public static boolean login(String name, String password) {
		LOGGER.info("Attempt login user: " + name);
		Login p = ofy().load().type(Login.class).filter("name =", name).first().now();
		if ((p != null) && (p.getPassword().equals("" + password.hashCode()))) return true;
		return false;
	}

	/**
	 * On passe l'attribut isActive a false
	 * 
	 * @param tokenId
	 */
	public static void deactiveToken(final String tokenId) {

		SecurityToken token = ofy().load().type(SecurityToken.class).filter("tokenId =", tokenId)
				.first().now();
		if (token != null) {
			token.setIsActive(0);
			ofy().save().entity(token).now();
		}

	}

	private static void addTokenForLogin(SecurityTokenMessage tokenMsg, LoginMessage login)
			throws ParseException {
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SecurityToken token = new SecurityToken(tokenMsg.getTokenId(),
				df.parse(tokenMsg.getExpirationDate()), 1, Key.create(Login.class, login.getLogin()));
		ofy().save().entity(token).now();
	}

	/**
	 * Login uniquement grace au token
	 * 
	 * @param tokenId
	 * @return
	 */
	public static void loginWithToken(String tokenId) throws BossException {
		SecurityToken token = ofy().load().type(SecurityToken.class).filter("tokenId =", tokenId)
				.first().now();
		if (token != null && token.getIsActive() == 1) {
			// TODO On ne verifie plus la date des tokens pour l'instant. Si le token existe en base et
			// est actif, on valide le login
			// if (token.getExpirationDate().compareTo(new Date()) > 0) {

			// } else {
			// deactiveToken(tokenId);
			// }
		} else throw new LoginException(ErrorCode.LOGIN_FAILED,
				"Votre identifiant ne vous permet pas cette opération");
	}

	public static LoginMessage loginWithToken(LoginMessage loginMessage) throws BossException {
		if ((loginMessage.getToken() == null || loginMessage.getToken().equals(""))
				|| (loginMessage.getToken() != null && loginMessage.getLogin() != null
						&& loginMessage.getPassword() != null)) {
			// Si aucun token, on verifie le login par rapport login/pwd et on creee un token.
			if (login(loginMessage.getLogin(), loginMessage.getPassword())) {
				// S'il y a Login/pwd + token, on invalide le token dans la base (c'est un ancien token
				// a retirer)
				if (loginMessage.getToken() != null
						&& !loginMessage.getToken().getTokenId().equals("")) {
					LoginManager.deactiveToken(loginMessage.getToken().getTokenId());
				}
				loginMessage.setToken(new SecurityTokenMessage(3));
				try {
					addTokenForLogin(loginMessage.getToken(), loginMessage);
				} catch (ParseException e) {
					throw new DatabaseException(ErrorCode.DATABASE_ERROR, e.getMessage());
				}
			} else {
				throw new LoginException(ErrorCode.LOGIN_FAILED,
						"Identifiant ou mot de passe incorrect. Veuillez reessayer");

			}
		} else {
			// sinon on verifie la validite du token
			loginWithToken(loginMessage.getToken().getTokenId());
			// TODO: quand cette erreur arrive sur le telephone, il faut ouvrir la page de login
		}

		return loginMessage;
	}
}
