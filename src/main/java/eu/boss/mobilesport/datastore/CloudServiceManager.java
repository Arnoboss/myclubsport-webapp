package eu.boss.mobilesport.datastore;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.api.client.util.Base64;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;

import eu.boss.mobilesport.exceptions.ErrorCode;
import eu.boss.mobilesport.exceptions.ImageManagementException;
import eu.boss.mobilesport.externalmodel.ImageDetails;
import eu.boss.mobilesport.externalmodel.TreeNode;
import eu.boss.mobilesport.utils.Constant;

public class CloudServiceManager {

	private static final Logger LOGGER = Logger.getLogger(CloudServiceManager.class.getName());

	/**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	  private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	  
	/**
	 * This is where backoff parameters are configured. Here it is aggressively retrying with
	 * backoff, up to 10 times but taking no more that 15 seconds total to do so.
	 */
	private final static GcsService gcsService = GcsServiceFactory
			.createGcsService(new RetryParams.Builder().initialRetryDelayMillis(10)
					.retryMaxAttempts(10).totalRetryPeriodMillis(15000).build());

	/**
	 * Upload image to cloud storage and returns image path
	 * 
	 * @param newsMsg
	 * @return image path ;
	 */
	public static String uploadImage(ImageDetails image, String folder)
			throws ImageManagementException {
		try {
			GcsFilename fileName = new GcsFilename(Constant.BUCKET_NAME + folder, URLDecoder.decode(image.getImageName(),
				"UTF-8"));
			GcsOutputChannel outputChannel;
		
			LOGGER.info("CloudServiceManager.uploadImage");
			GcsFileOptions options = new GcsFileOptions.Builder().mimeType(image.getImageMimeType())
					.build();
			outputChannel = gcsService.createOrReplace(fileName, options);

			outputChannel.write(ByteBuffer.wrap(Base64.decodeBase64(image.getImageAsBytes())));
			outputChannel.close();
			return Constant.IMAGE_PUBLIC_PATH + folder + "/" + fileName.getObjectName();
		} catch (Exception e) {
			LOGGER.severe("CloudServiceManager.uploadImage error: " + e.toString());
			throw new ImageManagementException(ErrorCode.UPLOAD_IMAGE_ERROR,
					"Erreur lors de l'enregistrement de l'image");
		}
	}
	
	public static String uploadImageFile(InputStream inputStream, String contentType, String filename, String folder)
			throws ImageManagementException {
		try {
			GcsFilename file = new GcsFilename(Constant.BUCKET_NAME + folder, URLDecoder.decode(filename,
				"UTF-8"));
			GcsOutputChannel outputChannel;
		
			LOGGER.info("CloudServiceManager.uploadImage");
			GcsFileOptions options = new GcsFileOptions.Builder().mimeType(contentType)
					.build();
			outputChannel = gcsService.createOrReplace(file, options);

			copy(inputStream, Channels.newOutputStream(outputChannel));
			return Constant.IMAGE_PUBLIC_PATH + folder + "/" + file.getObjectName();
		} catch (Exception e) {
			LOGGER.severe("CloudServiceManager.uploadImage error: " + e.toString());
			throw new ImageManagementException(ErrorCode.UPLOAD_IMAGE_ERROR,
					"Erreur lors de l'enregistrement de l'image");
		}
	}

	private static void copy(InputStream input, OutputStream output) throws IOException {
	    try {
	      byte[] buffer = new byte[BUFFER_SIZE];
	      int bytesRead = input.read(buffer);
	      while (bytesRead != -1) {
	        output.write(buffer, 0, bytesRead);
	        bytesRead = input.read(buffer);
	      }
	    } finally {
	      input.close();
	      output.close();
	    }
	  }
	
	public static void removefile(String fileName, String folder) throws ImageManagementException {
		LOGGER.info("** Deleting image " +fileName +" from folder" + folder);
		GcsFilename file = new GcsFilename(Constant.BUCKET_NAME + folder, fileName);
		try {
			gcsService.delete(file);
		} catch (Exception e) {
			LOGGER.severe("CloudServiceManager.removeImage error: " + e.toString());
			throw new ImageManagementException(ErrorCode.UPLOAD_IMAGE_ERROR,
					"Erreur lors de la suppression de l'image");
		}
	}

	/**
	 * Test if a file already exists on cloud storage
	 * 
	 * @param filename
	 * @return
	 */
	public static boolean isExistingFile(GcsFilename filename) {
		GcsFileMetadata md;
		try {
			md = gcsService.getMetadata(filename);

			if (md == null) {
				return false;
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Retourne l'arborescence des dossiers a partir du du dossier donne
	 * 
	 * @return
	 * @throws ImageManagementException
	 */
	public static TreeNode<String> retrieveTreeFolders(String folder)
			throws ImageManagementException {

		TreeNode<String> root = new TreeNode<String>(folder);
		try {
			ListResult result = gcsService.list(Constant.BUCKET_NAME,
					new ListOptions.Builder().setPrefix(folder).setRecursive(false).build());

			while (result.hasNext()) {
				ListItem item = result.next();
				if (!item.getName().equals(folder)) {
					LOGGER.info(item.getName());
					root.addChild(retrieveTreeFolders(item.getName()));
				}

			}
		} catch (IOException e) {
			LOGGER.severe("Impossible de lister le contenu du dossier " + folder);
			throw new ImageManagementException("Impossible de lister le contenu du dossier " + folder);
		}
		return root;
	}

	/**
	 * Retourne les fichiers contenus dans un dossier. Exclut les dossiers
	 * 
	 * @param folder
	 * @param isFromWebService
	 *           si la requete vient d'un web service, on n'ajoute pas le IMAGE_PUBLIC_PATH pour
	 *           alleger la requete
	 * @return
	 * @throws ImageManagementException
	 */
	public static List<String> retrieveAllFilesFromFolder(String folder, boolean isFromWebService)
			throws ImageManagementException {
		LOGGER.info("retrieveAllFilesFromFolder: " + folder);
		List<String> urlImageList = new ArrayList<String>();
		try {
			ListResult result = gcsService.list(Constant.BUCKET_NAME,
					new ListOptions.Builder().setPrefix(folder).setRecursive(false).build());

			while (result.hasNext()) {
				ListItem item = result.next();
				if (!item.isDirectory()
						&& item.getName().lastIndexOf("/") != item.getName().length() - 1) {
					LOGGER.info(item.getName());
					if (isFromWebService) urlImageList.add(item.getName());
					else urlImageList.add(Constant.IMAGE_PUBLIC_PATH + "/" + item.getName());
				}
			}
			return urlImageList;
		} catch (IOException e) {
			LOGGER.severe("Impossible de lister le contenu du dossier " + folder);
			throw new ImageManagementException("Impossible de lister le contenu du dossier " + folder);
		}
	}
}
