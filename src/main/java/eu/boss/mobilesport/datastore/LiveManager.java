package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.CommentMessage;
import eu.boss.mobilesport.externalmodel.MatchStatus;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.model.Comment;
import eu.boss.mobilesport.model.Match;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.utils.Util;

public class LiveManager {

	static {
		ObjectifyService.register(Comment.class);
		ObjectifyService.register(Match.class);
	}

	private static final Logger LOGGER = Logger.getLogger(LiveManager.class.getName());

	/**
	 * get all comments a a given match
	 * 
	 * @param matchId
	 * @return comments list
	 */
	public static List<Comment> getCommentsForMatch(Long matchId) {

		return ofy().load().type(Comment.class).ancestor(Key.create(Match.class, matchId))
				.order("-minuteEvent").list();
	}

	/**
	 * Load match from its id
	 * 
	 * @param id
	 * @return
	 */
	public static Match getMatchById(Long matchId) {
		return ofy().load().type(Match.class)
				.parent(Key.create(Season.class, Util.currentSeasonDate())).id(matchId).now();
	}

	/**
	 * Add a comment to the given match
	 * 
	 * @param matchId
	 * @param comment
	 */
	public static Key<Comment> addComment(Long matchId, Comment comment) {
		comment.setParent(Key.create(Match.class, matchId));
		return ofy().save().entity(comment).now(); // Synchrone (enregistre
		// immediatement)
	}

	/**
	 * Returns the last matchs planned before a given date. Used to see match history
	 */
	public static List<Match> getLastMatchsBeforeDate(String date, int numberMaxOfMatchs) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date current;
		try {
			current = sdf.parse(date);
			// On prend les X derniers matchs. Le tri se fera plus finement dans
			// le code java en fonction de la date du match le plus recent
			return ofy().load().type(Match.class).filter("date <", current).order("date")
					.limit(numberMaxOfMatchs).list();
		} catch (ParseException e) {
			LOGGER.severe("getLastMatchsBeforeDate: Unable to parse the given date: " + e.toString());
			return null;
		}
	}

	/**
	 * Returns the matchs planned next week
	 */
	public static List<Match> getNextMatchs() {
		Date threeDaysAgo = new Date();
		Date threeDaysAfter = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(threeDaysAgo);
		c.add(Calendar.DATE, -4); // number of days to add
		threeDaysAgo = c.getTime();

		c.setTime(threeDaysAfter);
		c.add(Calendar.DATE, 4); // number of days to add
		threeDaysAfter = c.getTime();
		return ofy().load().type(Match.class).filter("date >", threeDaysAgo)
				.filter("date <", threeDaysAfter).order("date").limit(10).list();
	}

	public static List<Match> getMatchByDate(Date date) {
		Date dayBefore = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, -1); // number of days to add
		dayBefore = c.getTime();

		return ofy().load().type(Match.class).filter("date >", dayBefore).filter("date <", date)
				.order("date").list();

	}

	/**
	 * On recupere la liste des matchs du jour termines et qui n'ont pas encore eu leurs stats mises
	 * a jour automatiquement
	 * 
	 * @param date
	 * @param status
	 * @return
	 */
	public static List<Match> getMatchByDateAndStatus(Date date, MatchStatus status) {
		Date dayBefore = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, -2); // number of days to add
		dayBefore = c.getTime();

		return ofy().load().type(Match.class).filter("date >", dayBefore).filter("date <", date)
				.filter("autoSavedStats", false).filter("status", status.name()).order("date").list();
	}

	/**
	 * @param match
	 * @return
	 */
	public static Key<Match> addNewMatch(Match match) {
		match.setParent(Key.create(Season.class, Util.currentSeasonDate()));
		return ofy().save().entity(match).now();
	}

	/**
	 * Delete match
	 * 
	 * @param matchId
	 */
	public static void deleteMatch(Long matchId) {
		LOGGER.info("** Deleting match: " + matchId + " **");
		// FormationManager.deleteFormationAndItemsForMatch(matchId);
		ofy().delete().type(Match.class).parent(Key.create(Season.class, Util.currentSeasonDate()))
				.id(matchId).now();
	}

	/**
	 * Modify match
	 * 
	 * @param MatchMessage
	 */
	public static void updateMatch(final Match matchToUpdate) {
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Match match = ofy().load().type(Match.class)
						.parent(Key.create(Season.class, Util.currentSeasonDate()))
						.id(matchToUpdate.getId()).now();
				match.setDate(matchToUpdate.getDate());
				match.setIntitule(matchToUpdate.getIntitule());
				match.setGoalsTeam1(matchToUpdate.getGoalsTeam1());
				match.setGoalsTeam2(matchToUpdate.getGoalsTeam2());
				match.setButeursTeam1(matchToUpdate.getButeursTeam1());
				match.setButeursTeam2(matchToUpdate.getButeursTeam2());
				match.setTeam1(matchToUpdate.getTeam1());
				match.setTeam2(matchToUpdate.getTeam2());
				match.setStatus(matchToUpdate.getStatus());
				match.setAutoSavedStats(matchToUpdate.isAutoSavedStats());
				ofy().save().entity(match).now();
			}
		});
	}

	public static void updateComment(final Long matchId, final CommentMessage commentMessage) {
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Comment comment = ofy().load().type(Comment.class)
						.parent(Key.create(Match.class, matchId)).id(commentMessage.getId()).now();

				comment.setContent(commentMessage.getContent());
				comment.setImportant(commentMessage.isImportant());
				comment.setMinuteEvent(commentMessage.getMinuteEvent());
				ofy().save().entity(comment).now();
			}
		});

	}

	public static Match getLastMatchForTeam(Category teamCategory, TeamNameMessage teamName) {
		return ofy().load().type(Match.class).filter("categoryName", teamCategory.name())
				.filter("teamName", teamName.name()).order("-date").first().now();
	}

}
