package eu.boss.mobilesport.datastore;

import java.util.logging.Logger;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class ImageManager {

	private static final Logger LOGGER = Logger.getLogger(ImageManager.class.getName());

	public static void deleteImage(String blobKey) {
		try {
			BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
			BlobKey key = new BlobKey(blobKey);
			blobstoreService.delete(key);
			LOGGER.info("blobKey deleted " + blobKey);
		} catch (Exception e) {
			LOGGER.severe("Can not delete blobkey: " + blobKey);
		}
	}
}
