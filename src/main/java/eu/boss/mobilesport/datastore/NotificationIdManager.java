package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.model.NotificationId;

public class NotificationIdManager {

	private static final Logger LOGGER = Logger.getLogger(NotificationIdManager.class.getName());
	static {
		ObjectifyService.register(NotificationId.class);
	}

	public static Key<NotificationId> persistNotificationId(NotificationId notif) {
		return ofy().save().entity(notif).now(); // Synchrone (enregistre immediatement)
	}

	public static List<NotificationId> loadAllNotificationId() {
		List<NotificationId> pList = ofy().load().type(NotificationId.class).list();
		LOGGER.info("loadAllNotificationId size: " + pList.size());
		return pList;
	}

	public static void removeNotificationId(String idToRemove) {
		NotificationId notif = ofy().load().type(NotificationId.class)
				.filter("notificationId =", idToRemove).first().now();
		ofy().delete().entity(notif);

	}

	public static void removeNotificationIdList(final List<NotificationId> notificationList) {
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				for (NotificationId id : notificationList) {
					ofy().delete().type(NotificationId.class).id(id.getId()).now();
				}
			}

		});
	}
}
