package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.exceptions.ImageManagementException;
import eu.boss.mobilesport.externalmodel.NewsType;
import eu.boss.mobilesport.model.News;
import eu.boss.mobilesport.utils.Constant;

public class NewsManager {

	private static final Logger LOGGER = Logger.getLogger(NewsManager.class.getName());
	static {
		ObjectifyService.register(News.class);
	}

	public static Key<News> addNews(News news) {
		news.setContent(news.getContent().replaceAll("(background-color)[^&]*(;)", ""));
		Key<News> key = ofy().save().entity(news).now();
		return key;
	}

	/**
	 * retourne la liste des news de la plus recente e la moins recente. Le padding est
	 * utilise pour la pagination (retourner les news de la numero paddingStart a la numero
	 * paddingEnd)
	 * 
	 * @return
	 */

	public static List<News> getNews() {
		LOGGER.info("getNews");
		return ofy().load().type(News.class).order("-date").limit(15).list();
	}

	/**
	 * @param newsId
	 * @return loaded news
	 */
	public static News getNewsById(Long newsId) {
		LOGGER.info("getNewsById");
		return ofy().load().type(News.class).id(newsId).now();
	}

	/**
	 * Delete news and the associate image blob
	 * 
	 * @param newsId
	 * @throws ImageManagementException
	 */
	public static void deleteNews(Long newsId) throws ImageManagementException {
		LOGGER.info("Deleting news: " + newsId + " **");
		News news = ofy().load().type(News.class).id(newsId).now();
		if (news.getNewsType() == null || news.getNewsType().equals(NewsType.AUTRE)) {
			if (news.getImageBlobKey() != null && !news.getImageBlobKey().equals(""))
				ImageManager.deleteImage(news.getImageBlobKey());
			else {
				String[] splittedUrl = news.getImageUrl().split("/");
				String imageName = splittedUrl[splittedUrl.length - 1];
				CloudServiceManager.removefile(imageName, Constant.FOLDER_NEWS);
			}
		}
		ofy().delete().type(News.class).id(newsId).now();
	}

	/**
	 * Modification d'une news. Doit se faire dans une transaction ou on charge, on modifie puis on
	 * enregistre la news
	 * 
	 * @param newsToEdit
	 */
	public static void editNews(final News newsToEdit) {
		LOGGER.info("editNews");
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				News news = ofy().load().type(News.class).id(newsToEdit.getId()).now();
				// do modifications here
				news.setContent(newsToEdit.getContent().replaceAll("(background-color)[^&]*(;)", ""));
				news.setNewsType(newsToEdit.getNewsType());
				news.setImageUrl(newsToEdit.getImageUrl());
				news.setTitle(newsToEdit.getTitle());

				ofy().save().entity(news).now();
			}
		});
	}

}
