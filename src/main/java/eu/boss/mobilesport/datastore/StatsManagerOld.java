package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.converters.PlayerConverter;
import eu.boss.mobilesport.converters.TeamStatsConverter;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.model.Player;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.model.TeamStats;
import eu.boss.mobilesport.utils.Util;

/**
 * Ancienne classe de gestion des stats. L'ancestor des teamStats ayant change depuis la V2, cette
 * classe est obsolete. Elle a ete gardee pour un rattrage des anciennes donnees vers les nouvelles
 * 
 * @author Arnaud
 */
public class StatsManagerOld {

	private static final Logger LOGGER = Logger.getLogger(StatsManager.class.getName());
	static {
		ObjectifyService.register(Player.class);
		ObjectifyService.register(Season.class);
		ObjectifyService.register(TeamStats.class);
	}

	public static Key<Season> initSeason() {
		Season season = new Season(Util.currentSeasonDate());
		return ofy().save().entity(season).now();
	}

	public static Key<Player> persistPlayer(Player p) {
		return ofy().save().entity(p).now(); // Synchrone (enregistre immediatement)
	}

	public static void persistPlayerList(final List<PlayerMessage> players) {
		LOGGER.info("persistPlayerList: list size: " + players.size());
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Key<Season> seasonKey = Key.create(Season.class, Util.currentSeasonDate());
				for (PlayerMessage p : players) {
					// Tout ce qui est execute dans cette methode appartient a la meme transaction
					Player pToUpdate = PlayerConverter.toUpdateDatastore(
							loadPlayerForSeason(p.getName(), Util.currentSeasonDate()), p, seasonKey);
					/* Modification de l'objet... */
					// LOGGER.info("pToUpdate: " + pToUpdate.getName());
					ofy().save().entity(pToUpdate);

					List<TeamStats> oldStats = loadPlayerStats(p.getName());
					int i = 0;
					for (eu.boss.mobilesport.externalmodel.TeamStatsMessage stat : p.getTeamStats()) {
						// LOGGER.info("stat: " + stat.getTeamName().name());

						// On ecrase les valeurs d'une teamStat par une autre appartenant au joueur (peu
						// importe laquelle). Dans tous les cas, la liste est parcourue et les stats
						// enregistrees
						TeamStats ts;
						if (oldStats.size() == 0) {
							// LOGGER.info("oldStats is null");
							ts = TeamStatsConverter.toUpdateDatastoreOld(null, stat, p.getName(),
									seasonKey);
						} else {
							// LOGGER.info("oldStats(" + i + "): " + oldStats.get(i).getTeamName());
							ts = TeamStatsConverter.toUpdateDatastoreOld(oldStats.get(i), stat,
									p.getName(), seasonKey);
						}
						ofy().save().entity(ts);
						i++;
					}
				}
			}
		});
	}

	/**
	 * removes players from datastore
	 * 
	 * @param players
	 */
	public static void removePlayerList(final List<PlayerMessage> players) {
		LOGGER.info("removePlayerList: list size: " + players.size());
		if (players != null && players.size() > 0) {
			ofy().transact(new VoidWork() {

				@Override
				public void vrun() {
					Key<Season> seasonKey = Key.create(Season.class, Util.currentSeasonDate());
					for (PlayerMessage p : players) {
						Player pToDelete = PlayerConverter.toUpdateDatastore(
								loadPlayerForSeason(p.getName(), Util.currentSeasonDate()), p, seasonKey);

						List<TeamStats> stats = loadPlayerStats(p.getName());
						for (TeamStats stat : stats) {
							/* suppression des stats associees... */
							// LOGGER.info("remove stat: " + stat.getTeamName());
							ofy().delete().entity(stat).now();
						}

						/* suppression du player... */
						// LOGGER.info("remove player: " + pToDelete.getName());
						ofy().delete().entity(pToDelete).now();
					}
				}
			});
		}

	}

	/**
	 * removes players from datastore
	 * 
	 * @param players
	 */
	public static void removePlayerStats(final List<PlayerMessage> players) {
		LOGGER.info("removePlayerList: list size: " + players.size());
		if (players != null && players.size() > 0) {
			ofy().transact(new VoidWork() {

				@Override
				public void vrun() {
					for (PlayerMessage p : players) {
						List<TeamStats> stats = loadPlayerStats(p.getName());
						for (TeamStats stat : stats) {
							/* suppression des stats associees... */
							// LOGGER.info("remove stat: " + stat.getTeamName());
							ofy().delete().entity(stat).now();
						}
					}
				}
			});
		}

	}

	/**
	 * Loads player from a name. Tested and approved
	 * 
	 * @param name
	 * @return given player if exists
	 */
	public static Player loadPlayer(String name) {
		Player p = ofy().load().type(Player.class).filter("name =", name).first().now();
		return p;
	}

	/**
	 * @return
	 */
	public static Player loadPlayerForSeason(String name, String seasonDate) {
		return ofy().load().type(Player.class).ancestor(Key.create(Season.class, seasonDate))
				.filter("name =", name).first().now();

	}

	/**
	 * Loads all players. Tested and approved
	 * 
	 * @return
	 */
	public static List<Player> loadPlayers() {
		List<Player> pList = ofy().load().type(Player.class).order("name").list();
		return pList;
	}

	/**
	 * @return
	 */
	public static List<Player> loadPlayersForSeason(String seasonDate) {
		List<Player> pList = ofy().load().type(Player.class)
				.ancestor(Key.create(Season.class, seasonDate)).order("name").list();
		return pList;
	}

	/**
	 * Loads stats for a given player.
	 * 
	 * @param playerName
	 * @return stats for the given player
	 */
	public static List<TeamStats> loadPlayerStats(String playerName) {

		return ofy().load().type(TeamStats.class).ancestor(Key.create(Player.class, playerName))
				.list();
	}

	/**
	 * @return playerList from external model. Made to respond to a JSON web service. Tested and
	 *         approved
	 */
	public static List<PlayerMessage> loadAllPlayerStatsForSeason(String seasonDate) {
		LOGGER.info("loadAllPlayerStatsForSeason: " + seasonDate);
		List<PlayerMessage> pListToReturn = new ArrayList<PlayerMessage>();
		List<Player> pList = loadPlayersForSeason(seasonDate);
		for (Player p : pList) {
			PlayerMessage pToReturn = PlayerConverter.toExternalModel(p);
			pToReturn.setTeamStats(TeamStatsConverter.toExternalModel(loadPlayerStats(p.getName())));
			pListToReturn.add(pToReturn);
		}
		return pListToReturn;
	}

}
