package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.converters.PlayerConverter;
import eu.boss.mobilesport.converters.TeamStatsConverter;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.externalmodel.TeamStatsMessage;
import eu.boss.mobilesport.model.Player;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.model.TeamStats;
import eu.boss.mobilesport.utils.Util;

/**
 * This class contains multiple usefull functions managing player stats <br>
 * TODO Toutes les classes "Datastore" ne devraient pas traiter des objets externalmodel. Retirer
 * ceux d'ici a l'occasion.
 * 
 * @author Arnaud
 */
public class StatsManager {

	private static final Logger LOGGER = Logger.getLogger(StatsManager.class.getName());
	static {
		ObjectifyService.register(Player.class);
		ObjectifyService.register(Season.class);
		ObjectifyService.register(TeamStats.class);
	}

	public static Key<Season> initSeason() {
		Season season = new Season(Util.currentSeasonDate());
		return ofy().save().entity(season).now();
	}

	public static Key<Player> persistPlayer(Player p) {
		return ofy().save().entity(p).now(); // Synchrone (enregistre immediatement)
	}

	public static void persistPlayerList(final List<PlayerMessage> players, final Category category,
			final String season) {
		LOGGER.info("persistPlayerList: list size: " + players.size());
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Key<Season> seasonKey = Key.create(Season.class, season);
				for (PlayerMessage p : players) {
					// Tout ce qui est execute dans cette methode appartient a la meme transaction
					Player pToUpdate = PlayerConverter.toUpdateDatastore(loadPlayer(p.getId()), p,
							seasonKey);
					/* Modification de l'objet... */
					// LOGGER.info("pToUpdate: " + pToUpdate.getName());
					// rattrapage des anciennes stats:
					if (category != null) pToUpdate.setCategory(category.name());
					Long playerId = ofy().save().entity(pToUpdate).now().getId();
					p.setId(playerId);
					List<TeamStats> oldStats = loadPlayerStats(playerId);
					int i = 0;
					for (eu.boss.mobilesport.externalmodel.TeamStatsMessage stat : p.getTeamStats()) {
						// LOGGER.info("stat: " + stat.getTeamName().name());

						// On ecrase les valeurs d'une teamStat par une autre appartenant au joueur (peu
						// importe laquelle). Dans tous les cas, la liste est parcourue et les stats
						// enregistrees
						TeamStats ts;
						if (oldStats.size() == 0 || oldStats.size() <= i) {
							// LOGGER.info("oldStats is null");
							ts = TeamStatsConverter.toUpdateDatastore(null, stat, playerId,
									category.name(), seasonKey);
						} else {
							// LOGGER.info("oldStats(" + i + "): " + oldStats.get(i).getTeamName());
							ts = TeamStatsConverter.toUpdateDatastore(oldStats.get(i), stat, playerId,
									category.name(), seasonKey);
						}
						ofy().save().entity(ts);
						i++;
					}
				}
			}
		});
	}

	/**
	 * Utilise pour le rattrapage de donnees. Rajout de la categorie en base
	 * 
	 * @param players
	 * @param category
	 */
	public static void persistPlayerList(final List<PlayerMessage> players, String season) {
		persistPlayerList(players, null, season);
	}

	/**
	 * removes players from datastore
	 * 
	 * @param players
	 */
	public static void removePlayerList(final List<PlayerMessage> players, final String category,
			final String season) {
		LOGGER.info("removePlayerList: list size: " + players.size());
		if (players != null && players.size() > 0) {
			ofy().transact(new VoidWork() {

				@Override
				public void vrun() {
					Key<Season> seasonKey = Key.create(Season.class, season);
					for (PlayerMessage p : players) {
						if (p != null && p.getId() != null) {
							Player pToDelete = PlayerConverter.toUpdateDatastore(loadPlayer(p.getId()), p,
									seasonKey);

							List<TeamStats> stats = loadPlayerStats(p.getId());
							for (TeamStats stat : stats) {
								/* suppression des stats associees... */
								// LOGGER.info("remove stat: " + stat.getTeamName());
								ofy().delete().entity(stat).now();
							}

							/* suppression du player... */
							// LOGGER.info("remove player: " + pToDelete.getName());
							// Si l'id est null, le joueur supprime n'existe pas dans le datastore. C'est
							// qu'il a ete cree puis suprime sans avoir ete sauvegarde.
							ofy().delete().entity(pToDelete).now();
						}
					}
				}
			});
		}

	}

	/**
	 * Loads player from a name. Tested and approved
	 * 
	 * @param playerId
	 * @return given player if exists
	 */
	public static Player loadPlayer(Long playerId) {
		Player p = null;
		if (playerId != null) ofy().load().type(Player.class).id(playerId).now();
		return p;
	}

	/**
	 * Loads all players. Tested and approved
	 * 
	 * @return
	 */
	public static List<Player> loadPlayers() {
		List<Player> pList = ofy().load().type(Player.class).order("name").list();
		return pList;
	}

	/**
	 * @return
	 */
	public static List<Player> loadPlayersForSeason(String seasonDate) {
		List<Player> pList = ofy().load().type(Player.class)
				.ancestor(Key.create(Season.class, seasonDate)).order("name").list();
		return pList;
	}

	/**
	 * Load players for season and category
	 * 
	 * @return player list
	 */
	public static List<Player> loadPlayersForSeasonAndCategory(String seasonDate,
			Category category) {
		List<Player> pList = ofy().load().type(Player.class)
				.ancestor(Key.create(Season.class, seasonDate)).filter("category", category.name())
				.order("name").list();
		return pList;
	}

	/**
	 * Loads stats for a given player.
	 * 
	 * @param playerId
	 * @return stats for the given player
	 */
	public static List<TeamStats> loadPlayerStats(Long playerId) {
		return ofy().load().type(TeamStats.class).ancestor(Key.create(Player.class, playerId)).list();
	}

	/**
	 * @return playerList from external model. Made to respond to a JSON web service. Tested and
	 *         approved
	 */
	public static List<PlayerMessage> loadAllPlayerStatsForSeason(String seasonDate,
			String category) {
		LOGGER.info("loadAllPlayerStatsForSeason: " + seasonDate);
		List<PlayerMessage> pListToReturn = new ArrayList<PlayerMessage>();
		List<Player> pList = loadPlayersForSeason(seasonDate);
		for (Player p : pList) {
			PlayerMessage pToReturn = PlayerConverter.toExternalModelWithNoId(p);
			pToReturn.setTeamStats(TeamStatsConverter.toExternalModel(loadPlayerStats(p.getId())));
			pListToReturn.add(pToReturn);
		}
		return pListToReturn;
	}

	/**
	 * Chargement des stats par saison et pour une categorie donnee. On ajoute une teamstat vide si
	 * une equipe a ete ajoutee. Par exemple, si on ajoute une equipe C, on ajoute une teamStat C
	 * pour pouvoir mettre a jour les stats.
	 * 
	 * @param seasonDate
	 * @param category
	 * @param numberOfTeams
	 * @return
	 */
	public static List<PlayerMessage> loadAllPlayerStatsForSeasonAndCategory(String seasonDate,
			Category category, int numberOfTeams) {
		LOGGER.info("loadAllPlayerStatsForSeason: " + seasonDate);
		List<PlayerMessage> pListToReturn = new ArrayList<PlayerMessage>();
		List<Player> pList = loadPlayersForSeasonAndCategory(seasonDate, category);
		for (Player p : pList) {
			PlayerMessage pToReturn = PlayerConverter.toExternalModel(p);
			pToReturn.setTeamStats(TeamStatsConverter.toExternalModel(loadPlayerStats(p.getId())));

			for (int i = pToReturn.getTeamStats().size(); i < numberOfTeams; i++) {
				pToReturn.getTeamStats().add(new TeamStatsMessage(TeamNameMessage.values()[i]));
			}

			pListToReturn.add(pToReturn);
		}
		return pListToReturn;
	}

	public static List<Player> loadPlayersForCategoryAndSeason(Category cat, String season) {
		try {
			List<Player> pList = ofy().load().type(Player.class).order("name")
					.ancestor(Key.create(Season.class, season)).filter("category", cat.name()).list();
			return pList;
		} catch (Exception e) {
			LOGGER.warning("loadPlayersForCategory error: no Player entity");
			return null;
		}
	}

	/**
	 * Add one game played and goals for a given player in a given category and team
	 * 
	 * @param categoryName
	 *           category name
	 * @param teamName
	 *           team name
	 * @param playerName
	 *           player name
	 * @param goalsNumber
	 *           number of goals to add
	 */
	public static void addGamePlayedAndGoals(String categoryName, String teamName, String playerName,
			int goalsNumber) {
		LOGGER.info("Add game and " + goalsNumber + " goals for " + playerName + " from "
				+ categoryName + " " + teamName);
		Player p = ofy().load().type(Player.class)
				.ancestor(Key.create(Season.class, Util.currentSeasonDate())).filter("name", playerName)
				.filter("category", categoryName).first().now();
		if (p != null) {
			List<TeamStats> stats = loadPlayerStats(p.getId());
			for (int i = 0; i < stats.size(); i++) {
				if (stats.get(i).getTeamName().equals(teamName)) {
					stats.get(i).setGamesPlayed(stats.get(i).getGamesPlayed() + 1);
					stats.get(i).setGoals(stats.get(i).getGoals() + goalsNumber);
					ofy().save().entity(stats.get(i));
					break;
				}
			}
		}
	}

}
