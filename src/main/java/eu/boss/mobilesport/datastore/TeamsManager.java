package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.converters.TeamConverter;
import eu.boss.mobilesport.externalmodel.TeamMessage;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.model.Team;

/**
 * Datastore operations for Team and TeamCategory
 * 
 * @author Arnaud
 */
public class TeamsManager {

	private static final Logger LOGGER = Logger.getLogger(TeamsManager.class.getName());

	static {
		ObjectifyService.register(Team.class);
	}

	/**
	 * @param forMobileApp
	 *           true if it is for mobile application. We remove "URLs" for a light response
	 * @return
	 */
	public static List<TeamMessage> getAllTeamsForSeason(boolean forMobileApp, String season) {
		LOGGER.info("getAllTeams");
		List<Team> teams = ofy().load().type(Team.class).ancestor(Key.create(Season.class, season))
				.order("categoryName").order("teamName").list();
		List<TeamMessage> teamMsg = new ArrayList<>();

		if (forMobileApp) {
			for (Team t : teams) {
				teamMsg.add(TeamConverter.toExternalModel(t, true));
			}
		} else {
			for (Team t : teams) {
				teamMsg.add(TeamConverter.toExternalModel(t));
			}
		}

		return teamMsg;
	}

	public static Key<Team> saveTeamForSeason(Team team, String season) {
		LOGGER.info("saveTeams");
		return ofy().save().entity(team).now();
	}

	public static void saveTeamsForSeason(List<TeamMessage> teamList, String season) {
		LOGGER.info("saveCategoriesAndTeams");
		for (TeamMessage teamMsg : teamList) {
			Team team = TeamConverter.toInternalModel(teamMsg);
			team.setParent(Key.create(Season.class, season));
			saveTeamForSeason(team, season);
		}
	}

	public static Team loadTeamForSeason(String categoryName, String teamName, String seasonDate) {
		return ofy().load().type(Team.class).ancestor(Key.create(Season.class, seasonDate))
				.filter("categoryName =", categoryName).filter("teamName =", teamName).first().now();
	}

	public static Team loadTeamForSeasonAndId(long id, Key<Season> seasonKey) {
		//This is the right way to load an entity filtering on id and ancestor
		Key<Team> keyteam = Key.create(Team.class, id);
		return ofy().load().type(Team.class).ancestor(seasonKey).filterKey("=", keyteam).first().now();
	}
	
	public static void removeTeamListForSeason(final List<TeamMessage> teamsToDelete,
			final String season) {
		LOGGER.info("removeTeamList: list size: " + teamsToDelete.size());
		if (teamsToDelete != null && teamsToDelete.size() > 0) {
			ofy().transact(new VoidWork() {

				@Override
				public void vrun() {
					Key<Season> seasonKey = Key.create(Season.class, season);
					for (TeamMessage t : teamsToDelete) {
						Team tToDelete = TeamConverter.toUpdateDatastore(
								loadTeamForSeasonAndId(t.getId(), seasonKey), t, seasonKey);

						/* suppression de l'equipe... */
						// LOGGER.info("remove team: " + tToDelete.getName());
						if (tToDelete != null && tToDelete.getId() != null)
							ofy().delete().entity(tToDelete).now();
					}
				}
			});
		}
	}

	public static void persistTeamListForSeason(final List<TeamMessage> teamList,
			final String season) {
		LOGGER.info("persistTeamList: list size: " + teamList.size());
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Key<Season> seasonKey = Key.create(Season.class, season);
				for (TeamMessage t : teamList) {
					// Tout ce qui est execute dans cette methode appartient a la meme transaction
					Team tToUpdate = TeamConverter.toUpdateDatastore(
							loadTeamForSeasonAndId(t.getId(), seasonKey), t, seasonKey);
					/* Modification de l'objet... */
					// LOGGER.info("pToUpdate: " + pToUpdate.getName());
					ofy().save().entity(tToUpdate);
				}
			}
		});

	}

}
