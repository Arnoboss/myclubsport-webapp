package eu.boss.mobilesport.datastore;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.VoidWork;

import eu.boss.mobilesport.converters.FormationConverter;
import eu.boss.mobilesport.converters.FormationItemConverter;
import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.FormationItemMessage;
import eu.boss.mobilesport.externalmodel.FormationMessage;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.model.Formation;
import eu.boss.mobilesport.model.FormationItem;
import eu.boss.mobilesport.model.Match;

public class FormationManager {

	static {
		ObjectifyService.register(Formation.class);
		ObjectifyService.register(FormationItem.class);
	}

	private static final Logger LOGGER = Logger.getLogger(FormationManager.class.getName());

	public static Formation getFormationForMatch(Long matchId) {
		return ofy().load().type(Formation.class).ancestor(Key.create(Match.class, matchId)).first()
				.now();
	}

	public static Formation getFormation() {
		return ofy().load().type(Formation.class).first().now();
	}

	public static FormationMessage getFormationOfLastMatch(Category teamCategory,
			TeamNameMessage teamName) {
		// on affiche la formation de l'equipe lors du dernier match pour
		// eviter de tout refaire de zero

		// get last match
		try {
			Match match = LiveManager.getLastMatchForTeam(teamCategory, teamName);
			if (match != null && match.getId() != null) {
				// get associated formation
				return loadFormationMessage(match.getId());
			} else return null;
		} catch (Exception e) {
			LOGGER.warning("No old match found");
			e.printStackTrace();
			return null;
		}
	}

	public static List<FormationItem> getItemsForFormation(Long formationId) {
		return ofy().load().type(FormationItem.class)
				.ancestor(Key.create(Formation.class, formationId)).order("numItem").list();
	}

	public static Key<Formation> persistFormation(Formation formation) {
		// Genere un identifiant unique pour chaque formation. Evite les problemes de doublons
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		if (formation.getIdFormation() == null) formation.setIdFormation(datastoreService
				.allocateIds("FormationUniqueKind", 1L).getStart().getId());
		return ofy().save().entity(formation).now();
	}

	public static Key<FormationItem> persistFormationItem(FormationItem item) {
		return ofy().save().entity(item).now();
	}

	// TODO TEST: not working yet
	public static void deleteFormationAndItemsForMatch(final Long matchId) {
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				// On charge la formation pour recuperer son id
				Formation f = getFormationForMatch(matchId);

				// On supprime les items associes, puis la formation
				if (f != null && f.getIdFormation() != null) {
					ofy().delete().type(FormationItem.class)
							.parent(Key.create(Formation.class, f.getIdFormation()));

					ofy().delete().type(Formation.class).parent(Key.create(Match.class, matchId))
							.id(f.getIdFormation());
				}

			}
		});
	}

	public static void persistFormationAndItems(final FormationMessage currentFormation) {
		ofy().transact(new VoidWork() {

			@Override
			public void vrun() {
				Formation formation = FormationConverter.toInternalModel(currentFormation);
				persistFormation(formation);
				Long idParent = formation.getIdFormation();
				LOGGER.warning("persistFormation(formation).getId(): " + idParent);
				Key<Formation> formationParent = Key.create(Formation.class, idParent);
				for (FormationItemMessage itemMsg : currentFormation.getFormationItemList()) {
					if (itemMsg.getPlayer() != null) {
						FormationItem item = FormationItemConverter.toInternalModel(itemMsg);
						item.setFormation(formationParent);

						ofy().save().entity(item).now();
					}
				}

			}
		});
	}

	public static FormationMessage loadFormationMessage(Long matchId) {
		Formation f = null;
		FormationMessage formationMessage = null;
		List<FormationItem> items = null;
		List<FormationItemMessage> itemsMessage = new ArrayList<>();

		if (matchId != null) {
			f = getFormationForMatch(matchId);
			if (f != null) items = getItemsForFormation(f.getIdFormation());
			else return null;
		} else {
			// Utilise pour les tests. matchId n'est normalement jamais null
			f = getFormation();
			if (f != null) items = getItemsForFormation(f.getIdFormation());
			else return null;
		}
		formationMessage = FormationConverter.toExternalModel(f);

		for (FormationItem item : items) {
			itemsMessage.add(FormationItemConverter.toExternalModel(item));
		}

		formationMessage.setFormationItemList(itemsMessage);

		return formationMessage;
	}

	public static List<String> loadPlayerListForMatch(Long matchId) {
		try {
			return FormationItemConverter
					.toPlayerStringList(getItemsForFormation(getFormationForMatch(matchId)
							.getIdFormation()));
		} catch (NullPointerException e) {
			// No formation for the given match
			return new ArrayList<String>();
		}
	}
	// public static void updateFormationItem(final Long formationId, final
	// FormationItemMessage itemMessage) {
	// ofy().transact(new VoidWork() {
	//
	// @Override
	// public void vrun() {
	// FormationItem item =
	// ofy().load().type(FormationItem.class).parent(Key.create(Match.class,
	// formationId))
	// .id(itemMessage.getIdFormationItem()).now();
	//
	// item.setCoord(itemMessage.getCoord());
	// item.setNumItem(itemMessage.getNumItem());
	// item.setPlayerName(itemMessage.getPlayer().getName());
	// ofy().save().entity(item).now();
	// }
	// });
	// }

}
