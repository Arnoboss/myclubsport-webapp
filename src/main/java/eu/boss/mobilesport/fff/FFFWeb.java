package eu.boss.mobilesport.fff;

import java.io.IOException;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import eu.boss.mobilesport.exceptions.BossException;
import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.utils.Constant;

/**
 * This class parses data from FFF website to return only what we need for the app or the website
 * 
 * @author Arnaud
 */
public class FFFWeb {

	private static final Logger LOGGER = Logger.getLogger(FFFWeb.class.getName());

	// valeurs par defaut de l'application AS Coteaux. Utilise dans les vieilles versions
	private static final String RANKING_PREFIX_URL = "http://www.fff.fr/la-vie-des-clubs/137541/page-classement/competition-";
	private static final String CALENDAR_URL = "http://www.fff.fr/la-vie-des-clubs/137541/calendrier/liste-matchs-a-venir/";
	private static final String A_SUFFIX = "316703/phase-1/groupe-3";
	private static final String B_SUFFIX = "316704/phase-1/groupe-8";
	private static final String C_SUFFIX = "316705/phase-1/groupe-7";

	// private static final String A_SUFFIX = "303552/phase-1/groupe-4";
	// private static final String B_SUFFIX = "303572/phase-1/groupe-8";
	// private static final String C_SUFFIX = "303573/phase-1/groupe-7";

	/**
	 * Loads ranking from FFF website
	 * 
	 * @param url
	 * @param isReturningForApp
	 * @return
	 * @throws BossException
	 */
	public static String loadRankingFromUrl(String url, boolean isReturningForApp)
			throws BossException {
		Document doc;

		try {
			doc = Jsoup.connect(url).get();

			// recupere l'intitule du championnat
			Element title = doc.getElementsByClass("tabs_title").first();
			Element table = doc.getElementsByClass("classement").first();
			// Supprime les liens mais garde le texte.
			table.select("a").unwrap();
			doc.select("tr").last().remove();
			String champTitle = "<h3>" + title.unwrap() + "</h3>";

			if (isReturningForApp) {
				// If we returns the code for the app, we add style tags
				table.getElementsByClass("title_box").remove();
				return "<html><head><LINK rel=stylesheet type=\"text/css\" href=\"style.css\"></head><body>"
						+ champTitle.replace("<h3", "<h3 class='sub_tilte'")
						+ table.toString().replace("\"", "\'").replace("\n", "") + "</body></html>";
			}

			return (champTitle + table.toString()).replace("\"", "\'").replace("\n", "");
		} catch (Exception e) {
			return "<p>Aucun classement disponible actuellement</p> ";
		}
	}

	/**
	 * Loads ranking from fff with default URL.
	 * 
	 * @deprecated Used in version < 2.0
	 * @param team
	 *           we wants the rank
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return rank as HTML page
	 * @throws IOException
	 */
	@Deprecated
	public static String loadRanking(TeamNameMessage team, boolean isReturningForApp)
			throws BossException {
		String url;

		if (team == TeamNameMessage.A) url = RANKING_PREFIX_URL + A_SUFFIX;
		else if (team == TeamNameMessage.B) url = RANKING_PREFIX_URL + B_SUFFIX;
		else url = RANKING_PREFIX_URL + C_SUFFIX;

		return loadRankingFromUrl(url, isReturningForApp);
	}

	/**
	 * @param url
	 * @param isReturningForApp
	 * @return
	 * @throws BossException
	 */
	public static String loadAgendaFromUrl(String url, boolean isReturningForApp)
			throws BossException {
		Document doc;
		try {
			doc = Jsoup.connect(url).get();

			// Supprime les liens mais garde le texte
			Element table = doc.getElementsByClass("list_links").first();
			table.getElementsByClass("morInfo").remove();
			table.select("a").unwrap();

			if (isReturningForApp) {
				// If we returns the code for the app, we add style tags
				table.getElementsByClass("title_box").remove();
				return "<html><head><LINK rel=stylesheet type=\"text/css\" href=\"style.css\"></head><body>"
						+ table.toString().replace("\"", "\'").replace("\n", "").replace("h2", "h3")
						.replaceAll("//www", "http://www")
						+ "</body></html>";
			}
			return table.toString().replace("\"", "\'").replace("\n", "").replace("h2", "h3");
		} catch (Exception e) {
			return "<p>Aucun match prévu actuellement </p> ";
		}
	}

	/**
	 * Load agenda of the week with default URL.
	 * 
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return agenda as HTML page
	 * @throws IOException
	 */

	public static String loadAgenda(boolean isReturningForApp) throws BossException {
		return loadAgendaFromUrl(Constant.AGENDA_URL, isReturningForApp);
	}

	/**
	 * load calendar with default URL. Used in version < 2.0
	 * 
	 * @param team
	 *           team we wants the calendar
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return calendar as HTML page
	 * @throws IOException
	 */

	public static String loadCalendarFromUrl(String url, boolean isReturningForApp)
			throws BossException {
		Document doc;

		try {
			doc = Jsoup.connect(url).get();

			// Supprime les liens mais garde le texte
			Element table = doc.getElementsByClass("list_links").first();
			table.select("h2").remove();
			table.getElementsByClass("morInfo").remove();
			table.getElementsByClass("sub_tilte").first().unwrap().wrap("<h3>");
			table.select("a").unwrap();
			
			if (isReturningForApp) {
				// If we returns the code for the app, we add style tags
				table.getElementsByClass("title_box").remove();
				return "<html><head><LINK rel=stylesheet type=\"text/css\" href=\"style.css\"></head><body>"
						+ table.toString().replace("\"", "\'").replace("\n", "")
								.replace("toshow", "toshow2").replace("<h3", "<h3 class='sub_tilte'")
								.replaceAll("//www", "http://www")
						+ "</body></html>";
			}
			return (table.toString().replace("\"", "\'").replace("\n", ""));
		} catch (Exception e) {
			LOGGER.warning(e.toString());
			return "<p>Aucun calendrier disponible actuellement pour cette équipe </p> ";
		}
	}

	/**
	 * load calendar from fff with default URL. Used in version < 2.0
	 * 
	 * @deprecated
	 * @param team
	 *           team we wants the calendar
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return calendar as HTML page
	 * @throws IOException
	 */
	@Deprecated
	public static String loadCalendar(TeamNameMessage team, boolean isReturningForApp)
			throws BossException {
		String url;
		if (team == TeamNameMessage.A) url = CALENDAR_URL + A_SUFFIX;
		else if (team == TeamNameMessage.B) url = CALENDAR_URL + B_SUFFIX;
		else url = CALENDAR_URL + C_SUFFIX;

		return loadCalendarFromUrl(url, isReturningForApp);
	}

	/**
	 * Load results from FFF website
	 * 
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return results as HTML page
	 * @throws IOException
	 */
	public static String loadResultsFromUrl(String url, boolean isReturningForApp)
			throws BossException {
		Document doc;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			throw new BossException(
					"Erreur lors de la récupération des données depuis le site de la FFF");
		}
		// Supprime les liens mais garde le texte
		Element table = doc.getElementsByClass("list_links").first();
		try {
			table.getElementsByClass("morInfo").remove();
			table.select("a").unwrap();
			if (isReturningForApp) {
				table.getElementsByClass("title_box").remove();
				return "<html><head><LINK rel=stylesheet type=\"text/css\" href=\"style.css\"></head><body>"
						+ table.toString().replace("\"", "\'").replace("\n", "").replace("h2", "h3")
						.replaceAll("//www", "http://www")
						+ "</body></html>";
			}

			return table.toString().replace("\"", "\'").replace("\n", "").replace("h2", "h3");
		} catch (Exception e) {
			return "<p>Aucun résultat disponible actuellement</p> ";
		}

	}

	/**
	 * Load results from FFF website with default URL.
	 * 
	 * @param isReturningForApp
	 *           true if called by REST webservice
	 * @return results as HTML page
	 * @throws IOException
	 */
	public static String loadResults(boolean isReturningForApp) throws BossException {
		return loadResultsFromUrl(Constant.RESULTATS_URL, isReturningForApp);

	}
}
