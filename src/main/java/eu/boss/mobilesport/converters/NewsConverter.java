package eu.boss.mobilesport.converters;

import eu.boss.mobilesport.externalmodel.NewsMessage;
import eu.boss.mobilesport.externalmodel.NewsRequestMessage;
import eu.boss.mobilesport.model.News;

public class NewsConverter {

	public static NewsMessage toExternalModel(News news) {
		NewsMessage toReturn = new NewsMessage();
		toReturn.setContent(news.getContent());
		toReturn.setDate(news.getDate());
		toReturn.setImageBlobKey(news.getImageBlobKey());
		toReturn.setImageUrl(news.getImageUrl());
		toReturn.setTitle(news.getTitle());
		// Les autres attributs ne sont pas utilises dans le sens serveur / mobile
		return toReturn;
	}

	public static News toInternalModel(NewsMessage news) {
		News toReturn = new News();
		toReturn.setContent(news.getContent());
		toReturn.setDate(news.getDate());
		toReturn.setImageBlobKey(news.getImageBlobKey());
		toReturn.setImageUrl(news.getImageUrl());
		toReturn.setTitle(news.getTitle());
		return toReturn;
	}

	public static News toInternalModel(NewsRequestMessage news) {
		News toReturn = new News();
		toReturn.setContent(news.getContent());
		toReturn.setDate(news.getDate());
		toReturn.setImageUrl(news.getImageUrl());
		toReturn.setTitle(news.getTitle());
		toReturn.setNewsType(news.getNewsType());
		return toReturn;
	}
}
