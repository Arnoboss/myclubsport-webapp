package eu.boss.mobilesport.converters;

import com.googlecode.objectify.Key;

import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.externalmodel.PlayerPositionMessage;
import eu.boss.mobilesport.model.Player;
import eu.boss.mobilesport.model.Season;

/**
 * Class used to convert Player from external to internal model, and vice versa
 * 
 * @author Arnaud
 */
public class PlayerConverter {

	/**
	 * Converts internal to external model. NOTE: Season and TeamStats are not set !
	 * 
	 * @param player
	 * @return
	 */
	public static PlayerMessage toExternalModel(Player player) {
		PlayerMessage toReturn = new PlayerMessage();
		toReturn.setAge(player.getAge());
		toReturn.setName(player.getName());
		toReturn.setCategory(Category.getByName(player.getCategory()));
		toReturn.setPosition(PlayerPositionMessage.valueOf(player.getPosition()));
		toReturn.setId(player.getId());
		return toReturn;
	}

	/**
	 * Converts internal to external model. NOTE: Parent Key is not set in the converter ! If you
	 * need to save the teamstats in the datastore, you need to specify the parent key !
	 * 
	 * @param player
	 * @return
	 */
	public static Player toInternalModel(PlayerMessage player) {
		Player toReturn = new Player();
		toReturn.setAge(player.getAge());
		toReturn.setName(player.getName());
		toReturn.setPosition(player.getPosition().name());
		toReturn.setCategory(player.getCategory().name());
		toReturn.setId(player.getId());
		return toReturn;
	}

	/**
	 * Set external model values to internal model values
	 * 
	 * @param toStore
	 * @param player
	 * @return
	 */
	public static Player toUpdateDatastore(Player toStore, PlayerMessage player,
			Key<Season> seasonKey) {

		if (toStore == null) {
			toStore = new Player();
			toStore.setName(player.getName());
			toStore.setParent(seasonKey);
		}
		toStore.setId(player.getId());
		toStore.setAge(player.getAge());
		toStore.setPosition(player.getPosition().name());

		return toStore;
	}

	public static PlayerMessage toExternalModelWithNoId(Player p) {
		PlayerMessage pMsg = toExternalModel(p);
		pMsg.setId(null);
		return pMsg;
	}

}
