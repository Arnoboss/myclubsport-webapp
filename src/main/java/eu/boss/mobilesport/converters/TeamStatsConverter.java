package eu.boss.mobilesport.converters;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.Key;

import eu.boss.mobilesport.externalmodel.TeamNameMessage;
import eu.boss.mobilesport.externalmodel.TeamStatsMessage;
import eu.boss.mobilesport.model.Player;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.model.TeamStats;

/**
 * Class used to convert TeamStats from external to internal model, and vice versa
 * 
 * @author Arnaud
 */
public class TeamStatsConverter {

	/**
	 * Converts internal model to external model
	 * 
	 * @param teamstats
	 *           to convert
	 * @return
	 */
	public static TeamStatsMessage toExternalModel(TeamStats ts) {
		TeamStatsMessage toReturn = new TeamStatsMessage();
		toReturn.setAssists(ts.getAssists());
		toReturn.setAutoGoals(ts.getAutoGoals());
		toReturn.setGamesPlayed(ts.getGamesPlayed());
		toReturn.setGoals(ts.getGoals());
		toReturn.setRedCards(ts.getRedCards());
		toReturn.setTeamName(TeamNameMessage.valueOf(ts.getTeamName()));
		toReturn.setYellowCards(ts.getYellowCards());

		return toReturn;
	}

	/**
	 * Converts external model to internal model NOTE: the parent is not set in this converter ! If
	 * you need to save the teamstats in the datastore, you need to specify the parent key !
	 * 
	 * @param teamstats
	 *           to convert
	 * @return
	 */
	public static TeamStats toInternalModel(TeamStatsMessage ts) {
		TeamStats toReturn = new TeamStats();
		toReturn.setAssists(ts.getAssists());
		toReturn.setAutoGoals(ts.getAutoGoals());
		toReturn.setGamesPlayed(ts.getGamesPlayed());
		toReturn.setGoals(ts.getGoals());
		toReturn.setRedCards(ts.getRedCards());
		toReturn.setTeamName(ts.getTeamName().name());
		toReturn.setYellowCards(ts.getYellowCards());

		return toReturn;
	}

	public static List<TeamStatsMessage> toExternalModel(List<TeamStats> tsList) {
		List<TeamStatsMessage> listToReturn = new ArrayList<TeamStatsMessage>();
		for (TeamStats ts : tsList) {
			listToReturn.add(toExternalModel(ts));
		}
		return listToReturn;
	}

	/**
	 * Set external model values to internal model values
	 * 
	 * @param toStore
	 * @param player
	 * @return
	 */
	public static TeamStats toUpdateDatastore(TeamStats toStore, TeamStatsMessage ts, Long playerId,
			String category, Key<Season> seasonKey) {

		if (toStore == null) {
			toStore = new TeamStats();
			toStore.setParent(Key.create(Player.class, playerId));
		}

		toStore.setAssists(ts.getAssists());
		toStore.setAutoGoals(ts.getAutoGoals());
		toStore.setGamesPlayed(ts.getGamesPlayed());
		toStore.setGoals(ts.getGoals());
		toStore.setRedCards(ts.getRedCards());
		toStore.setTeamName(ts.getTeamName().name());
		toStore.setYellowCards(ts.getYellowCards());

		return toStore;
	}

	public static TeamStats toUpdateDatastoreOld(TeamStats toStore, TeamStatsMessage ts,
			String playerName, Key<Season> seasonKey) {

		if (toStore == null) {
			toStore = new TeamStats();
			toStore.setParent(Key.create(Player.class, playerName));
		}

		toStore.setAssists(ts.getAssists());
		toStore.setAutoGoals(ts.getAutoGoals());
		toStore.setGamesPlayed(ts.getGamesPlayed());
		toStore.setGoals(ts.getGoals());
		toStore.setRedCards(ts.getRedCards());
		toStore.setTeamName(ts.getTeamName().name());
		toStore.setYellowCards(ts.getYellowCards());

		return toStore;
	}
}
