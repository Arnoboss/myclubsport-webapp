package eu.boss.mobilesport.converters;

import eu.boss.mobilesport.externalmodel.MatchMessage;
import eu.boss.mobilesport.model.Match;

public class MatchConverter {

	// boolean used for version >= 2.0 to avoid bugs in anterior versions
	public static MatchMessage toExternalModel(Match match, boolean includeCatAndTeam) {
		MatchMessage toReturn = new MatchMessage();
		toReturn.setButeursTeam1(match.getButeursTeam1());
		toReturn.setButeursTeam2(match.getButeursTeam2());
		toReturn.setDate(match.getDate());
		toReturn.setGoalsTeam1(match.getGoalsTeam1());
		toReturn.setGoalsTeam2(match.getGoalsTeam2());
		toReturn.setStatus(match.getStatus());
		toReturn.setTeam1(match.getTeam1());
		toReturn.setTeam2(match.getTeam2());
		toReturn.setId(match.getId());
		toReturn.setIntitule(match.getIntitule());
		if (includeCatAndTeam) {
			toReturn.setCategoryName(match.getCategoryName());
			toReturn.setTeamName(match.getTeamName());
		}
		return toReturn;
	}

	public static Match toInternalModel(MatchMessage match) {
		Match toReturn = new Match();
		toReturn.setButeursTeam1(match.getButeursTeam1());
		toReturn.setButeursTeam2(match.getButeursTeam2());
		toReturn.setDate(match.getDate());
		toReturn.setGoalsTeam1(match.getGoalsTeam1());
		toReturn.setGoalsTeam2(match.getGoalsTeam2());
		toReturn.setStatus(match.getStatus());
		toReturn.setTeam1(match.getTeam1());
		toReturn.setTeam2(match.getTeam2());
		toReturn.setId(match.getId());
		toReturn.setIntitule(match.getIntitule());
		toReturn.setCategoryName(match.getCategoryName());
		toReturn.setTeamName(match.getTeamName());

		return toReturn;
	}
}
