package eu.boss.mobilesport.converters;

import java.util.Iterator;
import java.util.List;

import eu.boss.mobilesport.externalmodel.CommentListMessage;
import eu.boss.mobilesport.externalmodel.CommentMessage;
import eu.boss.mobilesport.model.Comment;

public class CommentConverter {

	public static CommentMessage toExternalModel(Comment comment) {
		CommentMessage toReturn = new CommentMessage();
		toReturn.setContent(comment.getContent());
		toReturn.setImportant(comment.isImportant());
		toReturn.setMatchId(comment.getParent().getId());
		toReturn.setMinuteEvent(comment.getMinuteEvent());
		toReturn.setId(comment.getId());
		return toReturn;
	}

	public static CommentListMessage toExternalModel(List<Comment> comments) {
		CommentListMessage toReturn = new CommentListMessage();
		Iterator<Comment> it = comments.iterator();
		while (it.hasNext()) {
			toReturn.addComment(toExternalModel(it.next()));
		}
		return toReturn;
	}

	public static Comment toInternalModel(CommentMessage comment) {
		Comment toReturn = new Comment();
		toReturn.setContent(comment.getContent());
		toReturn.setImportant(comment.isImportant());
		toReturn.setMinuteEvent(comment.getMinuteEvent());
		toReturn.setId(comment.getId());
		return toReturn;
	}
}
