package eu.boss.mobilesport.converters;

import eu.boss.mobilesport.externalmodel.NotificationIdMessage;
import eu.boss.mobilesport.model.NotificationId;

public class NotificationIdConverter {

	public static NotificationIdMessage toExternalModel(NotificationId notification) {
		NotificationIdMessage toReturn = new NotificationIdMessage();
		toReturn.setNotificationId(notification.getId());
		toReturn.setDeviceType(notification.getDeviceType());
		toReturn.setNotificationSubscription(notification.getNotificationSubscription());
		return toReturn;
	}

	public static NotificationId toInternalModel(NotificationIdMessage notification) {
		NotificationId toReturn = new NotificationId(notification.getNotificationId());
		toReturn.setDeviceType(notification.getDeviceType());
		toReturn.setNotificationSubscription(notification.getNotificationSubscription());
		return toReturn;
	}
}
