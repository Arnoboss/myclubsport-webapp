package eu.boss.mobilesport.converters;

import com.googlecode.objectify.Key;

import eu.boss.mobilesport.externalmodel.TeamMessage;
import eu.boss.mobilesport.model.Season;
import eu.boss.mobilesport.model.Team;

/**
 * Team Converter
 * 
 * @author Arnaud
 */
public class TeamConverter {

	public static TeamMessage toExternalModel(Team team) {
		TeamMessage toReturn = new TeamMessage();
		toReturn.setUrlCalendar(team.getUrlCalendar());
		toReturn.setUrlRank(team.getUrlRank());
		toReturn.setTeamName(team.getTeamName());
		toReturn.setCategory(team.getCategoryName());
		toReturn.setId(team.getId());
		return toReturn;
	}

	public static Team toInternalModel(TeamMessage team) {
		Team toReturn = new Team();
		toReturn.setUrlCalendar(team.getUrlCalendar());
		toReturn.setUrlRank(team.getUrlRank());
		toReturn.setTeamName(team.getTeamName());
		toReturn.setCategoryName(team.getCategory());
		toReturn.setId(team.getId());
		return toReturn;
	}

	public static TeamMessage toExternalModel(Team team, boolean removeUrls) {
		TeamMessage toReturn = new TeamMessage();
		if (!removeUrls) {
			toReturn.setUrlCalendar(team.getUrlCalendar());
			toReturn.setUrlRank(team.getUrlRank());
		}
		toReturn.setTeamName(team.getTeamName());
		toReturn.setCategory(team.getCategoryName());
		toReturn.setId(team.getId());
		return toReturn;
	}

	/**
	 * Set external model values to internal model values
	 * 
	 * @param toStore
	 * @param team
	 * @return
	 */
	public static Team toUpdateDatastore(Team toStore, TeamMessage team, Key<Season> seasonKey) {

		if (toStore == null) {
			toStore = new Team();
			toStore.setCategoryName(team.getCategory());
			toStore.setTeamName(team.getTeamName());
			toStore.setParent(seasonKey);
		}
		toStore.setId(team.getId());
		toStore.setUrlCalendar(team.getUrlCalendar());
		toStore.setUrlRank(team.getUrlRank());

		return toStore;
	}

}
