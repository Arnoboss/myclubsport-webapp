package eu.boss.mobilesport.converters;

import java.util.ArrayList;
import java.util.List;

import eu.boss.mobilesport.externalmodel.FormationItemMessage;
import eu.boss.mobilesport.externalmodel.PlayerMessage;
import eu.boss.mobilesport.model.FormationItem;

public class FormationItemConverter {

	/**
	 * @param FormationItem
	 *           to convert
	 * @return converted FormationItemMessage
	 */
	public static FormationItemMessage toExternalModel(FormationItem item) {
		FormationItemMessage toReturn = new FormationItemMessage();
		toReturn.setCoord(item.getCoord());
		toReturn.setNumItem(item.getNumItem());
		toReturn.setPlayer(new PlayerMessage(item.getPlayerName()));
		toReturn.setIdFormationItem(item.getIdFormationItem());
		return toReturn;
	}

	/**
	 * Converts external model to internal model NOTE: the parent is not set in this converter !
	 * 
	 * @param FormationItemMessage
	 *           to convert
	 * @return converted FormationItem
	 */
	public static FormationItem toInternalModel(FormationItemMessage item) {
		FormationItem toReturn = new FormationItem();
		toReturn.setCoord(item.getCoord());
		toReturn.setNumItem(item.getNumItem());
		if (item.getPlayer() != null) toReturn.setPlayerName(item.getPlayer().getName());
		toReturn.setIdFormationItem(item.getIdFormationItem());
		return toReturn;
	}

	/**
	 * Converts into simple String list the player names
	 * 
	 * @return
	 */
	public static List<String> toPlayerStringList(List<FormationItem> items) {
		ArrayList<String> compoNames = new ArrayList<>();
		for (FormationItem item : items) {
			if (item.getPlayerName() != null) compoNames.add(item.getPlayerName());
		}
		return compoNames;
	}
}
