package eu.boss.mobilesport.converters;

import com.googlecode.objectify.Key;

import eu.boss.mobilesport.externalmodel.FormationMessage;
import eu.boss.mobilesport.model.Formation;
import eu.boss.mobilesport.model.Match;

public class FormationConverter {

	public static FormationMessage toExternalModel(Formation formation) {
		FormationMessage toReturn = new FormationMessage();
		toReturn.setIdFormation(formation.getIdFormation());
		toReturn.setFormationName(formation.getFormationName());
		toReturn.setMatchId(formation.getMatchParent().getId());
		toReturn.setNbTitulaires(formation.getNbTitulaires());
		toReturn.setNbRemplacants(formation.getNbRemplacants());
		return toReturn;
	}

	public static Formation toInternalModel(FormationMessage formation) {
		Formation toReturn = new Formation();
		toReturn.setIdFormation(formation.getIdFormation());
		toReturn.setFormationName(formation.getFormationName());
		toReturn.setNbTitulaires(formation.getNbTitulaires());
		toReturn.setNbRemplacants(formation.getNbRemplacants());
		if (formation.getMatchId() != null) toReturn.setMatchParent(Key.create(Match.class,
				formation.getMatchId()));
		return toReturn;
	}
}
