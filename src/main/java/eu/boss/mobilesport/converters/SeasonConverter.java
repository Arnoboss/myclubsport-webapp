package eu.boss.mobilesport.converters;

import eu.boss.mobilesport.externalmodel.SeasonMessage;
import eu.boss.mobilesport.model.Season;

/**
 * Class used to convert Season from external to internal model, and vice versa
 * 
 * @author Arnaud
 */
public class SeasonConverter {

	public static SeasonMessage toExternalModel(Season season) {
		SeasonMessage toReturn = new SeasonMessage();
		toReturn.setSeasonDate(season.getSeasonDate());
		return toReturn;
	}

	public static Season toInternalModel(SeasonMessage season) {

		Season toReturn = new Season();
		toReturn.setSeasonDate(season.getSeasonDate());
		return toReturn;
	}
}
