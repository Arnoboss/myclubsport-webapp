package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import eu.boss.mobilesport.converters.MatchConverter;
import eu.boss.mobilesport.model.Match;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchListMessage implements Serializable {

	private static final long serialVersionUID = 4480199313941208476L;
	private final List<MatchMessage> match = new ArrayList<MatchMessage>();

	public List<MatchMessage> getMatchs() {
		return match;
	}

	public void addMatch(MatchMessage match) {
		this.match.add(match);
	}

	public void setMatch(Collection<MatchMessage> match) {
		for (MatchMessage n : match) {
			addMatch(n);
		}
	}

	@JsonIgnore
	public void setMatch2(Collection<Match> match, boolean includeCatAndTeam) {
		for (Match n : match) {
			addMatch(MatchConverter.toExternalModel(n, includeCatAndTeam));
		}
	}
}
