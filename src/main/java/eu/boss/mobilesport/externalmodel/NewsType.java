package eu.boss.mobilesport.externalmodel;

public enum NewsType {

	AUTRE("Autre"), CONVOCATION("Convocations"), AGENDA("Agenda"), RESULTAT("Résultats");

	private String label;

	private NewsType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}
	
	public static NewsType getByLabel(String newsStr) {
		for (NewsType n : values()) {
			if (n.label.equals(newsStr)) {
				return n;
			}
		}
		return null;
	}

}
