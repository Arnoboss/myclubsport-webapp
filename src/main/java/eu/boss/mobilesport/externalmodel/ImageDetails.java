package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Decrit une image a uploader sur le serveur
 * 
 * @author Arnaud
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageDetails implements Serializable {

	private static final long serialVersionUID = -7256744844242648039L;

	private byte[] imageAsBytes;
	private String imageName;
	private String imageMimeType;

	public byte[] getImageAsBytes() {
		return imageAsBytes;
	}

	public void setImageAsBytes(byte[] imageAsBytes) {
		this.imageAsBytes = imageAsBytes;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageMimeType() {
		return imageMimeType;
	}

	public void setImageMimeType(String imageMimeType) {
		this.imageMimeType = imageMimeType;
	}
}
