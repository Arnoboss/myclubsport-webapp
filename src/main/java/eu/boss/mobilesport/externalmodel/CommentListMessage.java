package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import eu.boss.mobilesport.converters.CommentConverter;
import eu.boss.mobilesport.model.Comment;

/**
 * @link Comment
 * @author Arnaud
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentListMessage implements Serializable {

	private static final long serialVersionUID = 1858784687017951043L;
	private final List<CommentMessage> comments = new ArrayList<CommentMessage>();

	public List<CommentMessage> getComments() {
		return comments;
	}

	public void addComment(CommentMessage comment) {
		this.comments.add(comment);
	}

	public void setComment(Collection<CommentMessage> comment) {
		for (CommentMessage n : comment) {
			addComment(n);
		}
	}

	@JsonIgnore
	public void setComment2(Collection<Comment> comment) {
		for (Comment n : comment) {
			addComment(CommentConverter.toExternalModel(n));
		}
	}
}
