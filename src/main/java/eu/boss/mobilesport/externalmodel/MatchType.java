package eu.boss.mobilesport.externalmodel;

public enum MatchType {

	FRIENDLY("Amical"), OFFICIAL("Officiel");

	private String label;

	private MatchType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}
	
	public static MatchType getByLabel(String newsStr) {
		for (MatchType n : values()) {
			if (n.label.equals(newsStr)) {
				return n;
			}
		}
		return null;
	}

}
