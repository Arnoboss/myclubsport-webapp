package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerListMessage implements Serializable {

	private static final long serialVersionUID = 8855221933026063597L;

	private final List<PlayerMessage> players = new ArrayList<PlayerMessage>();

	public List<PlayerMessage> getPlayers() {
		return players;
	}

	public void setPlayers(Collection<PlayerMessage> players) {
		for (PlayerMessage p : players) {
			addPlayer(p);
		}
	}

	public void addPlayer(PlayerMessage player) {
		this.players.add(player);
	}

	@Override
	public String toString() {
		String s = "";
		for (PlayerMessage p : players) {
			s += p.toString();
		}
		return s;
	}

	public String toStringHTML() {
		String s = "";
		for (PlayerMessage p : players) {
			s += p.toStringHTML();
		}
		return s;
	}
}
