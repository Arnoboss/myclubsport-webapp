package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @link Team
 * @author Arnaud
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamMessage implements Serializable {

	private static final long serialVersionUID = 4719436006365760109L;

	private Long id;
	private String category;
	private String teamName;

	private String urlCalendar;
	private String urlRank;
	private String responsibles;

	public String getUrlCalendar() {
		return urlCalendar;
	}

	public void setUrlCalendar(String urlCalendar) {
		this.urlCalendar = urlCalendar;
	}

	public String getUrlRank() {
		return urlRank;
	}

	public void setUrlRank(String urlRank) {
		this.urlRank = urlRank;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getCategory() {
		return category;
	}

	@JsonIgnore
	public String getDecodedCategory() {
		return Category.valueOf(category).getName();
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResponsibles() {
		return responsibles;
	}

	public void setResponsibles(String responsibles) {
		this.responsibles = responsibles;
	}

}
