package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormationMessage implements Serializable {

	private static final long serialVersionUID = -1099716944992693722L;

	private Long idFormation;
	private String formationName;
	private List<FormationItemMessage> formationItemList;
	private Long matchId;
	private int nbTitulaires;
	private int nbRemplacants;

	public FormationMessage() {
		nbTitulaires = 11;
		nbRemplacants = 3;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getFormationName() {
		return formationName;
	}

	public void setFormationName(String formationName) {
		this.formationName = formationName;
	}

	public List<FormationItemMessage> getFormationItemList() {
		return formationItemList;
	}

	public void setFormationItemList(List<FormationItemMessage> formationItemList) {
		this.formationItemList = formationItemList;
	}

	public Long getIdFormation() {
		return idFormation;
	}

	public void setIdFormation(Long idFormation) {
		this.idFormation = idFormation;
	}

	public int getNbTitulaires() {
		return nbTitulaires;
	}

	public void setNbTitulaires(int nbTitulaires) {
		this.nbTitulaires = nbTitulaires;
	}

	public int getNbRemplacants() {
		return nbRemplacants;
	}

	public void setNbRemplacants(int nbRemplacants) {
		this.nbRemplacants = nbRemplacants;
	}

	@JsonIgnore
	public int getNumberOfPlayers() {
		return nbTitulaires + nbRemplacants;
	}

	/**
	 * @return la liste des remplacants
	 */
	@JsonIgnore
	public String getRemplacants() {
		String remplacants = "";
		if (formationItemList != null && formationItemList.size() > nbTitulaires) {
			for (int i = nbTitulaires; i < (nbTitulaires + nbRemplacants); i++) {
				if (formationItemList.get(i) != null && formationItemList.get(i).getPlayer() != null
						&& formationItemList.get(i).getPlayer().getName() != null) {
					remplacants += formationItemList.get(i).getPlayer().getName() + " - ";
				}
			}
		}
		if (remplacants.lastIndexOf("-") == -1) return remplacants;
		return remplacants.substring(0, remplacants.lastIndexOf("-"));
	}

	public void resetFormationIds() {
		this.idFormation = null;
		for (FormationItemMessage item : formationItemList) {
			item.setIdFormationItem(null);
		}

	}

	/** Retourne la liste des convoques */
	@JsonIgnore
	public String getConvocs() {
		String convocs = "<div> <span style=\"text-decoration: underline;\"> Les convoqués</span>: ";
		if (formationItemList != null && formationItemList.size() > 11) {
			for (int i = 0; i < formationItemList.size(); i++) {
				if (formationItemList.get(i).getPlayer() != null
						&& formationItemList.get(i).getPlayer().getName() != null) {
					convocs += formationItemList.get(i).getPlayer().getName() + " - ";
				}
			}
		}
		// On retire le dernier tiret inutile et on ferme da div (retour a la ligne)
		return convocs.substring(0, convocs.lastIndexOf("-")) + "</div>";
	}
}
