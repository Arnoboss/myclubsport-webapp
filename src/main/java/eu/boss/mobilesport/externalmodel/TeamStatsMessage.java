package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamStatsMessage implements Serializable, Comparable<TeamStatsMessage> {

	private static final long serialVersionUID = -124049732025180405L;

	private TeamNameMessage teamName;
	private int goals;
	private int assists;
	private int autoGoals;
	private int yellowCards;
	private int redCards;
	private int gamesPlayed;

	public TeamStatsMessage() {
	}

	public TeamStatsMessage(TeamNameMessage team) {
		this.teamName = team;
		this.goals = 0;
		this.assists = 0;
		this.autoGoals = 0;
		this.yellowCards = 0;
		this.redCards = 0;
		this.gamesPlayed = 0;
	}

	public TeamNameMessage getTeamName() {
		return teamName;
	}

	public void setTeamName(TeamNameMessage teamName) {
		this.teamName = teamName;
	}

	public int getGoals() {
		return goals;
	}

	public void setGoals(int goals) {
		this.goals = goals;
	}

	public int getAssists() {
		return assists;
	}

	public void setAssists(int assists) {
		this.assists = assists;
	}

	public int getAutoGoals() {
		return autoGoals;
	}

	public void setAutoGoals(int autoGoals) {
		this.autoGoals = autoGoals;
	}

	public int getYellowCards() {
		return yellowCards;
	}

	public void setYellowCards(int yellowCards) {
		this.yellowCards = yellowCards;
	}

	public int getRedCards() {
		return redCards;
	}

	public void setRedCards(int redCards) {
		this.redCards = redCards;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	@Override
	public String toString() {
		return "team name: " + getTeamName() + "\n games played: " + getGamesPlayed() + "\n goals : "
				+ getGoals() + "\n " + "assists: " + assists + "\n yellow cards: " + yellowCards
				+ "\n red cards: " + redCards + "\n\n";
	}

	public String toStringHTML() {
		return "team name: " + getTeamName() + "</br> games played: " + getGamesPlayed()
				+ "</br> goals : " + getGoals() + "</br>" + "assists: " + assists
				+ "</br>yellow cards: " + yellowCards + "</br> red cards: " + redCards + "</br></br>";
	}

	/**
	 * Sort results by team name
	 */
	@Override
	public int compareTo(TeamStatsMessage ts) {
		return this.teamName.name().compareTo(ts.getTeamName().name());
	}
}
