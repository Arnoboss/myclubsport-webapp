package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SeasonMessage implements Serializable {

	private static final long serialVersionUID = 3061720754594455143L;
	String seasonDate;

	public SeasonMessage() {
	}

	public String getSeasonDate() {
		return seasonDate;
	}

	public void setSeasonDate(String seasonDate) {
		this.seasonDate = seasonDate;
	}

}
