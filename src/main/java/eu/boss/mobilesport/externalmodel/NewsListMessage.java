package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import eu.boss.mobilesport.converters.NewsConverter;
import eu.boss.mobilesport.model.News;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsListMessage implements Serializable {

	private static final long serialVersionUID = 8886656922658477137L;
	private final List<NewsMessage> news = new ArrayList<NewsMessage>();

	public List<NewsMessage> getNews() {
		return news;
	}

	public void addNews(NewsMessage news) {
		this.news.add(news);
	}

	public void setNews(Collection<NewsMessage> news) {
		this.news.clear();
		for (NewsMessage n : news) {
			addNews(n);
		}
	}

	@JsonIgnore
	public void setNews2(Collection<News> news) {
		for (News n : news) {
			addNews(NewsConverter.toExternalModel(n));
		}
	}
}
