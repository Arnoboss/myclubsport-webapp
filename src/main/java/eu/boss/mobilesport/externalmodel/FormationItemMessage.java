package eu.boss.mobilesport.externalmodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormationItemMessage implements Serializable, Comparable<FormationItemMessage> {

	private static final long serialVersionUID = 2496762932522958273L;

	Long idFormationItem;
	// used for css display
	private String rowClass;
	private Integer numItem;
	private Integer coord;
	// Le player n'est pas charge entierement. On ne set que son nom
	private PlayerMessage player = new PlayerMessage();

	public Long getIdFormationItem() {
		return idFormationItem;
	}

	public void setIdFormationItem(Long idFormationItem) {
		this.idFormationItem = idFormationItem;
	}

	public Integer getNumItem() {
		return numItem;
	}

	public void setNumItem(Integer numItem) {
		this.numItem = numItem;
	}

	public Integer getCoord() {
		return coord;
	}

	public void setCoord(Integer coord) {
		this.coord = coord;
	}

	public PlayerMessage getPlayer() {
		return player;
	}

	public void setPlayer(PlayerMessage player) {
		this.player = player;
	}

	public String getRowClass() {
		if (numItem == null || numItem == 0) rowClass = "hiddenRow";
		else rowClass = "";
		return rowClass;
	}

	public void setRowClass(String rowClass) {
		this.rowClass = rowClass;
	}

	@Override
	public int compareTo(FormationItemMessage o) {
		return this.numItem.compareTo(o.getNumItem());
	}

}
