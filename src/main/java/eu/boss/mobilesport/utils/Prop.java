package eu.boss.mobilesport.utils;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class Prop {

	private final static Logger LOGGER = Logger.getLogger(Prop.class.getName());

	private static ConcurrentHashMap<EnumProperties, Prop> props = new ConcurrentHashMap<EnumProperties, Prop>();

	private Properties properties = null;

	private String propertyFileName;

	/**
	 * @param fileName
	 */
	private Prop(String fileName) {

		InputStream inputStream = Prop.class.getResourceAsStream("/" + fileName);
		if (inputStream == null) {
			LOGGER.severe("Error reading property file: " + fileName);
		} else {
			propertyFileName = fileName;
			properties = new Properties();
			try {
				properties.load(inputStream);
			} catch (IOException e) {
				properties = null;
				LOGGER.info("Error loading property file: " + fileName);
			}
		}
	}

	/**
	 * @param property
	 * @return
	 */
	public static Prop getInstance(EnumProperties property) {
		Prop prop;
		if (!props.contains(property)) {
			LOGGER.info("Loading property file: <" + property.getFileName() + ">");

			prop = new Prop(property.getFileName());
			props.put(property, prop);
		} else {
			prop = props.get(property);
		}
		return prop;
	}

	/**
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		if (properties == null) {
			LOGGER.info(
					"Could not load key: <" + key + "> from propertiy file: <" + propertyFileName + ">");

			return null;
		}
		return properties.getProperty(key);
	}

	/**
	 * @param key
	 * @return
	 */
	public String getReplacedString(String key, String... params) {
		String result = getString(key);
		if (params == null || params.length == 0) {
			return result;
		}
		MessageFormat format = new MessageFormat(result);
		return format.format(params);
	}

	/**
	 * @param key
	 * @return
	 */
	public boolean getBoolean(String key) {
		String s = getString(key);
		if (!s.isEmpty() && ("1".equals(s) || "true".equals(s))) {
			return true;
		}
		return false;
	}

	/**
	 * @param key
	 * @return
	 */
	public Integer getInt(String key) {
		String s = getString(key);
		Integer result = null;
		try {
			result = Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			LOGGER.severe("Unable to get int value for key:<" + key + "> and value:<" + s + ">");

		}
		return result;
	}

	/**
	 * @param key
	 * @return
	 */
	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}
}