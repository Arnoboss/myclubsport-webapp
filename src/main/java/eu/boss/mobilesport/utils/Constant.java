package eu.boss.mobilesport.utils;

import com.google.appengine.api.utils.SystemProperty;

public class Constant {

	public static final String CONTACT_EMAIL = "caracolee57@gmail.com";
	public static final String GENERIC_PWD = "radiat3ur";
	public static final String CONTACT_RECIPIENT = "arnaud.bossmann@gmail.com";
	public static final String RESULTATS_URL = "http://www.fff.fr/la-vie-des-clubs/137541/resultats";
	public static final String AGENDA_URL = "http://www.fff.fr/la-vie-des-clubs/137541/agenda";

	// APNS
	public static final String API_KEY = "AIzaSyDAFybd_OwhuBG_N6dHw408gMf1jvO3-ak";

	public static final boolean IS_PROD;
	public static final String BUCKET_NAME;
	public static final String FOLDER_NEWS = "/news";
	public static final String FOLDER_GALLERY = "gallery/";
	public static final String FOLDER_GALLERY2 = "/gallery";
	public static final String IMAGE_PUBLIC_PATH;

	public static final String AGENDA_IMG_URL;
	public static final String RESULTS_IMG_URL;
	public static final String CONVOCS_IMG_URL;
	public static final String ADMIN_INIT_USER = "coteauxAdmin";
	public static final String ADMIN_INIT_PWD = "ASCadmin2015";

	static {

		IS_PROD = !SystemProperty.applicationId.get().contains("dev");
		BUCKET_NAME = SystemProperty.applicationId.get() + ".appspot.com";
		IMAGE_PUBLIC_PATH = "https://storage.googleapis.com/" + BUCKET_NAME;

		AGENDA_IMG_URL = IMAGE_PUBLIC_PATH + "/news/agenda.png";
		RESULTS_IMG_URL = IMAGE_PUBLIC_PATH + "/news/resultats.png";
		CONVOCS_IMG_URL = IMAGE_PUBLIC_PATH + "/news/convocs.png";
	}
}