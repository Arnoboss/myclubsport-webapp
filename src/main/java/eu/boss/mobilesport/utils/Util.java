package eu.boss.mobilesport.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Util {

	public static String currentSeasonDate() {
		Date current = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(current);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		if (month < 7) return year - 1 + "-" + year;

		else return year + "-" + (year + 1);
	}

	/**
	 * @return la liste des saisons sportives depuis 2015/2016
	 */
	public static List<String> getSeasonsList() {
		List<String> seasonList = new ArrayList<>();
		String current = currentSeasonDate();
		boolean stop = false;
		int startSeason = 2015;
		while (!stop) {
			String season = startSeason + "-" + (startSeason + 1);
			seasonList.add(season);
			if (season.equals(current)) stop = true;
			startSeason++;
		}
		return seasonList;

	}
}
