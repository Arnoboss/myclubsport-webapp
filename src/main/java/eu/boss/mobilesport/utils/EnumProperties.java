package eu.boss.mobilesport.utils;

public enum EnumProperties {
	MAIL("mail.properties"), COMMUNICATION("communication.properties"), OFB_SEL_MSG(
			"ofbselmsg.properties"), AUTOTASK("autoTask.properties"), LABELS("labels_web.properties");

	private final String fileName;

	private EnumProperties(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}
}