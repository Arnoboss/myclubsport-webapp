package eu.boss.mobilesport.pushnotifications;

import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.DeferredTask;

import apns.ApnsConnection;
import apns.ApnsConnectionFactory;
import apns.ApnsConnectionPool;
import apns.ApnsException;
import apns.ApnsRuntimeException;
import apns.CannotOpenConnectionException;
import apns.CannotUseConnectionException;
import apns.DefaultApnsConnectionFactory;
import apns.DefaultPushNotificationService;
import apns.PayloadException;
import apns.PushNotification;
import apns.PushNotificationService;
import apns.keystore.ClassPathResourceKeyStoreProvider;
import apns.keystore.KeyStoreProvider;
import apns.keystore.KeyStoreType;
import eu.boss.mobilesport.utils.Constant;

public class SendPushNotificationToApnsTask implements DeferredTask {

	private static final long serialVersionUID = 5570156279303032521L;
	private static final Logger LOGGER = Logger
			.getLogger(SendPushNotificationToApnsTask.class.getName());

	private final PushNotification mPushNotification;

	private static volatile ApnsConnectionFactory sApnsConnectionFactory;
	private static volatile ApnsConnectionPool sApnsConnectionPool;
	private static volatile PushNotificationService sPushNotificationService;
	private static final int APNS_CONNECTION_POOL_CAPACITY = 5;

	public SendPushNotificationToApnsTask(PushNotification pushNotification) {
		mPushNotification = pushNotification;
	}

	@Override
	public void run() {
		try {
			trySendingPushNotification();
		} catch (CannotOpenConnectionException e) {
			LOGGER.severe("CannotOpenConnectionException: " + e.toString());
			throw new RuntimeException("Could not connect to APNS", e);
		} catch (CannotUseConnectionException e) {
			LOGGER.severe("CannotUseConnectionException: " + e.toString());
			// Cela permet d'eviter les relances intempestives en cas de token expire (voir bug
			// Mantis BT num 23). Si aucune exception n'est lancee, la Task est consideree comme
			// realisee et ne sera pas relancee.
			// A double tranchant en cas de vraie erreur
			// throw new RuntimeException("Could not send: " + mPushNotification, e);
		} catch (PayloadException e) {
			LOGGER.severe("Could not send push notification (dropping task): " + e.toString());
		}

	}

	private void trySendingPushNotification()
			throws CannotOpenConnectionException, CannotUseConnectionException, PayloadException {
		ApnsConnection apnsConnection = getApnsConnectionPool().obtain();
		if (apnsConnection == null) {
			apnsConnection = openConnection();
		}

		try {
			LOGGER.info("Sending push notification: {} " + mPushNotification);
			getPushNotificationService().send(mPushNotification, apnsConnection);
			getApnsConnectionPool().put(apnsConnection);
		} catch (CannotUseConnectionException e) {
			LOGGER.info("Could not send push notification");
			LOGGER.info("failed token: " + mPushNotification.getDeviceTokens());

			// apnsConnection = openConnection();
			// LOGGER.info("Retrying sending push notification");
			// getPushNotificationService().send(mPushNotification, apnsConnection);
			// getApnsConnectionPool().put(apnsConnection);
		}
	}

	private static ApnsConnectionPool getApnsConnectionPool() {
		if (sApnsConnectionPool == null) {
			synchronized (SendPushNotificationToApnsTask.class) {
				if (sApnsConnectionPool == null) {
					sApnsConnectionPool = new ApnsConnectionPool(APNS_CONNECTION_POOL_CAPACITY);
				}
			}
		}
		return sApnsConnectionPool;
	}

	private static PushNotificationService getPushNotificationService() {
		if (sPushNotificationService == null) {
			synchronized (SendPushNotificationToApnsTask.class) {
				if (sPushNotificationService == null) {
					sPushNotificationService = new DefaultPushNotificationService();
				}
			}
		}
		return sPushNotificationService;
	}

	private static ApnsConnection openConnection() throws CannotOpenConnectionException {
		LOGGER.info("Connecting to APNS");
		return getApnsConnectionFactory().openPushConnection();
	}

	public static ApnsConnectionFactory getApnsConnectionFactory() {
		boolean usingProductionApns = Constant.IS_PROD;

		if (sApnsConnectionFactory == null) {
			synchronized (SendPushNotificationToApnsTask.class) {
				if (sApnsConnectionFactory == null) {
					DefaultApnsConnectionFactory.Builder builder = DefaultApnsConnectionFactory.Builder
							.get();
					if (usingProductionApns) {
						KeyStoreProvider ksp = new ClassPathResourceKeyStoreProvider("eu/boss/mobilesport/prod_apns.p12",
								KeyStoreType.PKCS12, Constant.GENERIC_PWD.toCharArray());
						"radiat3ur".toCharArray();
						builder.setProductionKeyStoreProvider(ksp);
					} else {
						KeyStoreProvider ksp = new ClassPathResourceKeyStoreProvider("eu/boss/mobilesport/dev_apns.p12",
								KeyStoreType.PKCS12, Constant.GENERIC_PWD.toCharArray());
						"radiat3ur".toCharArray();
						builder.setSandboxKeyStoreProvider(ksp);
					}
					try {
						sApnsConnectionFactory = builder.build();
					} catch (ApnsException e) {
						throw new ApnsRuntimeException("Could not create APNS connection factory", e);
					}
				}
			}
		}
		return sApnsConnectionFactory;

	}

}
