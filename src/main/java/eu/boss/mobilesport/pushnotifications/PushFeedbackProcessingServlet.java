package eu.boss.mobilesport.pushnotifications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import apns.ApnsConnection;
import apns.ApnsConnectionFactory;
import apns.DefaultFeedbackService;
import apns.FailedDeviceToken;
import apns.FeedbackService;
import eu.boss.mobilesport.datastore.NotificationIdManager;
import eu.boss.mobilesport.model.NotificationId;
import eu.boss.mobilesport.utils.Constant;

/**
 * Servlet implementation class PushFeedbackProcessingServlet
 */
public class PushFeedbackProcessingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(PushFeedbackProcessingServlet.class
			.getName());

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.info("PushFeedbackProcessingServlet cron call ");
		try {
			// Retrieving the list of inactive devices from the Feedback service
			if (Constant.IS_PROD) {
				ApnsConnectionFactory acf = SendPushNotificationToApnsTask.getApnsConnectionFactory(); // obtain
																																	// one
				ApnsConnection connection = acf.openFeedbackConnection();
				FeedbackService fs = new DefaultFeedbackService();
				List<FailedDeviceToken> failedTokens = fs.read(connection);
				List<NotificationId> idListToRemove = new ArrayList<NotificationId>();

				for (FailedDeviceToken failedToken : failedTokens) {
					idListToRemove.add(new NotificationId(failedToken.getDeviceToken()));
					LOGGER.info("token to remove: " + failedToken.getDeviceToken() + " timestamp: "
							+ failedToken.getFailTimestamp());
				}

				NotificationIdManager.removeNotificationIdList(idListToRemove);
			}

		} catch (Exception e) {
			LOGGER.severe("Retrieving the list of inactive devives failed with CommunicationException:"
					+ e.toString());
		}
	}
}
