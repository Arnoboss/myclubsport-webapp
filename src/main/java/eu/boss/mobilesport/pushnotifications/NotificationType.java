package eu.boss.mobilesport.pushnotifications;

public enum NotificationType {

	NEWS("news"), LIVE("live"), OTHER("");

	private String label;

	private NotificationType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static NotificationType getByLabel(String newsStr) {
		for (NotificationType n : values()) {
			if (n.label.equals(newsStr)) {
				return n;
			}
		}
		return null;
	}

}
