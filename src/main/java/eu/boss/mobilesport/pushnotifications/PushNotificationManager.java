package eu.boss.mobilesport.pushnotifications;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.appengine.api.taskqueue.DeferredTask;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

import apns.PushNotification;
import eu.boss.mobilesport.datastore.NotificationIdManager;
import eu.boss.mobilesport.model.NotificationId;
import eu.boss.mobilesport.utils.Constant;

/**
 * Notification manager based on google sample
 * 
 * @author Arnaud
 */
public class PushNotificationManager {

	// push id de l'iphone de test en production
	// "426a16664a62ad9b8f5b50cf3f76f799e6fa7c3bd118b8a12b45c0d9215faf1e"
	// push id de l'iphone de test en dev
	// 4ce46b65408e109d7c0ae2391a12638e26c981d39f262e6984ff90bd406e3f7b
	// notification id Celiouninoute
	// "fcdcb5fbb5ecc9b80399558b1d4dc04584c436ccef5ab9360a2ffe517ed1451f"

	private static final Logger LOGGER = Logger.getLogger(PushNotificationManager.class.getName());

	public static boolean sendNotificationMsgToAll(String title, String msg, NotificationType type,
			String urlImage, String detailsId) {
		// Substring message to send. iOS supports max 2048 bits
		if (msg.length() > 750) msg = msg.substring(0, 750) + "...";
		return sendNotificationMsgTo(title, msg, type, urlImage, detailsId, true, true);
	}

	/**
	 * @param title
	 *           titre de la notification
	 * @param msg
	 *           contenu de la notif
	 * @param type
	 *           news, live
	 * @param urlImage
	 *           url de l'image (utilise sur android uniquement
	 * @param detailsId
	 *           ID de la news ou du match. utilise pour acceder directement au contenu apres avoir
	 *           clique sur la notification
	 * @param sendIOS
	 *           boolean pour l'envoi ios ou non
	 * @param sendAndroid
	 *           boolean pour l'envoi android ou non
	 * @return
	 */
	public static boolean sendNotificationMsgTo(String title, String msg, NotificationType type,
			String urlImage, String detailsId, boolean sendIOS, boolean sendAndroid) {
		msg = msg.replaceAll("\\<[^>]*>", "");
		LOGGER.info("Message to send: " + msg);
		try {
			List<NotificationId> idList = NotificationIdManager.loadAllNotificationId();
			List<String> androidIdListString = new ArrayList<String>();
			List<String> iOsIdListString = new ArrayList<String>();

			for (NotificationId id : idList) {
				if (Pattern.matches("[a-fA-F0-9]{64}", id.getId()) && sendIOS) {
					// iOsIdListString.add(id.getId());
					// Pour le moment on envoie les notifications une par une pour verifier que tous les
					// tokens sont ok
					sendAPNSNotification(title, msg, type, urlImage, detailsId, id.getId());

				} else androidIdListString.add(id.getId());
			}

			// iOsIdListString.add("426a16664a62ad9b8f5b50cf3f76f799e6fa7c3bd118b8a12b45c0d9215faf1e");
			// iOsIdListString.add("fcdcb5fbb5ecc9b80399558b1d4dc04584c436ccef5ab9360a2ffe517ed1451f");
			if (!androidIdListString.isEmpty() && sendAndroid)
				sendGCMNotifications(title, msg, type, urlImage, detailsId, androidIdListString);

			// if (!iOsIdListString.isEmpty() && sendIOS) sendAPNSNotifications(title, msg, type,
			// urlImage, detailsId, iOsIdListString);

			// ** Envoi 50 par 50 **
			// boolean finished = false;
			// int index = 0;
			// while (!finished) {
			// if (iOsIdListString.size() >= index + 50) {
			// if (!iOsIdListString.isEmpty() && sendIOS) sendAPNSNotifications(title, msg, type,
			// urlImage, detailsId, iOsIdListString.subList(index, index + 50));
			// } else {
			// if (!iOsIdListString.isEmpty() && sendIOS) sendAPNSNotifications(title, msg, type,
			// urlImage, detailsId, iOsIdListString.subList(index, iOsIdListString.size()));
			// finished = true;
			// }
			// index += 50;
			// }

			return true;
		} catch (Exception e) {
			LOGGER.severe("Error notification: " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Send Android notifications
	 * 
	 * @throws Exception
	 */
	public static void sendGCMNotifications(String title, String msg, NotificationType type,
			String urlImage, String detailsId, List<String> androidIdListString) throws Exception {

		Sender sender = new Sender(Constant.API_KEY);

		Builder builder = new Message.Builder().collapseKey("1").delayWhileIdle(true)
				.addData("title", title).addData("message", msg).addData("type", type.getLabel())
				.addData("detailsId", detailsId).addData("urlImage", urlImage);

		Message message = builder.build();

		// Check the GCM results: if error 401: the id is not valid anymore. If canonicalId not
		// null, it means a device has 2 ids or more.
		List<NotificationId> idListToRemove = new ArrayList<NotificationId>();
		MulticastResult multiResults = sender.send(message, androidIdListString, 4);
		if (multiResults.getCanonicalIds() > 0) {
			int count = 0;
			for (Result r : multiResults.getResults()) {
				if ((r.getCanonicalRegistrationId() != null) || (r.getErrorCodeName() != null)) {
					idListToRemove.add(new NotificationId(androidIdListString.get(count)));
				}
				count++;
			}
		}

		// remove obsolete ids
		NotificationIdManager.removeNotificationIdList(idListToRemove);

	}

	/**
	 * Send iOs notification to a single device
	 **/
	public static void sendAPNSNotification(String title, String msg, NotificationType type,
			String urlImage, String detailsId, String iOsToken) throws Exception {

		LOGGER.info("Try sending notification iOS notification to device token: " + iOsToken);

		PushNotification pn = new PushNotification().setAlert(msg).setCustomPayload("title", title)
				.setCustomPayload("body", msg).setCustomPayload("urlImage", urlImage)
				.setCustomPayload("detailsId", detailsId).setCustomPayload("type", type.getLabel())
				.setBadge(1).setDeviceTokens(iOsToken);

		DeferredTask task = new SendPushNotificationToApnsTask(pn);
		QueueFactory.getQueue("apns").add(TaskOptions.Builder.withPayload(task));
	}

	/**
	 * Send iOs notifications
	 */
	public static void sendAPNSNotifications(String title, String msg, NotificationType type,
			String urlImage, String detailsId, List<String> iOsIdListString) throws Exception {

		LOGGER.info("iOS notification list size: " + iOsIdListString.size());
		// Prepare ApnsService

		/* Java apns-GAE */
		PushNotification pn = new PushNotification().setAlert(msg).setCustomPayload("title", title)
				.setCustomPayload("body", msg).setCustomPayload("urlImage", urlImage)
				.setCustomPayload("detailsId", detailsId).setCustomPayload("type", type.getLabel())
				.setBadge(1).setDeviceTokens(iOsIdListString);

		DeferredTask task = new SendPushNotificationToApnsTask(pn);
		QueueFactory.getQueue("apns").add(TaskOptions.Builder.withPayload(task));
		/* End java apns GAE */

	}
}
