package eu.boss.mobilesport.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import eu.boss.mobilesport.externalmodel.NewsType;

@Entity
@Cache(expirationSeconds = 600)
public class News implements Serializable {

	private static final long serialVersionUID = 2601781002138506750L;

	public News() {
	}

	public News(String title, Date date, String imageUrl, String imageBlobKey, String content,
			NewsType type) {
		super();
		this.title = title;
		this.date = date;
		this.imageUrl = imageUrl;
		this.imageBlobKey = imageBlobKey;
		this.content = content;
		this.newsType = type;
	}

	@Id
	Long id;

	@Index
	private String title;
	@Index
	private Date date;

	private String imageUrl;
	private String imageBlobKey;
	private String content;
	private NewsType newsType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public String getFormatedDate() {
		TimeZone tzEurope = TimeZone.getTimeZone("Europe/Paris");
		SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
		df.setTimeZone(tzEurope);
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.FRANCE);
		df2.setTimeZone(tzEurope);
		return df.format(date) + " à " + df2.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageBlobKey() {
		return imageBlobKey;
	}

	public void setImageBlobKey(String imageBlobKey) {
		this.imageBlobKey = imageBlobKey;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NewsType getNewsType() {
		return newsType;
	}

	public void setNewsType(NewsType newsType) {
		this.newsType = newsType;
	}

	public String getContentTruncated() {
		String contentTemp = content.replaceAll("\\<[^>]*>", "");
		if (contentTemp.length() > 200) {
			return contentTemp.substring(0, 200) + "...";
		}
		return contentTemp + "...";
	}

}
