package eu.boss.mobilesport.model;

import java.io.Serializable;
import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * POJO representant un token pour garder le login actif depuis l'appli. Datastore a une structure
 * particuliere, si besoin: http://openclassrooms
 * .com/courses/montez-votre-site-dans-le-cloud-avec-google-app-engine/le-datastore
 * 
 * @author Arnaud
 */
@Entity
@Cache
public class SecurityToken implements Serializable {

	private static final long serialVersionUID = -7401995298791527900L;

	@Id
	Long id;

	@Index
	String tokenId;

	private Date expirationDate;
	private int isActive;

	@Parent
	private Key<Login> parent;

	public SecurityToken() {
	}

	public SecurityToken(String tokenId, Date expirationDate, int isActive, Key<Login> parent) {
		super();
		this.tokenId = tokenId;
		this.expirationDate = expirationDate;
		this.isActive = isActive;
		this.parent = parent;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Key<Login> getParent() {
		return parent;
	}

	public void setParent(Key<Login> parent) {
		this.parent = parent;
	}

}
