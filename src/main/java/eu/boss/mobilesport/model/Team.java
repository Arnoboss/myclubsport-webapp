package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * POJO representant une equipe. Chaque equipe possede un intitule (A, B, C, ...) et a pour parent
 * une Season
 * 
 * @author Arnaud
 */
@Entity
@Cache
public class Team implements Serializable {

	private static final long serialVersionUID = 4719436006365760109L;

	@Id
	Long id;

	@Parent
	Key<Season> parent;

	@Index
	String categoryName;

	// String value de TeamNameMessage
	@Index
	String teamName;

	String urlCalendar;
	String urlRank;

	public Team() {
	}

	public Team(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getUrlCalendar() {
		return urlCalendar;
	}

	public void setUrlCalendar(String urlCalendar) {
		this.urlCalendar = urlCalendar;
	}

	public String getUrlRank() {
		return urlRank;
	}

	public void setUrlRank(String urlRank) {
		this.urlRank = urlRank;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Key<Season> getParent() {
		return parent;
	}

	public void setParent(Key<Season> parent) {
		this.parent = parent;
	}

}
