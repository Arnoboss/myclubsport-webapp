package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * POJO representant une saison Datastore a une structure particuliere, si besoin:
 * http://openclassrooms
 * .com/courses/montez-votre-site-dans-le-cloud-avec-google-app-engine/le-datastore
 * 
 * @author Arnaud
 */
@Entity
@Cache
public class Season implements Serializable {

	private static final long serialVersionUID = 2825918570821114684L;

	@Id
	Long id;

	@Index
	String seasonDate;

	public Season() {
	}

	public Season(String seasonDate) {
		super();
		this.seasonDate = seasonDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSeasonDate() {
		return seasonDate;
	}

	public void setSeasonDate(String seasonDate) {
		this.seasonDate = seasonDate;
	}

	@Override
	public String toString() {
		return "Saison: " + seasonDate;
	}

}
