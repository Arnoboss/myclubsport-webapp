package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import eu.boss.mobilesport.externalmodel.PlayerPositionMessage;

/**
 * POJO representant un joueur. Les joueurs sont lies e une saison. Ils ont pour enfant un ou des
 * TeamStats
 * 
 * @author Arnaud
 */
@Entity
@Cache
public class Player implements Serializable {

	private static final long serialVersionUID = -3960955935393890527L;

	@Id
	Long id;

	@Index
	String name;

	@Index
	String category;

	int age;
	String position;

	@Parent
	Key<Season> parent;

	public Player() {
	}

	public Player(String name, int age, PlayerPositionMessage position, Key<Season> parent) {
		super();
		this.name = name;
		this.age = age;
		this.position = position.name();
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Key<Season> getParent() {
		return parent;
	}

	public void setParent(Key<Season> parent) {
		this.parent = parent;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "id: " + id + "\nname: " + getName() + "\n age: " + getAge() + " ans \n"
				+ "position: " + getPosition() + "\n";
	}

	public String toStringHTML() {
		return "name: " + getName() + "</br>" + "age: " + getAge() + " ans </br>" + "position: "
				+ getPosition() + "</br>";
	}

}
