package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

/**
 * Classe un peu particuliere liee au fonctionnement du datastore. Une instance de cette classe
 * represente les statistiques d'un joueur dans une equipe (A, B ou C)
 * 
 * @author Arnaud
 */
@Entity
@Cache
public class TeamStats implements Serializable {

	private static final long serialVersionUID = 8490657316001060348L;

	@Id
	public Long id;

	@Parent
	Key<Player> parent;

	String teamName;
	int gamesPlayed;
	int goals;
	int assists;
	int autoGoals;
	int yellowCards;
	int redCards;

	public TeamStats(Key<Player> parent, int gamesPlayed, int goals, int assists, int autoGoals,
			int yellowCards, int redCards, String category, String teamName) {
		super();
		this.gamesPlayed = gamesPlayed;
		this.parent = parent;
		this.goals = goals;
		this.assists = assists;
		this.autoGoals = autoGoals;
		this.yellowCards = yellowCards;
		this.redCards = redCards;
		this.teamName = teamName;
	}

	public TeamStats() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Key<Player> getParent() {
		return parent;
	}

	public void setParent(Key<Player> parent) {
		this.parent = parent;
	}

	public int getGoals() {
		return goals;
	}

	public void setGoals(int goals) {
		this.goals = goals;
	}

	public int getAssists() {
		return assists;
	}

	public void setAssists(int assists) {
		this.assists = assists;
	}

	public int getAutoGoals() {
		return autoGoals;
	}

	public void setAutoGoals(int autoGoals) {
		this.autoGoals = autoGoals;
	}

	public int getYellowCards() {
		return yellowCards;
	}

	public void setYellowCards(int yellowCards) {
		this.yellowCards = yellowCards;
	}

	public int getRedCards() {
		return redCards;
	}

	public void setRedCards(int redCards) {
		this.redCards = redCards;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	@Override
	public String toString() {
		return "team name: " + getTeamName() + "\n games played: " + getGamesPlayed() + "\n goals : "
				+ getGoals() + "\n " + "assists: " + assists + "\n yellow cards: " + yellowCards
				+ "\n red cards: " + redCards + "\n\n";
	}

	public String toStringHTML() {
		return "team name: " + getTeamName() + "</br> games played: " + getGamesPlayed()
				+ "</br> goals : " + getGoals() + "</br>" + "assists: " + assists
				+ "</br>yellow cards: " + yellowCards + "</br> red cards: " + redCards + "</br></br>";
	}
}
