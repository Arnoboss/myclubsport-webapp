package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache(expirationSeconds = 600)
public class Login implements Serializable {

	private static final long serialVersionUID = 2601781002138506750L;

	public Login() {
	}

	public Login(String name, String password) {
		this.name = name;
		this.password = password;
	}

	@Id
	Long id;

	@Index
	private String name;
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
