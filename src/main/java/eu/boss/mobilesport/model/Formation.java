package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
@Cache(expirationSeconds = 600)
public class Formation implements Serializable {

	private static final long serialVersionUID = -1099716944992693722L;

	@Id
	Long idFormation;

	@Parent
	Key<Match> matchParent;
	String formationName;
	Integer nbTitulaires;
	Integer nbRemplacants;

	public Long getIdFormation() {
		return idFormation;
	}

	public void setIdFormation(Long idFormation) {
		this.idFormation = idFormation;
	}

	public String getFormationName() {
		return formationName;
	}

	public void setFormationName(String formationName) {
		this.formationName = formationName;
	}

	public Key<Match> getMatchParent() {
		return matchParent;
	}

	public void setMatchParent(Key<Match> matchParent) {
		this.matchParent = matchParent;
	}

	public int getNbTitulaires() {
		return nbTitulaires;
	}

	public void setNbTitulaires(int nbTitulaires) {
		this.nbTitulaires = nbTitulaires;
	}

	public Integer getNbRemplacants() {
		return nbRemplacants;
	}

	public void setNbRemplacants(Integer nbRemplacants) {
		this.nbRemplacants = nbRemplacants;
	}

}
