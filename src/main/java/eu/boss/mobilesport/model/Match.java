package eu.boss.mobilesport.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import eu.boss.mobilesport.externalmodel.Category;
import eu.boss.mobilesport.externalmodel.MatchStatus;
import eu.boss.mobilesport.externalmodel.MatchType;

@Entity
@Cache(expirationSeconds = 600)
public class Match implements Serializable {

	private static final long serialVersionUID = -923553433055785838L;

	public Match() {
	}

	public Match(Key<Season> parent, Date date, String team1, String team2, String intitule,
			String categoryName, String teamName, MatchType matchType) {
		super();
		this.parent = parent;
		this.date = date;
		this.team1 = team1;
		this.team2 = team2;
		this.intitule = intitule;
		this.categoryName = categoryName;
		this.teamName = teamName;
		this.matchType = matchType;
	}

	@Id
	Long id;

	@Parent
	Key<Season> parent;

	@Index
	private Date date;

	@Index
	private MatchStatus status = MatchStatus.NOT_STARTED;

	@Index
	private boolean autoSavedStats = false;

	private String team1;
	private String team2;
	// intitule du match (championnat, amicaux, ...)

	private String intitule;
	private int goalsTeam1;
	private int goalsTeam2;
	private String buteursTeam1;
	private String buteursTeam2;
	private MatchType matchType;

	// Gestion dynamique des differentes equipes et des stats
	@Index
	private String categoryName;
	@Index
	private String teamName;

	public Date getDate() {
		return date;
	}

	public String getFormatedDate() {

		TimeZone tzEurope = TimeZone.getTimeZone("Europe/Paris");
		SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
		df.setTimeZone(tzEurope);
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.FRANCE);
		df2.setTimeZone(tzEurope);
		return df.format(date) + " à " + df2.format(date);
	}

	/**
	 * @return title for auto generated convocation
	 */
	@JsonIgnore
	public String getConvocTitle() {
		return "Convocations match " + getCategoryNameStr() + " " + teamName;
	}

	@JsonIgnore
	public String getConvocContent() {
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.FRANCE);
		return "Voici les convocations pour le match " + getCategoryNameStr() + " " + teamName
				+ " prévu ce week end. Il opposera <span style=\"font-weight:bold\">" + team1
				+ "</span> à <span style=\"font-weight:bold\">" + team2
				+ "</span>. Le coup d'envoi sera donné à " + df2.format(date) + ".";
	}

	public String getCategoryNameStr() {
		return Category.valueOf(categoryName).getName();
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Key<Season> getParent() {
		return parent;
	}

	public void setParent(Key<Season> parent) {
		this.parent = parent;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public int getGoalsTeam1() {
		return goalsTeam1;
	}

	public void setGoalsTeam1(int goalsTeam1) {
		this.goalsTeam1 = goalsTeam1;
	}

	public int getGoalsTeam2() {
		return goalsTeam2;
	}

	public void setGoalsTeam2(int goalsTeam2) {
		this.goalsTeam2 = goalsTeam2;
	}

	public String getButeursTeam1() {
		return buteursTeam1;
	}

	public void setButeursTeam1(String buteursTeam1) {
		this.buteursTeam1 = buteursTeam1;
	}

	public String getButeursTeam2() {
		return buteursTeam2;
	}

	public void setButeursTeam2(String buteursTeam2) {
		this.buteursTeam2 = buteursTeam2;
	}

	public MatchStatus getStatus() {
		return status;
	}

	public void setStatus(MatchStatus status) {
		this.status = status;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public boolean isAutoSavedStats() {
		return autoSavedStats;
	}

	public void setAutoSavedStats(boolean autoSavedStats) {
		this.autoSavedStats = autoSavedStats;
	}

	public MatchType getMatchType() {
		return matchType;
	}

	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}
}