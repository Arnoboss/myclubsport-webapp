package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
@Cache(expirationSeconds = 600)
public class FormationItem implements Serializable {

	private static final long serialVersionUID = 2496762932522958273L;

	@Id
	Long idFormationItem;

	@Parent
	Key<Formation> formation;

	@Index
	Integer numItem;
	Integer coord;
	String playerName;

	public Long getIdFormationItem() {
		return idFormationItem;
	}

	public void setIdFormationItem(Long idFormationItem) {
		this.idFormationItem = idFormationItem;
	}

	public Integer getNumItem() {
		return numItem;
	}

	public void setNumItem(Integer numItem) {
		this.numItem = numItem;
	}

	public Integer getCoord() {
		return coord;
	}

	public void setCoord(Integer coord) {
		this.coord = coord;
	}

	public Key<Formation> getFormation() {
		return formation;
	}

	public void setFormation(Key<Formation> formation) {
		this.formation = formation;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

}
