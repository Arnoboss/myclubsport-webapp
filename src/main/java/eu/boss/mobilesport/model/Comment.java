package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * POJO representant un commentaire. Lie e un Match par un lien parent / enfant
 * 
 * @author Arnaud
 */
@Entity
@Cache(expirationSeconds = 600)
public class Comment implements Serializable {

	private static final long serialVersionUID = -6575780731721313889L;

	public Comment() {
	}

	/**
	 * @param content
	 * @param important
	 *           Comments which will be displayed in bold in the list
	 */
	public Comment(Key<Match> parent, String content, boolean important, int minuteEvent) {
		super();
		this.parent = parent;
		this.content = content;
		this.important = important;
		this.minuteEvent = minuteEvent;
	}

	@Id
	Long id;

	@Parent
	Key<Match> parent;

	private String content;
	private boolean important;

	@Index
	private int minuteEvent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isImportant() {
		return important;
	}

	public void setImportant(boolean important) {
		this.important = important;
	}

	public Key<Match> getParent() {
		return parent;
	}

	public void setParent(Key<Match> parent) {
		this.parent = parent;
	}

	public int getMinuteEvent() {
		return minuteEvent;
	}

	public void setMinuteEvent(int minuteEvent) {
		this.minuteEvent = minuteEvent;
	}

}
