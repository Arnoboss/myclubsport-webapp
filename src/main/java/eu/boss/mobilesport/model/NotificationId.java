package eu.boss.mobilesport.model;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache(expirationSeconds = 600)
public class NotificationId implements Serializable {

	private static final long serialVersionUID = -5048995456816487842L;

	@Id
	String id;

	@Index
	String deviceType;

	String notificationSubscription;

	public NotificationId() {
	}

	public NotificationId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getNotificationSubscription() {
		return notificationSubscription;
	}

	public void setNotificationSubscription(String notificationSubscription) {
		this.notificationSubscription = notificationSubscription;
	}

}
